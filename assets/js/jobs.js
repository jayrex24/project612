$(function() {

  $('#job_table').DataTable({
    stateSave: true,
    dom: 'lBfrtip',
    buttons: ['csv', 'excel', 'print'],
    columnDefs: [{
      targets: 2,
      render: function(data, type, row) {
        return data.length > 80 ?
          data.substr(0, 80) + '…' :
          data;
      }
    }]
  });

  //set initial count
  $("#viewing_jobs, #total_jobs").text($("#open_jobs .card-wrapper:visible").length);

  $('#search').on("keyup", function() {
    let value = $(this).val().toLowerCase();

    $("#open_jobs .card-wrapper").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });

    let visibleCardsCount = $("#open_jobs .card-wrapper:visible").length;

    if (visibleCardsCount == 0) {
      $("#no_jobs").removeClass('hide');
    } else {
      $("#no_jobs").addClass('hide');
    }

    $("#viewing_jobs").text(visibleCardsCount);
  });

  $("#job_details_icon").on('click', function() {
    if ($("#job_details").hasClass('hide')) {
      $("#job_details").removeClass('hide');
      $("#job_details_icon span").removeClass('glyphicon-plus').addClass('glyphicon-minus');
    } else {
      $("#job_details").addClass('hide');
      $("#job_details_icon span").removeClass('glyphicon-minus').addClass('glyphicon-plus');
    }
  });

});