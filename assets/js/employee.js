$(function() {
  $.each($('.add-to'), function() {
    let closestDiv = $(this);
    let buttonsArea = closestDiv.next('.buttonsArea');
    let pageForm = closestDiv.closest('form');

    if (closestDiv.find('fieldset').length >= 10) {
      buttonsArea.find('.addButton').hide();
    }

    if (closestDiv.find('fieldset').length > 1) {
      closestDiv.find('.remove_div').show();
    }

    $.each(closestDiv.find('.legend_id'), function(i, v) {
      let fieldset = $(this).closest('fieldset');
      let removeButton = fieldset.find('legend > button');

      removeButton.on('click', function() {
        this.closest('fieldset').remove();

        //hide if only one fieldset exist
        if (closestDiv.find('.legend_id').length == 1) {
          closestDiv.find('.remove_div').hide();
        }

        $.each(closestDiv.find('.legend_id'), function(_i, _v) {
          $(this).text(_i + 1);
        });

        resetInputIndexAll(closestDiv.find('.legend_id'));

        if (closestDiv.find('fieldset').length <= 10) {
          buttonsArea.find('.addButton').show();
        }
      });
    });

    //click no education, phone, employment presentBox
    let firstFieldset = closestDiv.first('fieldset');
    let formId = pageForm.data('id');
    if (firstFieldset.find('input:first').val() == "" && formId != "applyForm") {
      buttonsArea.find('.opt-out').click();
    }
  });

  $('.addButton').on('click', function() {
    maskPhone('.phone_number');

    let buttonsArea = $(this).closest('.buttonsArea');
    let closestDiv = buttonsArea.prev('.add-to');
    let firstFieldset = closestDiv.find('fieldset').first();
    let new_fieldset = firstFieldset.clone(true);

    //clean new fieldset
    new_fieldset.find('input, textarea, select').removeClass('errorInput');
    new_fieldset.find('.appendedMsg').remove();

    //add new fieldset
    new_fieldset.appendTo(closestDiv);

    if (closestDiv.find('fieldset').length >= 10) {
      buttonsArea.find('.addButton').hide();
    }

    if (closestDiv.find('fieldset').length > 1) {
      closestDiv.find('.remove_div').show();
    }

    $.each(closestDiv.find('.legend_id'), function(i, v) {
      $(this).text(i + 1);
      let fieldset = $(this).closest('fieldset');
      let removeButton = fieldset.find('legend > button');

      resetInputIndex(fieldset, i);

      removeButton.on('click', function() {
        this.closest('fieldset').remove();

        //hide if only one fieldset exist
        if (closestDiv.find('.legend_id').length == 1) {
          closestDiv.find('.remove_div').hide();
        }

        $.each(closestDiv.find('.legend_id'), function(_i, _v) {
          $(this).text(_i + 1);
        });

        resetInputIndexAll(closestDiv.find('.legend_id'));

        if (closestDiv.find('fieldset').length <= 10) {
          buttonsArea.find('.addButton').show();
        }
      });
    });

    //clean new fieldset
    $.each(new_fieldset.find('input, select, textarea'), function() {
      $(this).val('');

      if ($(this).hasClass('calendar')) {
        $(this)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker(calendar_options);
      }
    });
  });

  $('.presentBox').on('change', function() {
    let toDate = $(this).next('.to-date');
    let fieldset = toDate.closest('fieldset');

    if (this.checked) {
      toDate.prop('disabled', true);
      toDate.val('');
      $(this).val('1');
      fieldset.find('.reason_for_leaving').hide();
    } else {
      toDate.prop('disabled', false);
      $(this).val('0');
      fieldset.find('.reason_for_leaving').show();
    }
  });

  $.each($('.to-date'), function() {
    let fieldset = $(this).closest('fieldset');
    if ($(this).val() == "0000-00-00") {
      $(this).val('');
      $(this).prev('.presentBox').click();
      fieldset.find('.reason_for_leaving').hide();
    } else {
      fieldset.find('.reason_for_leaving').show();
    }
  });
});