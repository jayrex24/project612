//minumum age for employment is 14
let minage_options = {
  dateFormat: 'yy-mm-dd',
  constrainInput: true,
  changeMonth: true,
  changeYear: true,
  maxDate: '-14yr',
  defaultDate: '-14yr',
};

let calendar_options = {
  dateFormat: 'yy-mm-dd',
  constrainInput: true,
  changeMonth: true,
  changeYear: true,
  maxDate: new Date,
};

let normal_datepicker_options = {
  dateFormat: 'yy-mm-dd',
  constrainInput: true,
  changeMonth: true,
  changeYear: true,
};

function jsUcfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

let errorInput = function(selector, msg) {
  $('#' + selector).addClass('errorInput');
  $('#' + selector).after('<span class="appendedMsg smallfont redFont">' + msg + ' ' + '</span>');
}

function createHTMLlist(array) {
  let str = '';
  $('input, textarea, select').removeClass('errorInput');
  $('.appendedMsg').remove();

  if (array.length > 1) {
    str += '<h4>Errors found:</h4><ul>';

    array.forEach(function(item) {
      if (item.id) {
        str += '<li><a href="#' + item.id + '" class="alertMessageLink">' + item.msg + '</a></li>';
      } else {
        str += '<li>' + item.msg + '</li>';
      }
      errorInput(item.id, item.msg);
    });

    str += '</ul>';
  } else {
    if (array[0].id) {
      str += '<span><a href="#' + array[0].id + '" class="alertMessageLink">' + array[0].msg + '</a></span>';
    } else {
      str += '<span>' + array[0].msg + '</span>';
    }
    errorInput(array[0].id, array[0].msg);
  }

  return str;
}

function scrollToMsg() {
  $("html, body").animate({
    scrollTop: $('#messageDiv').offset().top
  }, 'fast');
}

function successMsgAlert(msg) {
  hideMessages();
  $('#successMsg').html(createHTMLlist(msg));
  $('#successMsg').fadeIn(100);
}

function errorMsgAlert(msg) {
  hideMessages();
  $('#errorMsg').html(createHTMLlist(msg));
  $('#errorMsg').fadeIn(100);
}

function hideMessages() {
  $('#successMsg').fadeOut(100);
  $('#errorMsg').fadeOut(100);
}

function resetInputIndex(selector, i) {
  //replace the number in the name, label and id attribute
  $.each(selector.find('input, select, textarea'), function() {
    let input_name = $(this).attr('name');
    let new_name = input_name.replace(new RegExp("[0-9]"), i);
    $(this).attr('name', new_name);

    let input_id = $(this).attr('id');
    let new_id = input_id.substring(0, input_id.lastIndexOf('_') + 1) + (i + 1);
    $(this).attr('id', new_id);
    $(this).prev('label').attr('for', new_id);
  });
}

function resetInputIndexAll(selector) {
  $.each(selector, function(i, v) {
    let fieldset = $(this).closest('fieldset');
    resetInputIndex(fieldset, i);
  });
}

function maskPhone(selector) {
  $.each($(selector), function() {
    $(this).mask('000-000-0000'); //initialize
    $(this).on('focus', function() {
      $(this).mask('999-999-9999');
    });
  });
}

function maskSSN(selector) {
  $.each($(selector), function() {
    $(this).mask('000-00-0000'); //initialize
    $(this).on('focus', function() {
      $(this).mask('999-99-9999');
    });
  });

  $(selector).on('focus', function() {
    $(this).attr('type', 'text');
  });

  $(selector).on('blur', function() {
    $(this).attr('type', 'password');
  });
}

$(function() {
  $('#successMsg').hide();
  $('#errorMsg').hide();

  maskPhone('.phone_number');
  maskSSN('.ssn');

  $('.min_age').datepicker(minage_options);
  $('.calendar').datepicker(calendar_options);
  $('.normal-datepicker').datepicker(normal_datepicker_options);

  $('#example').DataTable({
    stateSave: true,
    dom: 'lBfrtip',
    buttons: ['csv', 'excel', 'print']
  });

  $('#applicant_table').DataTable({
    stateSave: true,
    dom: 'lBfrtip',
    buttons: ['csv', 'excel', 'print'],
    columnDefs: [{
      orderable: false,
      targets: 0
    }]
  });

  $("#dialog-message").dialog({
    modal: true,
    autoOpen: false,
    resizable: false,
    height: "auto",
    width: 400,
    buttons: {
      Ok: function() {
        $(this).dialog("close");
      }
    }
  });

  $("#dialog-message-confirmation").dialog({
    modal: true,
    autoOpen: false,
    resizable: false,
    height: "auto",
    width: 400,
    title: 'Confirmation Required',
    buttons: {
      No: function() {
        $(this).dialog("close");
      },
      Yes: function() {
        $(this).dialog("close");
      }
    }
  });

  $('#add_new_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        let _data = $.parseJSON(data);
        let visit = "";

        if (_data.success == false) {
          errorMsgAlert(_data.message);
          scrollToMsg();
        } else {
          if (_data.path) {
            visit = "<a href='" + _data.path + "'>View</a>";
          }
          $("#dialog-message")
            .dialog('option', 'title', 'Success')
            .dialog('option', 'close', function() {
              location.href = _data.redirect_url;
            })
            .dialog('option', 'buttons', {
              'Ok': function() {
                $(this).dialog('close');
                location.href = _data.redirect_url;
              }
            })
            .html(_data.message[0].msg + " " + visit)
            .dialog("open");
        }
      }
    });
  });

  $('#update_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        var _data = $.parseJSON(data);

        if (_data.success == false) {
          errorMsgAlert(_data.message);
        } else {
          successMsgAlert(_data.message);
        }
        scrollToMsg();
      }
    });
  });

  $('#redirect_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        let _data = $.parseJSON(data);

        if (_data.success == false) {
          errorMsgAlert(_data.message);
          scrollToMsg();
        } else {
          location.href = _data.redirect_url;
        }
      }
    });
  });

  $('.opt-out').on('click', function() {
    let buttonsAreaDiv = $(this).closest('.buttonsArea');
    let closestAddToDiv = buttonsAreaDiv.prev('.add-to');
    let id = buttonsAreaDiv.data('area');

    closestAddToDiv.toggle();
    buttonsAreaDiv.find('.addButton').toggle();
    $('#' + id).remove();

    if (closestAddToDiv.is(":hidden")) {
      closestAddToDiv.append(
        $('<input>', {
          type: 'hidden',
          class: 'opt-out-input',
          id: id,
          name: id,
          val: '1'
        })
      );
    }
  });

  $('#admin_forgot_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        let _data = $.parseJSON(data);
        if (_data.success == false) {
          errorMsgAlert(_data.message);
        } else {
          successMsgAlert(_data.message);
          $('#forgot_email').val('');
        }
      }
    });
  });
});