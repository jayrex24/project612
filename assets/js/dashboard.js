$(window).on('load change', function() {
  $.each($("#dashboard_checkboxes input[type='checkbox']"), function() {
    var dashboard_type = $(this)[0].id;

    if ($(this).prop("checked")) {
      if ($("#" + dashboard_type + "_div").length == 0) {
        get_chart(dashboard_type);
      }
    } else {
      $("#" + dashboard_type + "_div").remove();
    }
  });
});

$(function() {
  $("#dashboard_checkboxes input[type='checkbox']").on('click', function() {
    var checked = $(this).is(':checked');
    var dashboard_type = $(this)[0].id;

    if (checked) {
      $.ajax({
        url: '/dashboard/add_dashboard_type',
        type: 'POST',
        data: {
          dashboard_type: dashboard_type
        }
      });
    } else {
      $.ajax({
        url: '/dashboard/delete_dashboard_type',
        type: 'POST',
        data: {
          dashboard_type: dashboard_type
        }
      });
    }
  });
});

function get_chart(name) {
  var el = $('#' + name);
  var url = el.data("url");

  $.ajax({
    url: url,
    type: 'POST',
    data: {},
    success: function(data, status, xhr) {
      var _data = $.parseJSON(data);

      if (_data.success) {
        $("#dashboard_main").append(_data.data);
      }
    }
  });
}