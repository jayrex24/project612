function successMsgAlertDialog(msg) {
  hideMessagesDialog();
  $('.successMsgDialog').html(createHTMLlist(msg));
  $('.messageDivDialog').fadeIn(100);
  $('.successMsgDialog').hide();
}

function errorMsgAlertDialog(msg) {
  hideMessagesDialog();
  $('.errorMsgDialog').html(createHTMLlist(msg));
  $('.messageDivDialog').fadeIn(100);
  $('.errorMsgDialog').show();
}

function hideMessagesDialog() {
  $('.successMsgDialog').hide();
  $('.errorMsgDialog').hide();
  $('.messageDivDialog').fadeOut(100);
}

$(function() {
  $('.successMsgDialog').hide();
  $('.errorMsgDialog').hide();

  $("#login-dialog").dialog({
    modal: true,
    open: function() {
      $('.messageDivDialog').fadeOut(100);
      $('input, textarea, select').removeClass('errorInput');
    },
    autoOpen: false,
    resizable: false,
    height: "auto",
    title: "Applicant Login",
    close: function() {
      $('input, textarea, select').removeClass('errorInput');
      $('.appendedMsg').remove();
    },
    width: 400
  });

  $('#applicant_login').on('click', function() {
    $("#forgot-dialog").dialog('close');
    $("#login-dialog").dialog('open');
  });

  $("#forgot-dialog").dialog({
    modal: true,
    open: function() {
      $('.messageDivDialog').fadeOut(100);
      $('input, textarea, select').removeClass('errorInput');
    },
    autoOpen: false,
    resizable: false,
    height: "auto",
    title: "Forgot Password",
    close: function() {
      $('input, textarea, select').removeClass('errorInput');
      $('.appendedMsg').remove();
    },
    width: 400
  });

  $('#forgot_link').on('click', function() {
    $("#login-dialog").dialog('close');
    $("#forgot-dialog").dialog('open');
  });

  $('#applicant_redirect_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        let _data = $.parseJSON(data);

        if (_data.success == false) {
          errorMsgAlertDialog(_data.message);
        } else {
          location.href = _data.redirect_url;
        }
      }
    });
  });

  $('#applicant_forgot_button').on('click', function() {
    let form = $(this).closest('form');
    let form_data = form.serialize();
    let url = form.data('url');

    $.ajax({
      url: url,
      type: 'POST',
      data: form_data,
      success: function(data, status, xhr) {
        var _data = $.parseJSON(data);

        if (_data.success == false) {
          errorMsgAlertDialog(_data.message);
        } else {
          $("#forgot-dialog").dialog('close');
          $("#forgot_email").val('');
        }
      }
    });
  });

  $('#logged_in_apply_button').on('click', function() {
    $.ajax({
      url: '/applicant/easy_apply_ajax',
      type: 'POST',
      data: {
        job_id: $(this).data('jobid')
      },
      success: function(data, status, xhr) {
        var _data = $.parseJSON(data);

        if (_data.success == false) {
          errorMsgAlert(_data.message);
        } else {
          successMsgAlert(_data.message);
          $('#logged_in_apply_button').hide();
        }
      }
    });
  });

  $('.convert_employee').on('click', function() {
    let url = $(this).data('url');
    let applicant_id = $(this).data('applicantId');
    let job_id = $(this).data('jobId');
    let buttonId = $(this).attr('id');

    if (buttonId == "reject_button") {
      let dialog = "Are you sure you want to reject this application? " +
        "This cannot be reversed. A user must submit another application in the event " +
        " that this was done by mistake.";

      $('#dialog-message-confirmation')
        .html(dialog)
        .dialog(
          'option',
          'buttons', {
            No: function() {
              $(this).dialog("close");
            },
            Yes: function() {
              reject_ajax(url, applicant_id, job_id);
            }
          }
        )
        .dialog('open');
    } else {
      check_email(url, applicant_id, job_id);
    }
  });
});

function reject_ajax(url, applicant_id, job_id) {
  $.ajax({
    url: url,
    type: 'POST',
    data: {
      'applicant_id': applicant_id,
      'job_id': job_id
    },
    success: function(data, status, xhr) {
      var _data = $.parseJSON(data);

      if (_data.success == false) {
        errorMsgAlert(_data.message);

      } else {
        location.reload();
      }
    }
  });
}

function convert_ajax(url, applicant_id, job_id) {
  $('#dialog-message-confirmation')
    .html("Are you sure you want to convert this applicant as an employee?")
    .dialog(
      'option',
      'buttons', {
        No: function() {
          $(this).dialog("close");
        },
        Yes: function() {
          $.ajax({
            url: url,
            type: 'POST',
            data: {
              'applicant_id': applicant_id,
              'job_id': job_id
            },
            success: function(data, status, xhr) {
              var _data = $.parseJSON(data);

              if (_data.success == false) {
                errorMsgAlert(_data.message);

              } else {
                location.reload();
              }
            }
          });
        }
      }
    )
    .dialog('open');
}

function check_email(url, applicant_id, job_id) {
  $.ajax({
    url: '/applicant/check_if_employee',
    type: 'POST',
    data: {
      'applicant_id': applicant_id
    },
    success: function(data, status, xhr) {
      var _data = $.parseJSON(data);

      if (_data.message.employee_exist == true) {
        let dialog = "<strong>Issues found:</strong> <br/>" +
          "Applicant email exists in the system for <strong>" + _data.message.employee_exist_name + ". " +
          "</strong><br/><br/>Click 'Yes' to " +
          "overwrite current employee information with applicant information. " +
          "Otherwise, you can access the employee profile and assign the position.";

        $('#dialog-message-confirmation')
          .html(dialog)
          .dialog(
            'option',
            'buttons', {
              No: function() {
                $(this).dialog("close");
              },
              Yes: function() {
                convert_ajax(url, applicant_id, job_id);
              }
            }
          )
          .dialog('open');

      } else if (_data.success == false) {
        errorMsgAlert(_data.message);

      } else {
        convert_ajax(url, applicant_id, job_id);
      }
    }
  });
}