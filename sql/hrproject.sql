-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 08, 2018 at 03:03 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bethesda_mission`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `addr1` varchar(255) NOT NULL,
  `addr2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `employee_id`, `applicant_id`, `addr1`, `addr2`, `city`, `state`, `zip_code`) VALUES
(1, 1, NULL, '123  Main Street', '', 'Towson', 'New York', 21220),
(2, 5, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(3, 6, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(4, 7, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(5, 8, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(6, 9, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(7, 4, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(13, 16, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(9, 13, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(10, 14, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(11, 15, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(16, NULL, 2, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(14, 17, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(15, 18, NULL, '423 Redbridge street', '', 'Middle River', 'Maryland', 21220),
(17, NULL, 4, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(18, NULL, 5, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(19, NULL, 6, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(20, NULL, 7, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(21, NULL, 8, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(22, NULL, 9, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(23, NULL, 10, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220),
(28, 23, NULL, '123  Main Street', '', 'Towson', 'Alabama', 21220),
(29, 24, NULL, '7800 York Rd.', '', 'Towson', 'Maryland', 21252),
(30, 25, NULL, '123  Main Street', '', 'Towson', 'Pennsylvania', 21220);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0=inactive, 1=active',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `test_account` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes (Test account may not change their password)',
  `selector` char(16) DEFAULT NULL,
  `token` char(64) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `employee_id`, `password`, `status`, `created_date`, `test_account`, `selector`, `token`, `expires`) VALUES
(1, 1, '$2y$10$ForgwHxnuaVPgZTEZHi0ZuvH3Qdt7aDIBN3a7kx7Uy9N3W07S0gcW', 1, '2018-11-04 16:12:56', 0, NULL, NULL, NULL),
(61, 4, '$2y$10$K8.CFT/DjNYz1JSvIBEKz.pzXoY7PNKLyOGNrIcHfJ2WhrsA1bIxG', 1, '2018-11-04 16:12:56', 0, NULL, NULL, NULL),
(62, 6, '$2y$10$NA96Pcdah8JG5IPL3h8mN.af7WLEA2WxyW2pVtSf/GrwzD2noXzdi', 1, '2018-11-04 16:13:53', 0, NULL, NULL, NULL),
(63, 5, '$2y$10$/q6Uxdt1DnYVRQ.matFxZ.h7ju6rJHcOf9I8sNYMyjRknKuVaVmQe', 1, '2018-11-04 17:32:57', 0, NULL, NULL, NULL),
(64, 7, '$2y$10$9hbP5hEMvuxNgQf07tUF5.iqtPzPpzVxJAqJ6Bg0k2MNu71gRGvYu', 1, '2018-11-04 17:33:17', 0, NULL, NULL, NULL),
(65, 14, '$2y$10$QH0uayn3BuHdjOWsoXtlyuH1eNgtaIbLq2u4A1izGYVaoEHblPXC6', 0, '2018-11-17 04:06:00', 0, NULL, NULL, NULL),
(66, 15, '$2y$10$F6VMva59/s/ft.xxmSEhOOa2fmZ6wFiKHFZySA2baRNF/NNEcrGYy', 0, '2018-11-24 17:40:18', 0, NULL, NULL, NULL),
(67, 24, '$2y$10$EF7U5OCPPg8cIvp3Q3mkOec1zGZzQC7JCmljB2ueZeNREprldpBvK', 1, '2018-12-07 03:15:01', 1, 'bcc8bbf9ff8d0434', '198e6c90e85fa30705df682cd44a0e328a9f86109a7f6042847fcc3190c41572', 1544212858);

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int(1) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `mid_init` varchar(1) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `ssn` varchar(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) NOT NULL,
  `selector` char(16) DEFAULT NULL,
  `token` char(64) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`id`, `first_name`, `last_name`, `mid_init`, `email`, `date_of_birth`, `ssn`, `created_date`, `password`, `selector`, `token`, `expires`) VALUES
(2, 'Luka', 'Dremmel', '', 'ldremm@fake.net', '2003-04-07', '123-12-3132', '2018-05-22 17:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(4, 'Drake', 'Young', '', 'dyoung@fake.net', '2002-10-09', '123-13-1313', '2018-09-18 17:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(5, 'Jack', 'Monti', '', 'jmonth@fake.net', '2004-11-10', '343-23-2324', '2018-10-10 17:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(6, 'Jennifer', 'Johnson', '', 'jjohns@fake.net', '2004-11-23', '123-13-1231', '2018-11-13 18:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(7, 'James', 'Lee', 'G', 'jlee@fake.net', '2004-11-10', '123-12-3131', '2018-09-12 17:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(8, 'Sarah', 'Larson', 'K', 'slarso@fake.net', '2004-11-17', '123-12-3133', '2018-11-14 18:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(9, 'Henry', 'McCann', 'K', 'hmccan@fake.net', '2004-11-10', '123-13-1231', '2018-12-05 18:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL),
(10, 'Drew', 'French', '', 'dfrenc@fake.net', '2004-10-20', '123-12-3123', '2018-12-12 18:01:17', '$2y$10$Nw/jHGu5IYdyhErpUDcqvO.sWKNmgwQapA3kqga8czbvJABUgavAi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `admin_id` int(11) NOT NULL,
  `dashboard_type` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`admin_id`, `dashboard_type`) VALUES
(1, 'applicants_by_month'),
(1, 'employee_by_status'),
(1, 'employee_by_state'),
(1, 'applications_by_month'),
(1, 'applications_by_day'),
(1, 'jobs_by_facility'),
(67, 'employee_by_status'),
(67, 'applications_by_day');

-- --------------------------------------------------------

--
-- Table structure for table `dependents`
--

CREATE TABLE `dependents` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `mid_init` varchar(1) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `relationship` enum('Son','Daughter','Mother','Father','Husband','Wife') NOT NULL,
  `date_of_birth` date NOT NULL,
  `ssn` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dependents`
--

INSERT INTO `dependents` (`id`, `employee_id`, `first_name`, `mid_init`, `last_name`, `relationship`, `date_of_birth`, `ssn`) VALUES
(8, 7, 'test', 'M', 'test', 'Husband', '2018-11-01', '123-12-3131'),
(9, 7, 're', 'M', 'test', 'Wife', '2018-11-02', '123-13-1311');

-- --------------------------------------------------------

--
-- Table structure for table `education_background`
--

CREATE TABLE `education_background` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `institution` varchar(255) NOT NULL,
  `degree_certification` varchar(255) NOT NULL,
  `comments` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `education_background`
--

INSERT INTO `education_background` (`id`, `employee_id`, `applicant_id`, `from_date`, `to_date`, `institution`, `degree_certification`, `comments`) VALUES
(5, 8, NULL, '2018-10-17', '2018-10-09', 'UMBC', 'Transfer Degree', ''),
(111, 9, NULL, '2018-10-02', '2018-10-18', 'FI', 'Associate Degree', ''),
(108, 1, NULL, '2018-11-16', '2018-11-24', 'FRE', 'Graduate Degrees', ''),
(70, 16, NULL, '2018-11-05', '2018-11-05', 'FRE', 'Graduate Degrees', ''),
(67, 14, NULL, '2018-11-01', '2018-11-02', 'FRE', 'Transfer Degree', ''),
(66, 13, NULL, '2018-11-01', '2018-11-01', 'qw', 'Transfer Degree', ''),
(65, 6, NULL, '2018-10-09', '2018-10-17', 'FREEEEsse', 'Professional Degree', 'testssss'),
(92, NULL, 2, '2018-10-08', '2018-10-16', 'FREZslo', 'Master Degree', 'TEst'),
(61, 7, NULL, '2018-10-16', '2018-10-24', 'TU', 'Undergraduate Degrees', '123123'),
(91, NULL, 2, '2018-10-17', '2018-10-03', 'UMK', 'Associate Degree', 'k'),
(68, 15, NULL, '2018-11-02', '2018-11-02', 'FRE', 'Associate Degree', ''),
(112, NULL, 4, '2018-11-06', '2018-11-15', 'UMBC', 'Bachelor Degree', ''),
(71, 17, NULL, '2018-11-05', '2018-11-05', 'FRE', 'Graduate Degrees', ''),
(72, 18, NULL, '2018-11-05', '2018-11-05', 'FRE', 'Graduate Degrees', ''),
(94, NULL, 5, '2018-11-02', '2018-11-23', 'FRE', 'Associate Degree', ''),
(109, NULL, 6, '2018-11-06', '2018-11-22', '123', 'Transfer Degree', '12313'),
(106, NULL, 7, '2018-11-06', '2018-11-23', 'FRE', 'Bachelor Degree', ''),
(97, NULL, 8, '2018-11-13', '2018-11-07', 'LO', 'Transfer Degree', ''),
(98, NULL, 9, '2018-11-13', '2018-11-16', 'LOP', 'Associate Degree', ''),
(99, NULL, 10, '2018-11-16', '2018-11-24', 'FRE', 'Graduate Degrees', ''),
(102, 23, NULL, '2018-11-06', '2018-11-22', '123', 'Transfer Degree', '12313'),
(107, 24, NULL, '2011-12-14', '2015-12-16', 'TU', 'Graduate Degrees', ''),
(114, 25, NULL, '2018-11-13', '2018-11-16', 'LOP', 'Associate Degree', '');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `mid_init` varchar(1) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `employment_date` date NOT NULL,
  `status_date` date DEFAULT NULL,
  `employment_status` enum('Active','Inactive','Terminated') NOT NULL,
  `ssn` varchar(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `first_name`, `mid_init`, `last_name`, `email`, `date_of_birth`, `employment_date`, `status_date`, `employment_status`, `ssn`, `created_date`) VALUES
(1, 'Jayson', '', 'Malabanan', 'jayson.malabanan@gmail.com', '2004-10-20', '2018-12-02', '2018-12-02', 'Active', '123-12-3123', '2018-12-07 18:43:21'),
(4, 'Youssof', '', 'Dembe', 'ydembe1@students.towson.edu', '2004-10-12', '2018-10-09', '2018-11-24', 'Active', '605-34-6323', '2018-11-24 17:54:09'),
(5, 'Kenneth', '', 'Pough', 'kenneth.pough@gmail.com', '2004-10-15', '2018-10-17', '2018-11-24', 'Active', '239-89-8939', '2018-11-24 17:54:25'),
(6, 'Esther', '', 'Akinmudo', 'eakinm1@students.towson.edu', '2004-10-20', '2018-10-17', '2018-11-24', 'Active', '689-89-8988', '2018-11-24 17:54:57'),
(7, 'Joel', '', 'Weymouth', 'joelweymouthtais@yahoo.com', '2004-10-20', '2018-10-23', '2018-11-26', 'Active', '289-89-8989', '2018-11-26 20:04:42'),
(8, 'Jake', 'E', 'Nicholson', 'jnicho1@students.to', '2004-10-13', '2018-10-09', '2018-12-07', 'Inactive', '898-08-0808', '2018-12-07 13:54:26'),
(9, 'John', 'T', 'Smith', 'jsmith1@students.to', '2004-10-12', '2018-10-23', '2018-12-03', 'Inactive', '343-43-4344', '2018-12-03 19:44:37'),
(10, 'Jeff', 'M', 'Riley', 'jriley1@students.to', '2004-10-19', '1997-12-06', '2018-12-03', 'Inactive', '605-34-6522', '2018-12-03 19:44:23'),
(11, 'Ed', 'E', 'Cruz', 'ecruz1@students.to', '2004-10-19', '1997-12-06', '2018-12-02', 'Active', '605-34-6522', '2018-12-02 04:11:21'),
(13, 'Brittany', 'R', 'Vegas', 'bvegas1@students.to', '2004-10-09', '2018-10-09', '2018-12-02', 'Terminated', '605-34-6572', '2018-12-02 04:11:20'),
(14, 'Lisa', 'K', 'Brown', 'lbrown1@students.to', '2004-10-08', '2018-11-21', '2018-12-02', 'Active', '605-34-1231', '2018-12-02 04:11:18'),
(15, 'Rodney', 'L', 'Lopez', 'rlopez1@students.to', '2004-10-08', '2018-10-02', '2018-12-02', 'Active', '605-34-6571', '2018-12-02 04:11:17'),
(16, 'Bob', '', 'Michigan', 'bmichi@students.to', '2004-10-08', '2018-11-19', '2018-12-02', 'Active', '123-12-3131', '2018-12-02 04:11:12'),
(17, 'Mike', '', 'Smith', 'msmith@students.to', '2004-10-08', '2018-11-19', '2018-12-02', 'Active', '123-12-3131', '2018-12-02 04:00:02'),
(18, 'Eric', '', 'Johns', 'ejohns@students.to', '2004-10-08', '2018-11-19', '2018-12-02', 'Active', '123-12-3131', '2018-12-02 04:11:15'),
(23, 'Jeff', '', 'Maine', 'jmaine@students.to', '2004-11-23', '2018-12-02', '2018-12-02', 'Active', '123-13-1231', '2018-12-02 04:11:07'),
(24, 'Towson', 'U', 'Tester', 'bethesda.md.hr@gmail.com', '2004-10-20', '2018-12-21', NULL, 'Active', '575-75-7585', '2018-12-07 02:41:52'),
(25, 'Henry', 'K', 'McCann', 'hmccan@fake.net', '2004-11-10', '2018-12-07', '2018-12-07', 'Active', '123-13-1231', '2018-12-07 20:11:40');

-- --------------------------------------------------------

--
-- Table structure for table `employment_background`
--

CREATE TABLE `employment_background` (
  `id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `institution` varchar(100) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `reason_for_leaving` text NOT NULL,
  `supervisor_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employment_background`
--

INSERT INTO `employment_background` (`id`, `applicant_id`, `employee_id`, `from_date`, `to_date`, `institution`, `job_title`, `reason_for_leaving`, `supervisor_name`) VALUES
(45, 4, NULL, '2012-11-08', '2018-11-09', 'KML', 'Developer', 'Got bored', ''),
(11, NULL, 6, '2018-06-12', '2018-11-02', 'ko', 'll', 'fired', 'loki'),
(25, 2, NULL, '2018-10-11', '2018-10-16', 'test', 'teting', 'test', ''),
(16, NULL, 7, '2018-10-02', '2018-10-24', 'Yahoo!', 'Testing Google stuff', 'Testing', 'Brandon'),
(15, NULL, 18, '2018-11-12', '2018-11-07', 'qwe', 'qweq', '1231', NULL),
(27, 5, NULL, '2018-11-02', '2018-11-14', 'FLL', 'Developer', 'Tired', NULL),
(44, 6, NULL, '2018-11-14', '2018-11-22', '1231', 'tester', 'test', ''),
(42, 7, NULL, '2018-11-07', '2018-11-23', 'ER', 'Lotto', 'qweqe', ''),
(30, 8, NULL, '2018-11-20', '2018-11-08', '12313', 'LO', '123131', NULL),
(31, 9, NULL, '2018-11-13', '2018-11-14', 'OLO', 'LOO', '1231MIN', NULL),
(32, 10, NULL, '2018-11-16', '2018-11-24', 'test', 'test', 'test', NULL),
(39, NULL, 1, '2018-11-16', '2018-11-24', 'test', 'test', 'test', NULL),
(38, NULL, 23, '2018-11-14', '2018-11-08', '1231', 'tester', 'test', NULL),
(43, NULL, 24, '2010-12-08', '2018-12-06', 'TU', 'Tester', 'Tired of testing', NULL),
(47, NULL, 25, '2018-11-13', '2018-11-14', 'OLO', 'LOO', '1231MIN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `facility_name` varchar(255) NOT NULL,
  `location_state` varchar(255) NOT NULL,
  `location_city` text NOT NULL,
  `manager_employee_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `facility_name`, `location_state`, `location_city`, `manager_employee_id`, `created_date`) VALUES
(1, 'Annapolis Lab', 'Maryland', 'Annapolis', 1, '2018-10-23 00:41:08'),
(2, 'Baltimore Lab', 'Maryland', 'Baltimore', 4, '2018-10-23 00:41:08'),
(3, 'Aberdeen Lab', 'Maryland', 'Aberdeen', 6, '2018-10-23 00:41:08'),
(4, 'Annapolis Lab', 'Maryland', 'Annapolis', 7, '2018-10-23 00:41:08'),
(5, 'Bethesda Lab', 'Maryland', 'Bethesda', 5, '2018-10-23 00:41:08'),
(6, 'Towson Lab', 'Maryland', 'Towson', 5, '2018-10-23 00:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_applied`
--

CREATE TABLE `jobs_applied` (
  `job_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `application_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=new, 1=rejected, 2=awarded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs_applied`
--

INSERT INTO `jobs_applied` (`job_id`, `applicant_id`, `application_date`, `status`) VALUES
(3, 8, '2018-11-24 17:27:48', 0),
(3, 7, '2018-11-24 17:24:55', 1),
(1, 6, '2018-11-24 17:15:00', 2),
(24, 2, '2018-11-19 21:30:32', 1),
(4, 2, '2018-11-19 21:26:32', 1),
(73, 2, '2018-11-19 21:25:28', 1),
(3, 2, '2018-11-19 20:57:57', 1),
(1, 2, '2018-11-19 20:55:44', 0),
(3, 9, '2018-11-24 17:30:51', 2),
(37, 2, '2018-11-27 01:20:15', 0),
(3, 10, '2018-11-27 02:24:44', 1),
(40, 10, '2018-11-28 21:21:17', 2),
(3, 5, '2018-12-07 14:10:20', 0),
(32, 5, '2018-12-07 14:10:31', 0),
(65, 4, '2018-12-07 14:11:20', 0),
(29, 4, '2018-12-07 14:11:26', 0),
(7, 4, '2018-12-07 14:11:41', 0),
(45, 7, '2018-12-07 14:12:37', 1),
(9, 9, '2018-12-07 14:13:15', 0),
(14, 9, '2018-12-07 14:13:20', 0),
(10, 9, '2018-12-07 14:13:31', 0),
(17, 9, '2018-12-07 14:13:36', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jobs_assignment`
--

CREATE TABLE `jobs_assignment` (
  `job_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `assignment_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs_assignment`
--

INSERT INTO `jobs_assignment` (`job_id`, `employee_id`, `assignment_date`) VALUES
(21, 18, '2018-11-19 20:59:28'),
(40, 1, '2018-12-02 03:41:44'),
(20, 17, '2018-11-19 20:59:28'),
(26, 13, '2018-11-19 20:59:28'),
(2, 15, '2018-11-19 20:59:28'),
(39, 4, '2018-11-19 20:59:28'),
(13, 14, '2018-11-19 20:59:28'),
(73, 11, '2018-11-19 21:34:09'),
(4, 16, '2018-12-02 03:59:24'),
(1, 23, '2018-12-02 01:52:52'),
(12, 24, '2018-12-07 03:34:36'),
(3, 25, '2018-12-07 20:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `job_position`
--

CREATE TABLE `job_position` (
  `id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `description` text,
  `facility_id` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_position`
--

INSERT INTO `job_position` (`id`, `job_title`, `description`, `facility_id`, `created_date`) VALUES
(1, 'Account Executive II', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. jayson\r\n\r\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. jayson\r\n\r\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. jayson', 3, '2018-12-07 04:37:10'),
(2, 'Junior Executive', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 6, '2018-10-23 00:39:31'),
(3, 'Occupational Therapist', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\r\n\r\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\r\n\r\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 1, '2018-10-23 00:39:31'),
(4, 'Account Executive II', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. jayson', 5, '2018-10-26 16:28:54'),
(5, 'Operator', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 3, '2018-10-23 00:39:31'),
(7, 'Executive Secretary I', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\r\n\r\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 4, '2018-10-23 00:39:31'),
(8, 'Web Designer II', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\r\n\r\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 2, '2018-10-23 00:39:31'),
(9, 'Compensation Analyst', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.', 5, '2018-10-23 00:39:31'),
(10, 'Software Consultant', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\r\n\r\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1, '2018-10-23 00:39:31'),
(11, 'Teacher', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\r\n\r\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\r\n\r\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 6, '2018-10-23 00:39:31'),
(12, 'Statistician II', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 4, '2018-10-23 00:39:31'),
(13, 'Project Manager', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 6, '2018-10-23 00:39:31'),
(14, 'Compensation Analyst', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 6, '2018-10-23 00:39:31'),
(15, 'Financial Advisor', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 1, '2018-10-23 00:39:31'),
(16, 'Structural Analysis Engineer', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 6, '2018-10-23 00:39:31'),
(17, 'Human Resources Manager', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\r\n\r\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 6, '2018-10-23 00:39:31'),
(18, 'Web Designer III', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1, '2018-10-23 00:39:31'),
(19, 'Food Chemist', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 2, '2018-10-23 00:39:31'),
(20, 'Budget/Accounting Analyst I', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, '2018-10-23 00:39:31'),
(21, 'Electrical Engineer', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 5, '2018-10-23 00:39:31'),
(22, 'Human Resources Manager', 'In congue. Etiam justo. Etiam pretium iaculis justo.\r\n\r\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 3, '2018-10-23 00:39:31'),
(23, 'Analyst Programmer', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 4, '2018-10-23 00:39:31'),
(24, 'Senior Cost Accountant XX', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. jayson', 6, '2018-10-23 15:26:29'),
(25, 'Editor', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\r\n\r\nIn congue. Etiam justo. Etiam pretium iaculis justo.\r\n\r\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 4, '2018-10-23 00:39:31'),
(26, 'Software Engineer III', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 5, '2018-10-23 00:39:31'),
(27, 'Graphic Designer', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\r\n\r\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 1, '2018-10-23 00:39:31'),
(28, 'Pharmacist', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 4, '2018-10-23 00:39:31'),
(29, 'Account Executive', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 5, '2018-10-23 00:39:31'),
(30, 'VP Marketing', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 4, '2018-10-23 00:39:31'),
(31, 'Health Coach II', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\r\n\r\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 1, '2018-10-23 00:39:31'),
(32, 'Occupational Therapist', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 1, '2018-10-23 00:39:31'),
(33, 'Professor', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\r\n\r\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 5, '2018-10-23 00:39:31'),
(34, 'Senior Sales Associate', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.\r\n\r\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 4, '2018-10-23 00:39:31'),
(35, 'Business Systems Development Analyst', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\r\n\r\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 3, '2018-10-23 00:39:31'),
(36, 'Financial Advisor', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\r\n\r\nSed ante. Vivamus tortor. Duis mattis egestas metus.', 6, '2018-10-23 00:39:31'),
(37, 'Office Assistant I', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 5, '2018-10-23 00:39:31'),
(38, 'Geological Engineer', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\r\n\r\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 2, '2018-10-23 00:39:31'),
(39, 'Legal Assistant', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 2, '2018-10-23 00:39:31'),
(40, 'Nuclear Power Engineer', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\r\n\r\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 5, '2018-10-23 00:39:31'),
(41, 'Recruiter', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\r\n\r\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 1, '2018-10-23 00:39:31'),
(42, 'Recruiter', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\r\n\r\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\r\n\r\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 3, '2018-10-23 00:39:31'),
(43, 'Desktop Support Technician', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 3, '2018-10-23 00:39:31'),
(44, 'Marketing Assistant', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\r\n\r\nIn congue. Etiam justo. Etiam pretium iaculis justo.\r\n\r\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, '2018-10-23 00:39:31'),
(45, 'Occupational Therapist', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 3, '2018-10-23 00:39:31'),
(46, 'Media Manager II', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 4, '2018-10-23 00:39:31'),
(47, 'Sales Representative', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\r\n\r\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 2, '2018-10-23 00:39:31'),
(48, 'Software Consultant', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\r\n\r\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 6, '2018-10-23 00:39:31'),
(49, 'Financial Analyst', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 2, '2018-10-23 00:39:31'),
(50, 'Civil Engineer', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\n\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 4, '2018-10-23 00:39:31'),
(51, 'Assistant Professor', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 2, '2018-10-23 00:39:31'),
(52, 'Health Coach IV', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\r\n\r\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\r\n\r\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 3, '2018-10-23 00:39:31'),
(53, 'Senior Financial Analyst', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 5, '2018-10-23 00:39:31'),
(54, 'Recruiting Manager', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.\r\n\r\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\r\n\r\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 1, '2018-10-23 00:39:31'),
(55, 'Account Coordinator', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\r\n\r\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 6, '2018-10-23 00:39:31'),
(56, 'Librarian', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\r\n\r\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 4, '2018-10-23 00:39:31'),
(57, 'VP Quality Control', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\r\n\r\nIn congue. Etiam justo. Etiam pretium iaculis justo.', 2, '2018-10-23 00:39:31'),
(58, 'Administrative Assistant III', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\r\n\r\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 4, '2018-10-23 00:39:31'),
(59, 'Actuary', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\r\n\r\nSed ante. Vivamus tortor. Duis mattis egestas metus.', 3, '2018-10-23 00:39:31'),
(60, 'VP Marketing', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\r\n\r\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\r\n\r\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 2, '2018-10-23 00:39:31'),
(61, 'Research Associate', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\r\n\r\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 2, '2018-10-23 00:39:31'),
(62, 'Compensation Analyst', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 1, '2018-10-23 00:39:31'),
(63, 'Pharmacist', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2, '2018-10-23 00:39:31'),
(64, 'Programmer Analyst III', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1, '2018-10-23 00:39:31'),
(65, 'Junior Executive', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\r\n\r\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\r\n\r\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 5, '2018-10-23 00:39:31'),
(66, 'Chief Design Engineer', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\r\n\r\nFusce consequat. Nulla nisl. Nunc nisl.', 6, '2018-10-23 00:39:31'),
(67, 'Computer Systems Analyst II', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\r\n\r\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 4, '2018-10-23 00:39:31'),
(68, 'Web Developer IV', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\r\n\r\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\r\n\r\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 3, '2018-10-23 00:39:31'),
(69, 'Senior Financial Analyst', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 4, '2018-10-23 00:39:31'),
(70, 'Database Administrator I', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\r\n\r\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\r\n\r\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 4, '2018-10-23 00:39:31'),
(71, 'Structural Engineer', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\r\n\r\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 1, '2018-10-23 00:39:31'),
(72, 'Product Engineer', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\r\n\r\nPhasellus in felis. Donec semper sapien a libero. Nam dui.\r\n\r\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 1, '2018-10-23 00:39:31'),
(73, 'Nuclear Power Engineer', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 5, '2018-10-23 00:39:31'),
(74, 'Business Systems Development Analyst', 'Fusce consequat. Nulla nisl. Nunc nisl.\r\n\r\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 1, '2018-10-23 00:39:31'),
(75, 'Web Developer I', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\r\n\r\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 5, '2018-12-07 13:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `admin_id` int(11) NOT NULL,
  `permission` enum('Employee Create','Admin Create','Admin Superuser','Facility Create','Job Create','Convert Applicants') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`admin_id`, `permission`) VALUES
(1, 'Admin Superuser'),
(63, 'Admin Superuser'),
(62, 'Admin Superuser'),
(64, 'Admin Superuser'),
(61, 'Facility Create'),
(66, 'Admin Create'),
(66, 'Employee Create'),
(65, 'Admin Create'),
(66, 'Facility Create'),
(66, 'Job Create'),
(61, 'Job Create'),
(67, 'Job Create'),
(67, 'Facility Create'),
(67, 'Employee Create'),
(67, 'Convert Applicants');

-- --------------------------------------------------------

--
-- Table structure for table `phone_information`
--

CREATE TABLE `phone_information` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `phone_type` enum('Mobile','Home') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_information`
--

INSERT INTO `phone_information` (`id`, `employee_id`, `applicant_id`, `phone_number`, `extension`, `phone_type`) VALUES
(67, 5, NULL, '009-009-9887', '', 'Mobile'),
(111, 6, NULL, '703-787-7666', '', 'Home'),
(101, 7, NULL, '345-533-5333', '', NULL),
(95, 4, NULL, '123-131-3131', '1231', 'Home'),
(6, 8, NULL, '12312313131', '1212', 'Home'),
(7, 9, NULL, '7034946368', '123', 'Mobile'),
(149, 1, NULL, '703-494-6368', '', NULL),
(155, NULL, 4, '443-826-5934', '', 'Mobile'),
(94, 4, NULL, '703-494-6368', '12313', NULL),
(136, NULL, 2, '443-846-5948', '', 'Mobile'),
(66, 5, NULL, '703-494-6323', '12313', 'Mobile'),
(65, 5, NULL, '131-312-3122', '', NULL),
(117, 17, NULL, '443-846-5948', '', NULL),
(116, 16, NULL, '443-846-5948', '', NULL),
(110, 6, NULL, '703-494-6368', '', 'Mobile'),
(100, 7, NULL, '703-494-6368', '', 'Home'),
(112, 13, NULL, '703-494-6368', '', NULL),
(144, 14, NULL, '703-494-6368', '', 'Mobile'),
(114, 15, NULL, '703-494-6368', '', 'Home'),
(138, NULL, 5, '703-494-6368', '', NULL),
(118, 18, NULL, '443-846-5948', '', NULL),
(154, NULL, 6, '703-494-6368', '', 'Home'),
(152, NULL, 7, '443-847-6176', '', NULL),
(141, NULL, 8, '123-131-3131', '', 'Home'),
(142, NULL, 9, '123-131-1312', '', 'Mobile'),
(143, NULL, 10, '703-494-6368', '', NULL),
(145, 14, NULL, '787-878-7878', '', 'Mobile'),
(148, 23, NULL, '703-494-6368', '', 'Home'),
(153, 24, NULL, '410-704-2000', '', 'Home'),
(157, 25, NULL, '123-131-1312', '', 'Mobile');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `degree_cert` varchar(50) NOT NULL,
  `certification_id` varchar(50) NOT NULL,
  `effective_date` date NOT NULL,
  `expiration_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id` (`employee_id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `dependents`
--
ALTER TABLE `dependents`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `education_background`
--
ALTER TABLE `education_background`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `employment_background`
--
ALTER TABLE `employment_background`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_position`
--
ALTER TABLE `job_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_information`
--
ALTER TABLE `phone_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dependents`
--
ALTER TABLE `dependents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `education_background`
--
ALTER TABLE `education_background`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `employment_background`
--
ALTER TABLE `employment_background`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `job_position`
--
ALTER TABLE `job_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `phone_information`
--
ALTER TABLE `phone_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
