<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_Model extends CI_Model
{
    public function is_admin()
    {
        if (!isset($_SESSION['is_admin']) || ($_SESSION['is_admin'] == false)) {
            redirect(base_url('/'), 'refresh');
        }
    }

    public function user_login_verify($email, $password)
    {
        $sql = "SELECT a.id, a.password
                  FROM employee e
             LEFT JOIN admin a
                    ON e.id = a.employee_id
                 WHERE a.employee_id IS NOT NULL
                   AND LOWER(e.email) LIKE LOWER(?)";

        if (!$hash = $this->db->query($sql, array(strtolower($email)))->row()) {
            return false;
        }

        return array(
          $this->password->verify_password_hash($password, $hash->password),
          $hash->id
        );
    }

    public function applicant_login_verify($email, $password)
    {
        $sql = "SELECT id, password
                  FROM applicant
                 WHERE LOWER(email) LIKE LOWER(?)";

        if (!$hash = $this->db->query($sql, array(strtolower($email)))->row()) {
            return false;
        }

        return array(
          $this->password->verify_password_hash($password, $hash->password),
          $hash->id
        );
    }
}
