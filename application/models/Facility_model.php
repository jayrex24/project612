<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facility_Model extends CI_Model
{
    public function get_all_facilities()
    {
        $sql = "SELECT DISTINCT f.*, e.first_name, e.last_name
                  FROM facility f
             LEFT JOIN employee e
                    ON f.manager_employee_id = e.id";

        if ($result = $this->db->query($sql)->result()) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_facility_by_id($facility_id)
    {
        $sql = "SELECT f.*, e.first_name, e.last_name
                 FROM facility f
            LEFT JOIN employee e
                   ON f.manager_employee_id = e.id
                WHERE f.id = ?";

        if ($result = $this->db->query($sql, array($facility_id))->row()) {
            return $result;
        } else {
            return false;
        }
    }

    public function add_new_facility($facility)
    {
        if (!$this->db->insert('facility', $facility)) {
            return false;
        }
        return $this->db->insert_id();
    }

    public function set_facility_info($facility_id, $data)
    {
        $this->db->where('id', $facility_id);
        if (!$this->db->update('facility', $data)) {
            return false;
        }
        return true;
    }

    public function get_jobs_count_by_facility()
    {
        $sql = "SELECT COUNT(*) as count, f.facility_name
                  FROM facility f
             LEFT JOIN job_position jp
                    ON f.id = jp.facility_id
              GROUP BY f.facility_name";

        return $this->db->query($sql)->result();
    }
}
