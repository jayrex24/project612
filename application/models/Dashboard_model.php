<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_Model extends CI_Model
{
    public function get_dashboard_selected()
    {
        $admin_id = $this->session->id;

        $sql = "SELECT dashboard_type
                  FROM dashboard
                 WHERE admin_id = ?";

        $dashboard_items = array();
        if ($result = $this->db->query($sql, array($admin_id))->result()) {
            foreach ($result as $row) {
                $dashboard_items[] = $row->dashboard_type;
            }
        }
        return $dashboard_items;
    }

    public function insert_admin_dashboard($data)
    {
        if (!$this->db->replace('dashboard', $data)) {
            return false;
        }
        return true;
    }

    public function delete_admin_dashboard($data)
    {
        $sql = "DELETE FROM dashboard
                 WHERE admin_id = ?
                   AND dashboard_type = ?";

        if ($this->db->query($sql, $data)) {
            return true;
        }
        return false;
    }

    public function create_chart($id, $title, $yAxis, $xAxis, $color, $series, $xAxisData, $yAxisData, $type)
    {
        $return['success'] = true;
        $return['data'] = <<<EOT
              <div class="col col-md-6" id="{$id}_div">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div id="{$id}_chart"></div>
                    <script type="text/javascript">
                    Highcharts.chart('{$id}_chart', {
                      chart: {
                          type: '$type'
                      },
                      title: {
                          text: '$title'
                      },
                      yAxis: {
                          title: { text: '$yAxis' }
                      },
                      legend: {
                          verticalAlign: 'top'
                      },
                      xAxis: {
                          title: { text: '$xAxis'},
                          categories: [$xAxisData]
                      },
                      series: [{
                          name: '$series',
                          data: [$yAxisData],
                          color: '$color'
                      }],
                      responsive: {
                          rules: [{
                              condition: {
                                  maxWidth: 500
                              },
                              chartOptions: {
                                  legend: {
                                      layout: 'horizontal',
                                      align: 'center',
                                      verticalAlign: 'bottom'
                                  }
                              }
                          }]
                      }

                    });
                    </script>
                  </div>
                </div>
              </div>
EOT;

        return $return;
    }
}
