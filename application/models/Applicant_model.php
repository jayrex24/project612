<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Applicant_Model extends CI_Model
{
    public function get_all_applications()
    {
        $sql = "SELECT a.*, jp.id as job_id, jas.employee_id, jp.job_title, ja.application_date, ja.status
                  FROM jobs_applied ja
             LEFT JOIN job_position jp
                    ON ja.job_id = jp.id
             LEFT JOIN applicant a
                    ON ja.applicant_id = a.id
             LEFT JOIN jobs_assignment jas
                    ON jas.job_id = ja.job_id";

        return $this->db->query($sql)->result();
    }

    public function get_applicant_applied($applicant_id)
    {
        $sql = "SELECT jp.*, f.facility_name, jas.employee_id, jap.status, jap.application_date
                  FROM jobs_applied jap
             LEFT JOIN job_position jp
                    ON jap.job_id = jp.id
             LEFT JOIN facility f
                    ON f.id = jp.facility_id
             LEFT JOIN jobs_assignment jas
                    ON jas.job_id = jap.job_id
                 WHERE jap.applicant_id = ?";

        return $this->db->query($sql, array($applicant_id))->result();
    }

    public function job_assigned($job_id)
    {
        $sql = "SELECT *
                  FROM jobs_assignment
                 WHERE job_id = ?";

        return $this->db->query($sql, array($job_id))->result();
    }

    public function already_applied($applicant_id, $job_id)
    {
        $sql = "SELECT *
                  FROM jobs_applied
                 WHERE applicant_id = ?
                   AND job_id = ?";

        return $this->db->query($sql, array($applicant_id, $job_id))->result();
    }

    public function get_application_status($applicant_id, $job_id)
    {
        $sql = "SELECT status
                  FROM jobs_applied
                 WHERE applicant_id = ?
                   AND job_id = ?";

        return $this->db->query($sql, array($applicant_id, $job_id))->row();
    }

    public function set_application_status($applicant_id, $job_id, $status)
    {
        $this->db
          ->where('applicant_id', $applicant_id)
          ->where('job_id', $job_id);

        $set = array('status' => $status);
        if (!$this->db->update('jobs_applied', $set)) {
            return false;
        }
        return true;
    }

    public function apply_to_job($applicant_id, $job_id)
    {
        $binds = array(
        'applicant_id' => $applicant_id,
        'job_id' => $job_id
      );
        if (!$this->db->insert('jobs_applied', $binds)) {
            return false;
        }
        return true;
    }

    public function applicant_email_exists($email, $id = null)
    {
        $binds = array($email);

        $sql = "SELECT email
                  FROM applicant
                 WHERE LOWER(email) LIKE LOWER(?) ";

        if ($id) {
            $sql .= "AND id <> ?";
            $binds[] = $id;
        }

        if ($this->db->query($sql, $binds)->num_rows() <= 0) {
            return false;
        }
        return true;
    }

    public function update_applicant_complete($applicant_id, $personal_data, $address_data, $phone = null, $education = null, $employment = null)
    {
        $this->db->trans_begin();

        if (!$this->update_applicant($applicant_id, $personal_data)) {
            $this->db->trans_rollback();
            return "Unable to add applicant.";
        }

        if (!$this->update_address($applicant_id, $address_data)) {
            $this->db->trans_rollback();
            return "Unable to add address information.";
        }

        $this->delete_phone($applicant_id);
        if (isset($phone)) {
            foreach ($phone as $phone_info) {
                if (!$this->add_phone($applicant_id, $phone_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add phone information.";
                }
            }
        }

        $this->delete_education($applicant_id);
        if (isset($education)) {
            foreach ($education as $education_info) {
                if (!$this->add_education($applicant_id, $education_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add education background.";
                }
            }
        }

        $this->delete_history($applicant_id);
        if (isset($employment)) {
            foreach ($employment as $employment_info) {
                if (!$this->add_history($applicant_id, $employment_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add employment history.";
                }
            }
        }

        $this->db->trans_commit();
        return $applicant_id;
    }

    /** Add Applicant functions */

    public function create_applicant($personal_data, $address_data, $phone = null, $education = null, $employment = null, $job_id, $password)
    {
        $this->db->trans_begin();

        if (!$applicant_id = $this->add_applicant($personal_data)) {
            $this->db->trans_rollback();
            return "Unable to add applicant.";
        }

        if (!$this->add_address($applicant_id, $address_data)) {
            $this->db->trans_rollback();
            return "Unable to add address information.";
        }

        if (isset($phone)) {
            foreach ($phone as $phone_info) {
                if (!$this->add_phone($applicant_id, $phone_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add phone information.";
                }
            }
        }

        if (isset($education)) {
            foreach ($education as $education_info) {
                if (!$this->add_education($applicant_id, $education_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add education background.";
                }
            }
        }

        if (isset($employment)) {
            foreach ($employment as $employment_info) {
                if (!$this->add_history($applicant_id, $employment_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add employment history.";
                }
            }
        }

        if (!$this->apply_job($applicant_id, $job_id)) {
            $this->db->trans_rollback();
            return "Unable to apply for job position.";
        }

        if (!$this->update_password($applicant_id, $password)) {
            $this->db->trans_rollback();
            return "Unable to add password.";
        }

        $this->db->trans_commit();
        return $applicant_id;
    }

    public function add_applicant($personal_data)
    {
        if (!$this->db->insert('applicant', $personal_data)) {
            return false;
        }
        return $this->db->insert_id();
    }

    public function update_applicant($applicant_id, $binds)
    {
        $this->db->where('id', $applicant_id);
        if (!$this->db->update('applicant', $binds)) {
            return false;
        }
        return true;
    }

    public function add_address($applicant_id, $address)
    {
        $address['applicant_id'] = $applicant_id;
        if (!$this->db->insert('address', $address)) {
            return false;
        }
        return true;
    }

    public function update_address($applicant_id, $binds)
    {
        $this->db->where('applicant_id', $applicant_id);
        if (!$this->db->update('address', $binds)) {
            return false;
        }
        return true;
    }

    public function add_phone($applicant_id, $phone_info)
    {
        $phone_info['applicant_id'] = $applicant_id;
        if (!$this->db->insert('phone_information', $phone_info)) {
            return false;
        }
        return true;
    }

    public function delete_phone($applicant_id)
    {
        $sql = "DELETE FROM phone_information
                 WHERE applicant_id = ?";

        if (!$this->db->query($sql, array($applicant_id))) {
            return false;
        }
        return true;
    }

    public function add_education($applicant_id, $education_info)
    {
        $education_info['applicant_id'] = $applicant_id;
        if (!$this->db->insert('education_background', $education_info)) {
            return false;
        }
        return true;
    }

    public function delete_education($applicant_id)
    {
        $sql = "DELETE FROM education_background
                 WHERE applicant_id = ?";

        if (!$this->db->query($sql, array($applicant_id))) {
            return false;
        }
        return true;
    }

    public function add_history($applicant_id, $history_info)
    {
        $history_info['applicant_id'] = $applicant_id;
        if (!$this->db->insert('employment_background', $history_info)) {
            return false;
        }
        return true;
    }

    public function delete_history($applicant_id)
    {
        $sql = "DELETE FROM employment_background
                 WHERE applicant_id = ?";

        if (!$this->db->query($sql, array($applicant_id))) {
            return false;
        }
        return true;
    }

    /* End Add Applicant functions */

    public function apply_job($applicant_id, $job_id)
    {
        $binds = array();
        $binds['applicant_id'] = $applicant_id;
        $binds['job_id'] = $job_id;

        if ($this->db->insert('jobs_applied', $binds)) {
            return true;
        }
        return false;
    }

    public function get_applicant_info($applicant_id)
    {
        $sql = "SELECT *
                  FROM applicant
                 WHERE id = ?";

        return $this->db->query($sql, array($applicant_id))->row();
    }

    public function get_applicant_address($applicant_id)
    {
        $sql = "SELECT a.*
                  FROM applicant ap
             LEFT JOIN address a
                    ON a.applicant_id = ap.id
                 WHERE ap.id = ?";

        return $this->db->query($sql, array($applicant_id))->row();
    }

    public function get_applicant_education($applicant_id)
    {
        $sql = "SELECT eb.*
                  FROM applicant a
             LEFT JOIN education_background eb
                    ON a.id = eb.applicant_id
                 WHERE a.id = ?
              ORDER BY eb.id";

        return $this->db->query($sql, array($applicant_id))->result();
    }

    public function get_applicant_history($applicant_id)
    {
        $sql = "SELECT eb.*
                  FROM applicant a
             LEFT JOIN employment_background eb
                    ON a.id = eb.applicant_id
                 WHERE a.id = ?
              ORDER BY eb.id";

        return $this->db->query($sql, array($applicant_id))->result();
    }

    public function get_applicant_phone($applicant_id)
    {
        $sql = "SELECT pi.*
                  FROM applicant a
             LEFT JOIN phone_information pi
                    ON a.id = pi.applicant_id
                 WHERE a.id = ?
              ORDER BY pi.id";

        if (!$query = $this->db->query($sql, array($applicant_id))->result()) {
            return false;
        }
        return $query;
    }

    public function update_password($applicant_id, $new_password)
    {
        $new_password = $this->password->hash_password($new_password);

        $sql = "UPDATE applicant
                   SET password = ?
                 WHERE id = ?";

        if (!$this->db->query($sql, array($new_password, $applicant_id))) {
            return false;
        }
        return true;
    }

    public function get_token($selector, $time)
    {
        $sql = "SELECT *
                  FROM applicant
                 WHERE selector = ?
                   AND expires >= ?";

        if ($result = $this->db->query($sql, array($selector, $time))->row()) {
            return $result;
        }
        return false;
    }

    public function delete_token($email)
    {
        $sql = "UPDATE applicant
                   SET selector = NULL,
                       token = NULL,
                       expires = NULL
                 WHERE LOWER(email) LIKE LOWER(?)";

        if (!$this->db->query($sql, array($email))) {
            return false;
        }

        return true;
    }

    public function update_token($email, $selector, $token, $expires)
    {
        $sql = "UPDATE applicant
                   SET selector = ?,
                       token = ?,
                       expires = ?
                 WHERE LOWER(email) LIKE LOWER(?)";

        $binds = array($selector, $token, $expires, $email);

        if (!$this->db->query($sql, $binds)) {
            return false;
        }
        return true;
    }

    public function get_applicant_count_by_month()
    {
        $sql = "SELECT COUNT(*) as count, DATE_FORMAT(created_date, '%Y-%m') as monthyear
                  FROM applicant
              GROUP BY DATE_FORMAT(created_date, '%Y-%m')";

        return $this->db->query($sql)->result();
    }

    public function get_application_count_by_day()
    {
        $sql = "SELECT COUNT(*) as count, DATE_FORMAT(application_date, '%Y-%m-%d') as day
                  FROM jobs_applied
              GROUP BY DATE_FORMAT(application_date, '%Y-%m-%d')";

        return $this->db->query($sql)->result();
    }
}
