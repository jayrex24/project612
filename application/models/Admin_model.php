<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
    public function get_all()
    {
        $sql = "SELECT e.*, a.*
                  FROM employee e
             LEFT JOIN admin a
                    ON e.id = a.employee_id
                 WHERE a.employee_id IS NOT NULL
              ORDER BY last_name";

        return $this->db->query($sql)->result();
    }

    public function get_all_non_admin()
    {
        $sql = "SELECT e.*
                  FROM employee e
             LEFT JOIN admin a
                    ON e.id = a.employee_id
                 WHERE a.employee_id IS NULL
              ORDER BY last_name";

        return $this->db->query($sql)->result();
    }

    public function check_admin_exist($id_to_check)
    {
        $sql = "SELECT *
                  FROM admin
                 WHERE employee_id = ?";

        if ($this->db->query($sql, array($id_to_check))->row()) {
            return true;
        }
        return false;
    }

    public function add_new_admin($admin_info)
    {
        $this->db->trans_begin();

        if (!empty($admin_info['permissions'])) {
            $permissions = $admin_info['permissions'];
            unset($admin_info['permissions']);
        }

        if ($this->db->insert('admin', $admin_info)) {
            $new_admin_id = $this->db->insert_id();

            if (isset($permissions) && $new_admin_id) {
                if (!$this->admin_model->update_permission($new_admin_id, $permissions)) {
                    $this->db->trans_rollback();
                    return "Unable to add $perm permissions.";
                }
            }
        } else {
            $this->db->trans_rollback();
            return "Unable to add administrator.";
        }

        $this->db->trans_commit();
        return $new_admin_id;
    }

    public function get_admin_info($admin_id)
    {
        $sql = "SELECT e.*, a.*
                  FROM employee e
             LEFT JOIN admin a
                    ON e.id = a.employee_id
                 WHERE a.id = ?";

        return $this->db->query($sql, array($admin_id))->row();
    }

    public function get_admin_by_email($email)
    {
        $sql = "SELECT a.*
                  FROM admin a
             LEFT JOIN employee e
                    ON e.id = a.employee_id
                 WHERE LOWER(e.email) = LOWER(?)";

        if ($result = $this->db->query($sql, array($email))->row()) {
            return $result;
        }
        return false;
    }

    public function check_id($admin_id)
    {
        $sql = "SELECT *
                  FROM admin
                 WHERE id = ?";

        $query = $this->db->query($sql, array($admin_id));

        if ($query->num_rows() == 0) {
            return false;
        }
        return true;
    }

    public function set_admin_info($employee_id, $admin_id, $admin_info)
    {
        //updating employee information in admin
        $this->db->where('id', $employee_id);
        if ($this->db->update('employee', $admin_info)) {
            return true;
        }
        return false;
    }

    public function get_permissions($admin_id)
    {
        $sql = "SELECT permission
                  FROM permissions
                 WHERE admin_id = ?";

        $permissions = array();
        if ($result = $this->db->query($sql, array($admin_id))->result()) {
            foreach ($result as $row) {
                $permissions[] = $row->permission;
            }
        }
        return $permissions;
    }

    public function update_permission($admin_id, $permissions)
    {
        if (isset($permissions)) {
            $this->delete_current_permissions($admin_id);
            foreach ($permissions as $perm) {
                if (!$this->db->replace('permissions', array( "admin_id" => $admin_id, "permission" => $perm))) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    public function update_status($admin_id, $status)
    {
        $this->db->where('id', $admin_id);
        if (!$this->db->update('admin', array('status' => $status))) {
            return false;
        }
        return true;
    }

    public function delete_current_permissions($admin_id)
    {
        $sql = "DELETE FROM permissions
                 WHERE admin_id = ?";

        $this->db->query($sql, array($admin_id));
    }

    public function delete_admin($admin_id)
    {
        $sql = "DELETE FROM admin
                     WHERE id = ?";

        if ($this->db->query($sql, array($admin_id))) {
            $this->delete_current_permissions($admin_id);
            return true;
        }
        return false;
    }

    public function update_password($admin_id, $new_password)
    {
        $new_password = $this->password->hash_password($new_password);

        $this->db->where('id', $admin_id);
        if (!$this->db->update('admin', array('password' => $new_password))) {
            return false;
        }
        return true;
    }

    public function get_token($selector, $time)
    {
        $sql = "SELECT *
                  FROM admin
                 WHERE selector = ?
                   AND expires >= ?";

        if ($result = $this->db->query($sql, array($selector, $time))->row()) {
            return $result;
        }
        return false;
    }

    public function delete_token($id)
    {
        $sql = "UPDATE admin
                   SET selector = NULL,
                       token = NULL,
                       expires = NULL
                 WHERE id = ?";

        if (!$this->db->query($sql, array($id))) {
            return false;
        }

        return true;
    }

    public function update_token($id, $selector, $token, $expires)
    {
        $sql = "UPDATE admin
                   SET selector = ?,
                       token = ?,
                       expires = ?
                 WHERE id = ?";

        $binds = array($selector, $token, $expires, $id);

        if (!$this->db->query($sql, $binds)) {
            return false;
        }
        return true;
    }
}
