<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee_Model extends CI_Model
{
    public function check_id($employee_id)
    {
        $sql = "SELECT *
                  FROM employee
                 WHERE id = ?";

        $query = $this->db->query($sql, array($employee_id));

        if ($query->num_rows() == 0) {
            return false;
        }
        return true;
    }

    public function get_all()
    {
        $sql = "SELECT *
                  FROM employee
              ORDER BY last_name";

        return $this->db->query($sql)->result();
    }

    public function get_employee_info($employee_id)
    {
        $sql = "SELECT *
                  FROM employee
                 WHERE id = ?";

        return $this->db->query($sql, array($employee_id))->row();
    }

    public function get_employee_by_email($email)
    {
        $sql = "SELECT *
                  FROM employee
                 WHERE LOWER(email) = LOWER(?)";

        return $this->db->query($sql, array($email))->row();
    }

    public function set_personal_info($employee_id, $employee_info)
    {
        $this->db->where('id', $employee_id);
        if (!$this->db->update('employee', $employee_info)) {
            return false;
        }
        return true;
    }

    public function get_employee_address($employee_id)
    {
        $sql = "SELECT a.*
                  FROM employee e
             LEFT JOIN address a
                    ON e.id = a.employee_id
                 WHERE e.id = ?";

        return $this->db->query($sql, array($employee_id))->row();
    }

    public function set_address_info($employee_id, $employee_address)
    {
        $this->db->where('employee_id', $employee_id);
        if (!$this->db->update('address', $employee_address)) {
            return false;
        }
        return true;
    }

    public function delete_address_info($employee_id)
    {
        $sql = "DELETE FROM address
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function get_employee_education($employee_id)
    {
        $sql = "SELECT eb.*
                  FROM employee e
             LEFT JOIN education_background eb
                    ON e.id = eb.employee_id
                 WHERE e.id = ?
              ORDER BY eb.id";

        return $this->db->query($sql, array($employee_id))->result();
    }

    public function delete_education_info($employee_id)
    {
        $sql = "DELETE FROM education_background
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function set_education_info($employee_id, $educationArr)
    {
        //TODO transactions
        $this->delete_education_info($employee_id);

        foreach ($educationArr as $value) {
            $value['employee_id'] = $employee_id;
            if (!$this->add_education($employee_id, $value)) {
                return false;
            }
        }
        return true;
    }

    public function get_employee_history($employee_id)
    {
        $sql = "SELECT eb.*
                  FROM employee e
             LEFT JOIN employment_background eb
                    ON e.id = eb.employee_id
                 WHERE e.id = ?
              ORDER BY eb.id";

        return $this->db->query($sql, array($employee_id))->result();
    }

    public function delete_employment_history($employee_id)
    {
        $sql = "DELETE FROM employment_background
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function set_employment_info($employee_id, $employmentArr)
    {
        //TODO transactions
        $this->delete_employment_history($employee_id);

        foreach ($employmentArr as $value) {
            $value['employee_id'] = $employee_id;
            if (!$this->add_history($employee_id, $value)) {
                return false;
            }
        }
        return true;
    }

    public function delete_phone_number($employee_id)
    {
        $sql = "DELETE FROM phone_information
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function get_employee_phone($employee_id)
    {
        $sql = "SELECT pi.*
                  FROM employee e
             LEFT JOIN phone_information pi
                    ON e.id = pi.employee_id
                 WHERE e.id = ?
              ORDER BY pi.id";

        if (!$query = $this->db->query($sql, array($employee_id))->result()) {
            return false;
        }
        return $query;
    }

    public function set_phone_info($employee_id, $phoneArr)
    {
        //TODO transactions
        $this->delete_phone_number($employee_id);

        foreach ($phoneArr as $value) {
            $value['employee_id'] = $employee_id;
            if (!$this->add_phone($employee_id, $value)) {
                return false;
            }
        }
        return true;
    }

    public function get_employee_dependents($employee_id)
    {
        $sql = "SELECT d.*
                  FROM employee e
             LEFT JOIN dependents d
                    ON e.id = d.employee_id
                 WHERE e.id = ?
              ORDER BY d.id";

        return $this->db->query($sql, array($employee_id))->result();
    }

    public function set_dependent_info($employee_id, $dependentArr)
    {
        //TODO transactions
        $this->delete_dependents($employee_id);

        foreach ($dependentArr as $value) {
            $value['employee_id'] = $employee_id;
            if (!$this->add_dependent($employee_id, $value)) {
                return false;
            }
        }
        return true;
    }

    public function delete_dependents($employee_id)
    {
        $sql = "DELETE FROM dependents
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function delete_employee_info($employee_id)
    {
        $sql = "DELETE FROM employee
                 WHERE id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function delete_admin_employee($employee_id)
    {
        $sql = "DELETE FROM admin
                 WHERE employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    public function remove_facility_manager($employee_id)
    {
        $sql = "UPDATE facility
                   SET manager_employee_id = NULL
                 WHERE manager_employee_id = ?";

        if (!$this->db->query($sql, array($employee_id))) {
            return false;
        }
        return true;
    }

    /** Add Employee functions */

    public function create_employee($personal_data, $address_data, $phone = null, $education = null, $employment = null, $job_id)
    {
        $this->db->trans_begin();

        if (!$employee_id = $this->add_employee($personal_data)) {
            $this->db->trans_rollback();
            return "Unable to add employee.";
        }

        if (!$this->add_address($employee_id, $address_data)) {
            $this->db->trans_rollback();
            return "Unable to add address information.";
        }

        if (isset($phone)) {
            foreach ($phone as $phone_info) {
                if (!$this->add_phone($employee_id, $phone_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add phone information.";
                }
            }
        }

        if (isset($education)) {
            foreach ($education as $education_info) {
                if (!$this->add_education($employee_id, $education_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add education information.";
                }
            }
        }

        if (isset($employment)) {
            foreach ($employment as $employment_info) {
                if (!$this->add_history($employee_id, $employment_info)) {
                    $this->db->trans_rollback();
                    return "Unable to add employment information.";
                }
            }
        }

        if (!$this->assign_job($employee_id, $job_id)) {
            $this->db->trans_rollback();
            return "Unable to add job position.";
        }

        $this->db->trans_commit();
        return $employee_id;
    }

    public function add_employee($personal_data)
    {
        if (!$this->db->insert('employee', $personal_data)) {
            return false;
        }
        return $this->db->insert_id();
    }

    public function add_address($employee_id, $address)
    {
        $address['employee_id'] = $employee_id;
        if (!$this->db->insert('address', $address)) {
            return false;
        }
        return true;
    }

    public function add_phone($employee_id, $phone_info)
    {
        $phone_info['employee_id'] = $employee_id;
        if (!$this->db->insert('phone_information', $phone_info)) {
            return false;
        }
        return true;
    }

    public function add_education($employee_id, $education_info)
    {
        $education_info['employee_id'] = $employee_id;
        if (!$this->db->insert('education_background', $education_info)) {
            return false;
        }
        return true;
    }

    public function add_history($employee_id, $history_info)
    {
        $history_info['employee_id'] = $employee_id;
        if (!$this->db->insert('employment_background', $history_info)) {
            return false;
        }
        return true;
    }

    public function add_dependent($employee_id, $dependent_info)
    {
        $dependent_info['employee_id'] = $employee_id;
        if (!$this->db->insert('dependents', $dependent_info)) {
            return false;
        }
        return true;
    }

    public function assign_job($employee_id, $job_id)
    {
        $binds = array();
        $binds['employee_id'] = $employee_id;
        $binds['job_id'] = $job_id;

        if ($this->delete_assinged_job($employee_id)) {
            if ($this->db->insert('jobs_assignment', $binds)) {
                return true;
            }
        }
        return false;
    }

    public function delete_assinged_job($employee_id)
    {
        $this->db->where('employee_id', $employee_id);
        if ($this->db->delete('jobs_assignment')) {
            return true;
        }
        return false;
    }

    /* End Add Employee functions */

    public function employee_email_exists($email, $id = null)
    {
        $binds = array($email);

        $sql = "SELECT email
                  FROM employee
                 WHERE LOWER(email) LIKE LOWER(?) ";

        if ($id) {
            $sql .= "AND id <> ?";
            $binds[] = $id;
        }

        if ($this->db->query($sql, $binds)->num_rows() <= 0) {
            return false;
        }

        return true;
    }

    public function update_employee_complete($employee_id, $personal_data, $address_data, $phone, $education, $employment, $job_id)
    {
        $this->db->trans_begin();

        if (!$this->set_personal_info($employee_id, $personal_data)) {
            $this->db->trans_rollback();
            return "Unable to add employee.";
        }

        if (!$this->set_address_info($employee_id, $address_data)) {
            $this->db->trans_rollback();
            return "Unable to add address information.";
        }

        $this->delete_phone_number($employee_id);
        foreach ($phone as $phone_info) {
            if (!$this->add_phone($employee_id, $phone_info)) {
                $this->db->trans_rollback();
                return "Unable to add phone information.";
            }
        }

        $this->delete_education_info($employee_id);
        foreach ($education as $education_info) {
            if (!$this->add_education($employee_id, $education_info)) {
                $this->db->trans_rollback();
                return "Unable to add education background.";
            }
        }

        $this->delete_employment_history($employee_id);
        foreach ($employment as $employment_info) {
            if (!$this->add_history($employee_id, $employment_info)) {
                $this->db->trans_rollback();
                return "Unable to add employment history.";
            }
        }

        if (!$this->assign_job($employee_id, $job_id)) {
            $this->db->trans_rollback();
            return "Unable to add job position.";
        }

        $this->db->trans_commit();
        return $employee_id;
    }

    /* Dashboard functions */

    public function get_employee_count_by_state()
    {
        $sql = "SELECT COUNT(*) as count, a.state
                  FROM employee e
             LEFT JOIN address a
                    ON e.id = a.employee_id
              GROUP BY a.state";

        return $this->db->query($sql)->result();
    }

    public function get_employee_count_by_status()
    {
        $sql = "SELECT COUNT(*) as count, employment_status
                  FROM employee
              GROUP BY employment_status";

        return $this->db->query($sql)->result();
    }

    /* End Dashboard functions */
}
