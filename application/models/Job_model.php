<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job_Model extends CI_Model
{
    public function get_all_jobs()
    {
        $sql = "SELECT DISTINCT jp.*, f.facility_name, e.first_name, e.last_name
                  FROM job_position jp
             LEFT JOIN jobs_assignment ja
                    ON jp.id = ja.job_id
             LEFT JOIN employee e
                    ON ja.employee_id = e.id
             LEFT JOIN facility f
                    ON jp.facility_id = f.id";

        if ($result = $this->db->query($sql)->result()) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_open_jobs()
    {
        $sql = "SELECT jp.*, f.facility_name, f.location_city, f.location_state
                  FROM job_position jp
             LEFT JOIN facility f
                    ON jp.facility_id = f.id
                 WHERE jp.id NOT IN (SELECT job_id
                                       FROM jobs_assignment)";

        if ($result = $this->db->query($sql)->result()) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_employee_by_job_id($job_id)
    {
        $sql = "SELECT *
                  FROM employee e
             LEFT JOIN jobs_assignment ja
                    ON ja.employee_id = e.id
                 WHERE ja.job_id = ?";

        if ($result = $this->db->query($sql, array($job_id))->row()) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_job_by_id($job_id)
    {
        $sql = "SELECT jp.*, f.facility_name, f.location_city, f.location_state
                  FROM job_position jp
             LEFT JOIN facility f
                    ON jp.facility_id = f.id
                 WHERE jp.id = ?";

        if ($result = $this->db->query($sql, array($job_id))->row()) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_job_by_employee($employee_id)
    {
        $sql = "SELECT jp.*
                  FROM jobs_assignment ja
             LEFT JOIN job_position jp
                    ON ja.job_id = jp.id
                 WHERE ja.employee_id = ?";

        if ($result = $this->db->query($sql, array($employee_id))->row()) {
            return $result;
        } else {
            return false;
        }
    }

    public function add_new_position($position)
    {
        if (!$this->db->insert('job_position', $position)) {
            return false;
        }
        return $this->db->insert_id();
    }

    public function get_all_facility()
    {
        $sql = "SELECT *
                  FROM facility";

        if ($result = $this->db->query($sql)->result()) {
            return $result;
        }
        return false;
    }

    public function set_job_info($job_id, $data)
    {
        $this->db->where('id', $job_id);
        if (!$this->db->update('job_position', $data)) {
            return false;
        }
        return true;
    }

    public function job_assigned($job_id)
    {
        $sql = "SELECT *
                  FROM jobs_assignment
                 WHERE job_id = ?";

        return $this->db->query($sql, array($job_id))->result();
    }

    public function delete_job_position($job_id)
    {
        $sql = "DELETE FROM job_position
                 WHERE id = ?";

        if (!$this->db->query($sql, array($job_id))) {
            return false;
        }
        return true;
    }

    public function remove_job_assignments_employee($job_id)
    {
        $sql = "UPDATE employee
                   SET job_position_id = NULL
                 WHERE job_position_id = ?";

        if (!$this->db->query($sql, array($job_id))) {
            return false;
        }
        return true;
    }

    public function remove_job_assignments_applicant($job_id)
    {
        $sql = "UPDATE applicant
                   SET job_position_id = NULL
                 WHERE job_position_id = ?";

        if (!$this->db->query($sql, array($job_id))) {
            return false;
        }
        return true;
    }

    public function delete_position($job_id)
    {
        $this->db->trans_begin();

        if (!$this->delete_job_position($job_id)) {
            $this->db->trans_rollback();
            return false;
        }

        if (!$this->remove_job_assignments_employee($job_id)) {
            $this->db->trans_rollback();
            return false;
        }

        if (!$this->remove_job_assignments_applicant($job_id)) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
        return true;
    }
}
