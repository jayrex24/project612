		<div id="dialog-message" title="Alert" style="display:none;">
			<p>Alert Message</p>
		</div>

		<div id="dialog-message-confirmation" title="Alert" style="display:none;">
		  <p>Alert Message</p>
		</div>
	</div>
	<!-- Bootstrap -->
	<script src="<?php echo base_url('assets/source/Bootstrap3/js/bootstrap.min.js'); ?>"></script>

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/source/jQuery-ui/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/jQuery-Mask/src/jquery.mask.js'); ?>"></script>

	<!-- Datatables -->
	<script src="<?php echo base_url('assets/source/DataTables/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Buttons/js/dataTables.buttons.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Buttons/js/buttons.flash.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/JSZip/jszip.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Buttons/js/buttons.html5.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Buttons/js/buttons.print.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Responsive/js/dataTables.responsive.min.js'); ?>"></script>

	<!-- Highchart -->
	<script src="<?php echo base_url('assets/source/Highcharts/code/highcharts.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Highcharts/code/modules/exporting.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Highcharts/code/modules/offline-exporting.js'); ?>"></script>
	<script src="<?php echo base_url('assets/source/Highcharts/code/modules/export-data.js'); ?>"></script>

	<!-- Custom -->
	<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/employee.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/dashboard.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jobs.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/applicant.js'); ?>"></script>
</body>
</html>
