<div class="col col-md-9">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Facility Information (ID: <?php echo $facility_info->id; ?>)</h2>
      </div>
      <div class="panel-body">
        <div class="col col-md-8">
          <form data-url="<?php echo base_url('/facility/facility_info_ajax/'. $facility_info->id); ?>">
            <div class="form-group">
              <label for="facility_name">*Facility Name</label>
              <input type="text" class="form-control" name="facility_name" id="facility_name" placeholder="Enter facility name"
                value="<?php echo $facility_info->facility_name != "" ? htmlspecialchars($facility_info->facility_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="location_city">*Location City</label>
              <input type="text" class="form-control" name="location_city" id="location_city" placeholder="Enter location city"
                value="<?php echo $facility_info->location_city != "" ? htmlspecialchars($facility_info->location_city) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="location_state">*Location State</label>
              <select class="form-control select_state" name="location_state" id="location_state">
                  <option value="" disabled>Please select a state</option>
                  <?php foreach ($states as $state):?>
                    <option value="<?php echo $state;?>"
                      <?php echo $state == $facility_info->location_state ? "selected" : "";?>>
                      <?php echo $state;?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="form-group">
              <label for="manager_employee_id">*Facility Manager</label>
              <select class="form-control select_state" name="manager_employee_id" id="manager_employee_id">
                  <option value="" disabled selected>Please select an employee</option>
                  <?php foreach ($employees as $employee):?>
                    <option value="<?php echo $employee->id;?>"
                      <?php echo $facility_info->manager_employee_id == $employee->id ? "selected" : ""; ?>>
                      <?php echo sprintf(
                        '%s, %s (Employee ID: %d)',
                        htmlspecialchars($employee->last_name),
                        htmlspecialchars($employee->first_name),
                        $employee->id
                      ); ?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="form-group">
              <label for="created_date">Created Date</label>
              <input type="text" class="form-control" id="created_date" disabled
                value="<?php echo $facility_info->created_date != "" ? htmlspecialchars($facility_info->created_date) : ""; ?>">
            </div>
            <?php if (permission_granted(array(F_C, A_S))): ?>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
            <?php /*
            <button type="button" class="btn btn-danger" id="removeButton" data-id="<?php echo $facility_info->id; ?>" data-entity="facility"
                data-url="<?php echo base_url('/facility/delete_facility'); ?>" data-redirect="<?php echo base_url('/facility/home'); ?>">
              <span class="glyphicon glyphicon-trash"></span>
              Remove Facility
            </button> */ ?>
            <?php else: ?>
              <script>
                    $("input, select").attr("disabled", true);
              </script>
            <?php endif; ?>
          </form>
        </div>
      </div>
  </div>
</div>
