<div class="col col-md-9">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Add New Facility</h2>
      </div>
      <div class="panel-body">
        <div class="col col-md-8">
          <form data-url="<?php echo base_url('/facility/add_new_facility_ajax'); ?>">
            <div class="form-group">
              <label for="facility_name">*Facility Name</label>
              <input type="text" class="form-control" name="facility_name" id="facility_name" placeholder="Enter facility name">
            </div>
            <div class="form-group">
              <label for="location_city">*Location City</label>
              <input type="text" class="form-control" name="location_city" id="location_city" placeholder="Enter location city">
            </div>
            <div class="form-group">
              <label for="location_state">*Location State</label>
              <select class="form-control select_state" name="location_state" id="location_state">
                  <option value="" disabled selected>Please select a state</option>
                  <?php foreach ($states as $state):?>
                    <option value="<?php echo $state;?>">
                      <?php echo $state;?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="form-group">
              <label for="manager_employee_id">*Facility Manager</label>
              <select class="form-control select_state" name="manager_employee_id" id="manager_employee_id">
                  <option value="" disabled selected>Please select an employee</option>
                  <?php foreach ($employees as $employee):?>
                    <option value="<?php echo $employee->id;?>">
                      <?php echo sprintf(
                        '%s, %s (Employee ID: %d)',
                        htmlspecialchars($employee->last_name),
                        htmlspecialchars($employee->first_name),
                        $employee->id
                      ); ?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <?php if (permission_granted(array(F_C, A_S))): ?>
            <button type="button" class="btn btn-primary" id="add_new_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Submit
            </button>
            <?php else: ?>
              <script>
                    $("input, select").attr("disabled", true);
              </script>
            <?php endif; ?>
          </form>
        </div>
      </div>
  </div>
</div>
