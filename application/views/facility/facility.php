<div class="col col-md-12">
  <?php if (!isset($facilities) || empty($facilities)): ?>
    <div class="panel panel-default">
      <div class="panel-body">No facility currently exist. Add new facility using the 'Add New Facility' button.</div>
    </div>
  <?php else: ?>
    <div class="panel panel-default">
      <div class="panel-body">Click the link on the table rows to view/edit facility information.</div>
    </div>
    <table id="example" class="display responsive" style="width:100%">
        <thead>
            <tr>
                <th>Facility ID</th>
                <th>Facility Name</th>
                <th>Location</th>
                <th>Facility Manager</th>
                <th>Created Date</th>
            </tr>
        </thead>
        <tbody><?php foreach ($facilities as $facility): ?>
            <tr>
                <td>
                  <a href="<?php echo base_url('/facility/facility_info/' . $facility->id); ?>" title="View facility">
                    <?php echo $facility->id; ?></a>
                </td>
                <td><?php echo htmlspecialchars($facility->facility_name); ?></td>
                <td><?php echo htmlspecialchars($facility->location_city . ", " . $facility->location_state); ?></td>
                <td><?php
                  echo sprintf(
                      "%s %s",
                      isset($facility->first_name) ? htmlspecialchars($facility->first_name) : "",
                      isset($facility->last_name) ? htmlspecialchars($facility->last_name) : ""
                  ); ?></td>
                <td><?php echo htmlspecialchars($facility->created_date); ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table><br/>
  <?php endif; ?>

  <?php if (permission_granted(array(F_C, A_S))): ?>
  <button class="btn btn-primary" onclick="location.href='<?php echo base_url('/facility/add_new_facility'); ?>'">
    <span class="glyphicon glyphicon-plus"></span>
      Add New Facility
  </button>
  <?php endif; ?>
</div>
