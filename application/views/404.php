<div id="LoginForm">
	<div class="container">
		<h1 class="form-heading">
			<a href="<?php echo base_url(); ?>">Bethesda Mission HR</a>
		</h1>
		<div class="login-form">
			<div class="main-div center-text">
			    <div class="login-color">
			   		<h3>Sorry, I couldn't find that one.</h3><br/>
            			<img src="<?php echo base_url('/assets/images/notfound.png'); ?>" alt="not found in map">
            			<br/><br/>
			   		<p>Click the home button to return to the home page.</p><br/>
            		<a href="<?php echo base_url('/dashboard/home'); ?>" type="button" class="btn btn-primary">Home</a>
			 	</div>
  			</div>
		</div>
	</div>
</div>
