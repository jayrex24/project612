<div id="LoginForm">
	<div class="container"><br/>
		<a href="<?php echo base_url('/applicant/jobs'); ?>" class="pull-right"
				style="color:white; font-size:12pt;">Job Opportunities</a></li>
		<h1 class="form-heading">
			<a href="<?php echo base_url();?>">Bethesda Mission HR</a>
		</h1>
		<div class="login-form">
			<div class="main-div">
					<div id="messageDiv">
						<div class="alert alert-danger alert-dismissable" id="errorMsg"></div>
						<div class="alert alert-success alert-dismissable" id="successMsg"></div>
					</div>
			    <div class="text-muted" style="text-align:center;">
			   		<h4>Password Reset</h4>
			   		<p>Please enter your new password</p><br/>
			   </div>
			    <form data-url="<?php echo base_url('/reset/password_reset_ajax'); ?>" id="Login">
							<input type="hidden" name="selector" value="<?php echo $selector; ?>">
							<input type="hidden" name="validator" value="<?php echo $validator; ?>">
			        <div class="form-group">
			            <input type="password" class="form-control" name="password" id="password" maxlength="50"
											placeholder="Password">
			        </div>
       				<button type="button" class="btn btn-primary" id="redirect_button">Submit</button>
    			</form><br/>
					<div style="text-align:center;">
						<a href="<?php echo base_url(); ?>">Home</a>
					</div>
				</div>
		</div>
	</div>
</div>
