<div class="row">
  <div id="open_jobs">
    <?php if (empty($jobs)) : ?>
      <div class="col-md-12">
        <p>There are currently no job openings. Please check back later.</p>
      </div>
    <?php else: ?>
      <div class="col-md-12">
        <p>Viewing <span id="viewing_jobs"></span> out of <span id="total_jobs"></span> jobs</p>
      </div>
      <?php foreach ($jobs as $key => $job) : ?>
      <div class="col-md-4 card-wrapper">
        <div class="card well well-sm">
          <div class="card-body">
            <h5 class="card-title"><?php echo htmlspecialchars($job->job_title); ?></h5>
            <p class="card-text" id="short_description">
              <?php
                $pieces = explode(" ", $job->description);
                $first_part = implode(" ", array_splice($pieces, 0, 20));
                echo htmlspecialchars($first_part . "...");
              ?>
              <br/><br/>
              <span>Location: <?php echo htmlspecialchars(sprintf(
                  "%s, %s (%s)",
                  $job->location_city,
                  $job->location_state,
                  $job->facility_name
              )); ?></span>
            </p>
            <p class="hide" id="full_description"><?php echo htmlspecialchars($job->description); ?></p>
            <a href="<?php echo base_url('/applicant/apply/'.$job->id); ?>">View More</a>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      <div class="col-md-6">
        <p class="hide" id="no_jobs">No matching job opening.</p>
      </div>
    <?php endif; ?>
  </div>
</div>
