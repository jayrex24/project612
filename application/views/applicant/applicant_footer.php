  </div>
</div>
<div id="login-dialog" style="display:none;">
  <div class="messageDivDialog">
    <div class="alert alert-danger alert-dismissable errorMsgDialog"></div>
    <div class="alert alert-success alert-dismissable successMsgDialog"></div>
  </div>
  <form data-url="<?php echo base_url('/applicant/login_ajax'); ?>">
      <div class="form-group">
          <input type="email" class="form-control" name="email" id="applicant_email" placeholder="Email">
      </div>
      <div class="form-group">
          <input type="password" class="form-control" name="password" id="applicant_password" maxlength="50"
              placeholder="Password">
      </div>
      <button type="button" class="btn btn-primary" id="applicant_redirect_button">Login</button>
      <a href="#" id="forgot_link">Forgot Password?</a>
  </form>
</div>

<div id="forgot-dialog" style="display:none;">
  <div class="messageDivDialog">
    <div class="alert alert-danger alert-dismissable errorMsgDialog"></div>
    <div class="alert alert-success alert-dismissable successMsgDialog"></div>
  </div>
  <form data-url="<?php echo base_url('/applicant/password_retrieve_ajax'); ?>">
      <div class="form-group">
          <input type="email" class="form-control" name="forgot_email" id="forgot_email" placeholder="Email">
          <span class="small text-muted">Please check your email once submitted. We will be sending you a reset link.</span>
      </div>
      <button type="button" class="btn btn-primary" id="applicant_forgot_button">Submit</button>
      <button type="button" class="btn btn-warning" onClick="$('#applicant_login').click();">Canel</button>
  </form>
</div>
