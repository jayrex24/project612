<div class="row">
  <div class="col col-md-12">
    <h4>Application Status</h4>
    <table id="job_table" class="display responsive" style="width:100%">
      <thead>
        <tr>
          <th>Job ID</th>
          <th>Job Title</th>
          <th>Facility</th>
          <th>Application Date</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($applications as $application): ?>
        <tr>
          <td>
            <a href="<?php echo base_url('/applicant/apply/'.$application->id); ?>" title="View Job Description">
              <?php echo $application->id; ?></a>
          </td>
          <td><?php echo htmlspecialchars($application->job_title); ?></td>
          <td><?php echo htmlspecialchars($application->facility_name); ?></td>
          <td><?php echo htmlspecialchars($application->application_date); ?></td>
          <td><?php
                $string = '';
                if ($application->status == 0) {
                    $string = "New";
                    if ($application->employee_id) {
                        $string = "No longer available";
                    }
                } elseif ($application->status == 1) {
                    $string = "Rejected";
                } elseif ($application->status == 2) {
                    $string = "Awarded";
                }
                echo $string;
              ?>
          </td>
        </tr>
        <?php endforeach?>
      </tbody>
    </table>
  </div>
</div>
