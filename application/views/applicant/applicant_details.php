<div class="col col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading"><h2>Application Details</h2></div>
    <div class="panel-body">
      <?php if ($application_status == 0) : ?>
          <?php if (!$employee_assinged) : ?>
              <?php if (permission_granted(array(C_A, A_S))): ?>
                <button type="button" class="btn btn-success convert_employee" id="hire_button" data-url="<?php echo base_url('/applicant/hire_ajax'); ?>"
                  data-applicant-id='<?php echo $applicant_info->id; ?>' data-job-id='<?php echo $job_info->id; ?>'>
                  <span class="glyphicon glyphicon-ok"></span>
                  Hire
                </button>
                <button type="button" class="btn btn-danger convert_employee" id="reject_button" data-url="<?php echo base_url('/applicant/reject_ajax'); ?>"
                  data-applicant-id='<?php echo $applicant_info->id; ?>' data-job-id='<?php echo $job_info->id; ?>'>
                  <span class="glyphicon glyphicon-remove"></span>
                  Reject
                </button><br/><br/>
              <?php else: ?>
                <p>You currently don't have permission to convert applicant.</p>
              <?php endif; ?>
          <?php else: ?>
              <p>This position has been assigned.</p>
          <?php endif; ?>
      <?php elseif ($application_status == 1): ?>
        <p>This application has been rejected.</p>
      <?php elseif ($application_status == 2): ?>
        <p>This applicant has been awarded the position.</p>
      <?php endif; ?>
      <div class="panel panel-default">
        <div class="panel-heading">Job Information</div>
        <div class="panel-body">
          <div class="row">
            <div class="col col-md-8">
              <div class="form-group">
                <strong>Job ID</strong><br/>
                <span><?php echo $job_info->id != "" ? htmlspecialchars($job_info->id) : "";?></span>
              </div>
              <div class="form-group">
                <strong>Title</strong><br/>
                <span><?php echo $job_info->job_title != "" ? htmlspecialchars($job_info->job_title) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Description</strong><br/>
                <span class="description"><?php echo $job_info->description != "" ? htmlspecialchars($job_info->description) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Location</strong><br/>
                <span><?php echo $job_info->location_city != "" ? htmlspecialchars($job_info->location_city) . ", " : "" ;?>
                  <?php echo $job_info->location_state != "" ? htmlspecialchars($job_info->location_state) : "";?>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">Personal Information</div>
        <div class="panel-body">
          <div class="row">
            <div class="col col-md-8">
              <div class="form-group">
                <strong>First Name</strong><br/>
                <span><?php echo $applicant_info->first_name != "" ? htmlspecialchars($applicant_info->first_name) : "";?></span>
              </div>
              <div class="form-group">
                <strong>Middle Initial</strong><br/>
                <span><?php echo $applicant_info->mid_init != "" ? htmlspecialchars($applicant_info->mid_init) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Last Name</strong><br/>
                <span><?php echo $applicant_info->last_name != "" ? htmlspecialchars($applicant_info->last_name) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Email</strong><br/>
                <span><?php echo $applicant_info->email != "" ? htmlspecialchars($applicant_info->email) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Date of Birth <span class="smallfont text-muted">(YYYY-MM-DD)</span></strong><br/>
                <span><?php echo $applicant_info->date_of_birth != "" ? htmlspecialchars($applicant_info->date_of_birth) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Social Security Number <span class="smallfont text-muted">(XXX-XX-XXXX)</span></strong><br/>
                <span><?php echo $applicant_info->ssn != "" ? htmlspecialchars($applicant_info->ssn) : "--";?></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">Address Information</div>
        <div class="panel-body">
          <div class="row">
            <div class="col col-md-8">
              <div class="form-group">
                <strong>Address 1</strong><br/>
                <span><?php echo $applicant_address->addr1 != "" ? htmlspecialchars($applicant_address->addr1) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Address 2</strong><br/>
                <span><?php echo $applicant_address->addr2 != "" ? htmlspecialchars($applicant_address->addr2) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>City</strong><br/>
                <span><?php echo $applicant_address->city != "" ? htmlspecialchars($applicant_address->city) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>State</strong><br/>
                <span><?php echo $applicant_address->state != "" ? htmlspecialchars($applicant_address->state) : "--";?></span>
              </div>
              <div class="form-group">
                <strong>Zip Code</strong><br/>
                <span><?php echo $applicant_address->zip_code != "" ? htmlspecialchars($applicant_address->zip_code) : "--";?></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">Phone Information</div>
        <div class="panel-body">
          <div class="row">
            <div class="col col-md-8">
              <div class="add-to">
              <?php foreach ($applicant_phone as $key => $phone):
                $index = $key + 1; ?>
                <fieldset>
                  <legend>Phone #<span class="legend_id"><?php echo $index; ?></span>
                  </legend>
                  <div class="form-group">
                    <strong>Phone <span class="smallfont text-muted">(XXX-XXX-XXXX)</span></strong><br/>
                    <span><?php echo $phone->phone_number != "" ? htmlspecialchars($phone->phone_number) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>Extension</strong><br/>
                    <span><?php echo $phone->extension != "" ? htmlspecialchars($phone->extension) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>Phone Type</strong><br/>
                    <span><?php echo $phone->phone_type != "" ? htmlspecialchars($phone->phone_type) : "--";?></span>
                  </div>
                </fieldset>
              <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="panel panel-default">
        <div class="panel-heading">Education Information</div>
        <div class="panel-body">
          <div class="col col-md-8">
            <div class="row">
              <div class="add-to">
              <?php foreach ($applicant_education as $key => $education):
                $index = $key + 1; ?>
                <fieldset>
                  <legend>Education #<span class="legend_id"><?php echo $index; ?></span>
                  </legend>
                  <div class="form-group">
                    <strong>School/Institution</strong><br/>
                    <span><?php echo $education->institution != "" ? htmlspecialchars($education->institution) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>Degree/Certification</strong><br/>
                    <span><?php echo $education->degree_certification != "" ? htmlspecialchars($education->degree_certification) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>From Date</strong><br/>
                    <span><?php echo $education->from_date != "" ? htmlspecialchars($education->from_date) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>To Date</strong><br/>
                    <span>
                      <?php
                        if ($education->to_date == "0000-00-00") {
                            echo "Present";
                        } elseif ($education->to_date != "") {
                            htmlspecialchars($education->to_date);
                        } else {
                            echo "--";
                        }
                      ?>
                    </span>
                  </div>
                  <div class="form-group">
                    <strong>Comments</strong><br/>
                    <span><?php echo $education->comments != "" ? htmlspecialchars($education->comments) : "--";?></span>
                  </div>
                </fieldset>
              <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">Employment Information</div>
        <div class="panel-body">
          <div class="col col-md-8">
            <div class="row">
              <div class="add-to">
              <?php foreach ($applicant_history as $key => $history):
                $index = $key + 1; ?>
                <fieldset>
                  <legend>Employment #<span class="legend_id"><?php echo $index; ?></span>
                  </legend>
                  <div class="form-group">
                    <strong>Company/Institution</strong><br/>
                    <span><?php echo $history->institution != "" ? htmlspecialchars($history->institution) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>Position</strong><br/>
                    <span><?php echo $history->job_title != "" ? htmlspecialchars($history->job_title) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>Supervisor name</strong><br/>
                    <span><?php echo $history->supervisor_name != "" ? htmlspecialchars($history->supervisor_name) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>From Date</strong><br/>
                    <span><?php echo $history->from_date != "" ? htmlspecialchars($history->from_date) : "--";?></span>
                  </div>
                  <div class="form-group">
                    <strong>To Date</strong><br/>
                    <span>
                      <?php
                        if ($history->to_date == "0000-00-00") {
                            echo "Present";
                        } elseif ($history->to_date != "") {
                            htmlspecialchars($history->to_date);
                        } else {
                            echo "--";
                        }
                      ?>
                    </span>
                  </div>
                  <div class="form-group">
                    <strong>Reason for Leaving</strong>
                    <br/>
                    <span><?php echo $history->reason_for_leaving != "" ? htmlspecialchars($history->reason_for_leaving) : "--";?></span>
                  </div>
                </fieldset>
              <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
