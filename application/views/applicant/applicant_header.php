<div class="container">
  <div class="col-md-12">
    <div class="row" style="padding-top:10px">
      <div class="col-md-12">
        <span><a href="<?php echo base_url(); ?>">Admin Login</a></span>
        <div class="pull-right">
          <span style="padding-right:20px;"><a href="<?php echo base_url('/applicant/jobs'); ?>">Jobs</a></span>
          <?php if (!$session->applicant) : ?>
            <strong>Applied before? </strong>
            <button type="button" class="btn btn-primary btn-xs" id="applicant_login">
              Login
            </button>
          <?php else: ?>
            <div class="dropdown" style="display:inline">
              <button class="btn btn-default dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                  <strong>Options</strong>
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="<?php echo base_url('/applicant/status'); ?>">Application Status</a></li>
                <li><a class="dropdown-item" href="<?php echo base_url('/applicant/manage'); ?>">Manage Profile</a></li>
                <li class="divider"></li>
                <li><a class="dropdown-item" href="<?php echo base_url('/applicant/logout')?>">Logout</a></li>
              </ul>
            </div>
          <?php endif; ?>
        </div><br/><br/>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <p style="font-size:16pt;"><strong>Bethesda Mission Careers</strong></p>
      </div>
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
        <?php if ($page == 'jobs') : ?>
        <div class="form-group">
          <input type="text" class="form-control" name="search" id="search" placeholder="Search..">
        </div>
        <?php endif;?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="messageDiv">
          <div class="alert alert-danger alert-dismissable" id="errorMsg"></div>
          <div class="alert alert-success alert-dismissable" id="successMsg"></div>
        </div>
      </div>
    </div>
