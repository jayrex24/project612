<div class="col col-md-12">
  <?php if (!isset($applicants) || empty($applicants)): ?>
    <div class="panel panel-default">
      <div class="panel-body">No applicants currently exist.</div>
    </div>
  <?php else: ?>
    <div class="panel panel-default">
      <div class="panel-body">Click the link on the table rows to view application information.</div>
    </div>
    <table id="applicant_table" class="display responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>View/Edit</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Job Title</th>
                <th>Application Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($applicants as $applicant): ?>
            <tr>
                <td>
                  <a href="<?php echo base_url('/applicant/applicant_details/'.$applicant->id.'/'.$applicant->job_id); ?>" title="View Job Description">
                    <span class="glyphicon glyphicon-edit"></span></a>
                </td>
                <td><?php echo htmlspecialchars($applicant->first_name); ?></td>
                <td><?php echo htmlspecialchars($applicant->last_name); ?></td>
                <td><?php echo htmlspecialchars($applicant->email); ?></td>
                <td><?php echo htmlspecialchars($applicant->job_title . " (ID: ". $applicant->job_id .")"); ?></td>
                <td><?php echo htmlspecialchars($applicant->application_date); ?></td>
                <td>
                  <?php
                  if ($applicant->status == 0) {
                      $string = "New";
                      if ($applicant->employee_id) {
                          $string = "Position taken";
                      }
                  } elseif ($applicant->status == 1) {
                      $string = "Rejected";
                  } elseif ($applicant->status == 2) {
                      $string = "Awarded";
                  }
                  echo $string;
                  ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
  <?php endif; ?>
</div>
