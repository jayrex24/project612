<h5>
  <ul style="padding-left:20px;">
    <li>Fill the application below to apply for this role.</li>
    <li>Please note that your information will be kept in our system (We will not be using this information for purposes outside of your employment with us).</li>
    <li>If you have submitted an application in the past, simply login and your information will be retrieved.</li>
  </ul>
</h5>
<form data-url="<?php echo base_url('/applicant/add_applicant_ajax'); ?>" data-id="applyForm">
  <input type="hidden" name="job_position_id" value="<?php echo $job->id; ?>">
  <div class="panel panel-default">
    <div class="panel-heading">Personal Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="form-group">
            <label for="first_name">*First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name"
              placeholder="Enter first name" maxlength="50" >
          </div>
          <div class="form-group">
            <label for="mid_init">Middle Initial</label>
            <input type="text" class="form-control" name="mid_init" id="mid_init"
              placeholder="Enter Middle Initial" maxlength="1">
          </div>
          <div class="form-group">
            <label for="last_name">*Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name"
            placeholder="Enter last name" maxlength="50">
          </div>
          <div class="form-group">
            <label for="email">*Email</label>
            <input type="email" class="form-control" name="email" id="email"
              placeholder='Enter email' maxlength="100">
          </div>
          <div class="form-group">
            <label for="date_of_birth">*Date of Birth <span class="smallfont text-muted">(YYYY-MM-DD)</span></label>
            <input type="text" class="form-control min_age" name="date_of_birth"
                id="date_of_birth" placeholder='Enter date of birth' >
          </div>
          <div class="form-group">
            <label for="ssn">*Social Security Number <span class="smallfont text-muted">(XXX-XX-XXXX)</span></label>
            <input type="password" class="form-control ssn" name="ssn"
                id="ssn" placeholder='Enter Social Security Number' >
          </div>
          <div class="form-group">
            <label for="password">*Password</label>
            <input type="password" class="form-control" name="password"
                id="password" placeholder='Enter Password'>
          </div>
          <div class="form-group">
            <label for="confirm_password">*Confirm Password</label>
            <input type="password" class="form-control" name="confirm_password"
                id="confirm_password" placeholder='Enter Confirm Password'>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Address Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="form-group">
            <label for="addr1">*Address 1</label>
            <input type="text" class="form-control" name="addr1" id="addr1" placeholder='Enter Address 1' >
          </div>
          <div class="form-group">
            <label for="addr2">Address 2</label>
            <input type="text" class="form-control" name="addr2" id="addr2" placeholder='Enter Address 2'>
          </div>
          <div class="form-group">
            <label for="city">*City</label>
            <input type="text" class="form-control" name="city" id="city" placeholder='Enter city' >
          </div>
          <div class="form-group">
            <label for="states">*State</label>
            <select class="form-control select_state" name="state" id="state" >
                <option value="" selected disabled>Please select a state</option>
                <?php foreach ($states as $state): ?>
                  <option value="<?php echo $state; ?>">
                    <?php echo $state; ?>
                  </option>
                <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="zip_code">*Zip Code</label>
            <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder='Enter zip code' maxlength="5">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Phone Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="add-to">
            <fieldset>
              <legend>Phone #<span class="legend_id">1</span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="phoneNumber_1">*Phone <span class="smallfont text-muted">(XXX-XXX-XXXX)</span></label>
                <input type="text" class="form-control phone_number" id="phoneNumber_1" name="phone[0][phone_number]" placeholder='Enter phone number'>
              </div>
              <div class="form-group">
                <label for="extension_1">Extension</label>
                <input type="text" class="form-control" id="extension_1" name="phone[0][extension]" placeholder='Enter extension'>
              </div>
              <div class="form-group">
                <label for="type">Phone Type</label>
                <select class="form-control select_type" id="phoneType_1" name="phone[0][phone_type]">
                    <option value="" disabled selected>
                      Please select a phone type</option>
                    <?php foreach ($phone_types as $type): ?>
                      <option value="<?php echo htmlspecialchars($type); ?>">
                        <?php echo htmlspecialchars($type); ?>
                      </option>
                    <?php endforeach; ?>
                </select>
              </div>
            </fieldset>
          </div>
          <div class='buttonsArea' data-area='opt-out-phone'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Phone</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Phone
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Education Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="add-to">
            <fieldset>
              <legend>Education #<span class="legend_id">1</span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="institution_1">*School/Institute</label>
                <input type="text" class="form-control" id="institution_1" name="education[0][institution]" placeholder="Enter school/institute name">
              </div>
              <div class="form-group">
                <label for="degreeCertification_1">*Education Type</label>
                <select class="form-control select_type" id="degreeCertification_1" name="education[0][degree_certification]">
                    <option value="" selected disabled>Please select an education type</option>
                    <?php foreach ($education_types as $education): ?>
                      <option value="<?php echo $education; ?>">
                        <?php echo $education; ?>
                      </option>
                    <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="fromDate_1">*From Date</label>
                <input type="text" class="form-control calendar" id="fromDate_1" name="education[0][from_date]" placeholder="Enter From date">
              </div>
              <div class="form-group">
                <label for="toDate_1">*To Date</label>&nbsp;
                <input type="checkbox" class="presentBox" id="toDatePresentEducation_1" name="employment[0][present]" value="0"> Present
                <input type="text" class="form-control calendar to-date" id="toDate_1" name="education[0][to_date]" placeholder="Enter To date">
              </div>
              <div class="form-group">
                <label for="comments_1">Comments</label>
                <textarea class="form-control" rows="5" id="comments_1" name="education[0][comments]" placeholder="Enter comments"></textarea>
              </div>
            </fieldset>
          </div>
          <div class='buttonsArea' data-area='opt-out-education'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Education</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Education
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Employment History Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="add-to">
            <fieldset>
              <legend>Employment #<span class="legend_id">1</span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="institutionEmployment_1">*Company/Institute</label>
                <input type="text" class="form-control" id="institutionEmployment_1" name="employment[0][institution]" placeholder="Enter school/institute name">
              </div>
              <div class="form-group">
                <label for="positionEmployment_1">*Position</label>
                <input type="text" class="form-control" id="positionEmployment_1" name="employment[0][job_title]" placeholder="Enter position name">
              </div>
              <div class="form-group">
                <label for="fromDateEmployment_1">*From Date</label>
                <input type="text" class="form-control calendar" id="fromDateEmployment_1" name="employment[0][from_date]" placeholder="Enter From date">
              </div>
              <div class="form-group">
                <label for="toDateEmployment_1">*To Date</label>&nbsp;
                <input type="checkbox" class="presentBox" id="toDatePresentEmployment_1" name="employment[0][present]" value="0"> Present
                <input type="text" class="form-control calendar to-date" id="toDateEmployment_1" name="employment[0][to_date]" placeholder="Enter To date">
              </div>
              <div class="form-group reason_for_leaving">
                <label for="reasonForLeavingEmployment_1">*Reason for Leaving</label>
                <textarea class="form-control" rows="5" id="reasonForLeavingEmployment_1" name="employment[0][reason_for_leaving]" placeholder="Enter comments"></textarea>
              </div>
            </fieldset>
          </div>
          <div class='buttonsArea' data-area='opt-out-employment'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Employment History</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Employment
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col col-md-8">
      <button type="button" class="btn btn-primary" id="add_new_button">
        <span class="glyphicon glyphicon-floppy-disk"></span>
        Submit
      </button>
    </div>
  </div>
</form>
