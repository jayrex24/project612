<div class="row">
  <div class="col-md-12">
    <p style="font-size:12pt;"><strong>Password Reset</strong></p>
    <form data-url="<?php echo base_url('/applicant/password_reset_ajax'); ?>">
        <input type="hidden" name="selector" value="<?php echo $selector; ?>">
        <input type="hidden" name="validator" value="<?php echo $validator; ?>">
        <div class="row">
          <div class="col col-md-3">
            <div id="messageDiv">
              <div class="alert alert-danger" id="errorMsg"></div>
            </div>
            <div class="form-group">
              <label for="password">*Password</label>
              <input type="password" class="form-control" name="password" id="password"
                  placeholder="Enter your new password" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-md-3">
            <button type="button" class="btn btn-primary" id="redirect_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Submit
            </button>
          </div>
        </div>
    </form>
  </div>
</div>
