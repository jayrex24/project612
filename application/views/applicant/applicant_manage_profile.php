<h4>Manage Profile</h4>
<form method="POST" data-url="<?php echo base_url('/applicant/manage_ajax'); ?>">
  <div class="panel panel-default">
    <div class="panel-heading">Personal Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="form-group">
            <label for="first_name">*First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name"
              placeholder="Enter first name" maxlength="50"
              value="<?php echo htmlspecialchars($applicant_info->first_name); ?>">
          </div>
          <div class="form-group">
            <label for="mid_init">Middle Initial</label>
            <input type="text" class="form-control" name="mid_init" id="mid_init"
              placeholder="Enter Middle Initial" maxlength="1"
              value="<?php echo htmlspecialchars($applicant_info->mid_init); ?>">
          </div>
          <div class="form-group">
            <label for="last_name">*Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name"
              placeholder="Enter last name" maxlength="50"
              value="<?php echo htmlspecialchars($applicant_info->last_name); ?>">
          </div>
          <div class="form-group">
            <label for="email">*Email</label>
            <input type="email" class="form-control" name="email" id="email"
              placeholder='Enter email' maxlength="100"
              value="<?php echo htmlspecialchars($applicant_info->email); ?>">
          </div>
          <div class="form-group">
            <label for="date_of_birth">*Date of Birth <span class="smallfont text-muted">(YYYY-MM-DD)</span></label>
            <input type="text" class="form-control min_age" name="date_of_birth"
                id="date_of_birth" placeholder='Enter date of birth'
                value="<?php echo htmlspecialchars($applicant_info->date_of_birth); ?>">
          </div>
          <div class="form-group">
            <label for="ssn">*Social Security Number <span class="smallfont text-muted">(XXX-XX-XXXX)</span></label>
            <input type="password" class="form-control ssn" name="ssn"
                id="ssn" placeholder='Enter Social Security Number'
                value="<?php echo htmlspecialchars($applicant_info->ssn); ?>">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Address Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="form-group">
            <label for="addr1">*Address 1</label>
            <input type="text" class="form-control" name="addr1" id="addr1" placeholder='Enter Address 1'
              value="<?php echo htmlspecialchars($applicant_address->addr1); ?>">
          </div>
          <div class="form-group">
            <label for="addr2">Address 2</label>
            <input type="text" class="form-control" name="addr2" id="addr2" placeholder='Enter Address 2'
              value="<?php echo htmlspecialchars($applicant_address->addr2); ?>">
          </div>
          <div class="form-group">
            <label for="city">*City</label>
            <input type="text" class="form-control" name="city" id="city" placeholder='Enter city'
              value="<?php echo htmlspecialchars($applicant_address->city); ?>">
          </div>
          <div class="form-group">
            <label for="states">*State</label>
            <select class="form-control select_state" name="state" id="state" >
                <option value="" selected disabled>Please select a state</option>
                <?php foreach ($states as $state): ?>
                  <option value="<?php echo $state; ?>"
                    <?php echo $state == $applicant_address->state ? 'selected' : ''; ?>>
                    <?php echo $state; ?>
                  </option>
                <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="zip_code">*Zip Code</label>
            <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder='Enter zip code'
              value="<?php echo htmlspecialchars($applicant_address->zip_code); ?>" maxlength="5">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Phone Information</div>
    <div class="panel-body">
      <div class="row">
        <div class="col col-md-8">
          <div class="add-to">
          <?php foreach ($applicant_phone as $key => $phone):
            $index = $key + 1; ?>
            <fieldset>
              <legend>Phone #<span class="legend_id"><?php echo $index; ?></span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="phoneNumber_1">*Phone <span class="smallfont text-muted">(XXX-XXX-XXXX)</span></label>
                <input type="text" class="form-control phone_number" id="phoneNumber_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][phone_number]"
                  value="<?php echo htmlspecialchars($phone->phone_number); ?>" placeholder='Enter phone number'>
              </div>
              <div class="form-group">
                <label for="extension_1">Extension</label>
                <input type="text" class="form-control" id="extension_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][extension]"
                  value="<?php echo htmlspecialchars($phone->extension); ?>" placeholder='Enter extension'>
              </div>
              <div class="form-group">
                <label for="type">Phone Type</label>
                <select class="form-control select_type" id="phoneType_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][phone_type]">
                    <option value="" disabled <?php echo empty($phone->phone_type) ? "selected" : ""; ?>>
                      Please select a phone type</option>
                    <?php foreach ($phone_types as $type): ?>
                      <option value="<?php echo htmlspecialchars($type); ?>"
                        <?php echo $type == $phone->phone_type ? "selected" : ""; ?>>
                        <?php echo htmlspecialchars($type); ?>
                      </option>
                    <?php endforeach; ?>
                </select>
              </div>
            </fieldset>
          <?php endforeach; ?>
          </div>
          <div class='buttonsArea' data-area='opt-out-phone'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Phone</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Phone
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading">Education Information</div>
    <div class="panel-body">
      <div class="col col-md-8">
        <div class="row">
          <div class="add-to">
          <?php foreach ($applicant_education as $key => $education):
            $index = $key + 1; ?>
            <fieldset>
              <legend>Education #<span class="legend_id"><?php echo $index; ?></span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="institution_<?php echo $index; ?>">*School/Institution</label>
                <input type="text" class="form-control" id="institution_<?php echo $index; ?>"
                  name="education[<?php echo $key; ?>][institution]" placeholder="Enter School/Institution name"
                  value="<?php echo $education->institution != "" ? htmlspecialchars($education->institution) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="degreeCertification_<?php echo $index; ?>">*Degree/Certification</label>
                <select class="form-control select_type" id="degreeCertification_<?php echo $index; ?>"
                    name="education[<?php echo $key; ?>][degree_certification]">
                    <option value="" disabled <?php echo empty($education->degree_certification) ? "selected" : ""; ?>>
                      Please select an education type</option>
                    <?php foreach ($education_types as $type): ?>
                      <option value="<?php echo htmlspecialchars($type); ?>"
                        <?php echo $type == $education->degree_certification ? "selected" : ""; ?>>
                        <?php echo htmlspecialchars($type); ?>
                      </option>
                    <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group">
                <label for="fromDate_<?php echo $index; ?>">*From Date</label>
                <input type="text" class="form-control calendar" id="fromDate_<?php echo $index; ?>"
                  name="education[<?php echo $key; ?>][from_date]" placeholder="Enter From Date"
                  value="<?php echo $education->from_date != "" ? htmlspecialchars($education->from_date) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="toDate_<?php echo $index; ?>">*To Date</label>&nbsp;
                <input type="checkbox" class="presentBox" id="toDatePresentEducation_<?php echo $index; ?>" name="education[<?php echo $key; ?>][present]" value="0"> Present
                <input type="text" class="form-control calendar to-date" id="toDate_<?php echo $index; ?>"
                  name="education[<?php echo $key; ?>][to_date]" placeholder="Enter To Date"
                  value="<?php echo $education->to_date != "" ? htmlspecialchars($education->to_date) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="comments_<?php echo $index; ?>">Comments</label>
                <textarea class="form-control" rows="5" id="comments_<?php echo $index; ?>"
                  name="education[<?php echo $key; ?>][comments]"
                  placeholder="Enter comments"><?php echo $education->comments != "" ? htmlspecialchars($education->comments) : "";?></textarea>
              </div>
            </fieldset>
          <?php endforeach;?>
          </div>
          <div class='buttonsArea' data-area='opt-out-education'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Education</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Education
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading">Employment Information</div>
    <div class="panel-body">
      <div class="col col-md-8">
        <div class="row">
          <div class="add-to">
          <?php foreach ($applicant_history as $key => $history):
            $index = $key + 1; ?>
            <fieldset>
              <legend>Employment #<span class="legend_id"><?php echo $index; ?></span>
                <button type="button" class="btn btn-danger remove_div btn-xs">
                  Remove
                </button>
              </legend>
              <div class="form-group">
                <label for="institutionEmployment_<?php echo $index; ?>">*Company/Institution</label>
                <input type="text" class="form-control" id="institutionEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][institution]" placeholder="Enter Company/Institution name"
                  value="<?php echo $history->institution != "" ? htmlspecialchars($history->institution) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="positionEmployment_<?php echo $index; ?>">*Position</label>
                <input type="text" class="form-control" id="positionEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][job_title]" placeholder="Enter position name"
                  value="<?php echo $history->job_title != "" ? htmlspecialchars($history->job_title) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="supervisorEmployment_<?php echo $index; ?>">Supervisor name</label>
                <input type="text" class="form-control" id="supervisorEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][supervisor_name]" placeholder="Enter supervisor name"
                  value="<?php echo $history->supervisor_name != "" ? htmlspecialchars($history->supervisor_name) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="fromDateEmployment_<?php echo $index; ?>">*From Date</label>
                <input type="text" class="form-control calendar" id="fromDateEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][from_date]" placeholder="Enter From Date"
                  value="<?php echo $history->from_date != "" ? htmlspecialchars($history->from_date) : ""; ?>">
              </div>
              <div class="form-group">
                <label for="toDateEmployment_<?php echo $index; ?>">*To Date</label>&nbsp;
                <input type="checkbox" class="presentBox" id="toDatePresentEmployment_<?php echo $index; ?>" name="employment[<?php echo $key; ?>][present]" value="0"> Present
                <input type="text" class="form-control calendar to-date" id="toDateEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][to_date]" placeholder="Enter To Date"
                  value="<?php echo $history->to_date != "" ? htmlspecialchars($history->to_date) : ""; ?>">
              </div>
              <div class="form-group reason_for_leaving">
                <label for="reasonForLeavingEmployment_<?php echo $index; ?>">*Reason for Leaving</label>
                <textarea class="form-control" rows="5" id="reasonForLeavingEmployment_<?php echo $index; ?>"
                  name="employment[<?php echo $key; ?>][reason_for_leaving]"
                  placeholder="Enter comments"><?php echo $history->reason_for_leaving != "" ? htmlspecialchars($history->reason_for_leaving) : "";?></textarea>
              </div>
            </fieldset>
          <?php endforeach;?>
          </div>
          <div class='buttonsArea' data-area='opt-out-employment'>
            <div class="checkbox">
              <label><input type="checkbox" class="opt-out" value="">No Employment History</label>
            </div>
            <button type="button" class="btn btn-default addButton">
              <span class="glyphicon glyphicon-plus"></span>
              Add Employment
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col col-md-8">
      <button type="button" class="btn btn-primary" id="update_button">
        <span class="glyphicon glyphicon-floppy-disk"></span>
        Save
      </button>
    </div>
  </div>
</form>
