<div class="row">
  <div class="col-md-12">
    <h5>Join us in our mission.</h5>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="javascript:void(0)" id="job_details_icon">
          <span class="glyphicon glyphicon-minus"></span> Job Details (<?php echo $job->job_title;?>)</a>
      </div>
      <div class="panel-body" id="job_details">
        <div class="form-group">
          <label>Job Title</label>
          <p><?php echo htmlspecialchars($job->job_title);?></p>
        </div>
        <div class="form-group">
          <label>Job Description</label>
          <p class="description"><?php echo htmlspecialchars($job->description);?></p>
        </div>
        <div class="form-group">
          <label>Location</label>
          <p><?php echo htmlspecialchars(sprintf("%s, %s (%s)", $job->location_city, $job->location_state, $job->facility_name));?></p>
        </div>
      </div>
    </div>
    <?php echo $form; ?>
  </div>
</div>
