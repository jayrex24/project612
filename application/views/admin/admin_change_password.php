<div class="col col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>Admin Change Password</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/admin/admin_change_password_ajax/'.$admin->id); ?>">
        <div class="form-group">
          <label for="old_password">*Old Password</label>
          <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Enter old password" required>
        </div>
        <div class="form-group">
          <label for="new_password">*New Password</label>
          <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter new password" required>
        </div>
        <div class="form-group">
          <label for="confirm_password">*Confirm Password</label>
          <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm password" required>
        </div>
        <button type="button" class="btn btn-primary" id="update_button">
          <span class="glyphicon glyphicon-floppy-disk"></span>
          Save
        </button>
      </form>
    </div>
  </div>
</div>
