<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Admin Change Permissions</h2>
      </div>
      <div class="panel-body">
        <div>
          <form data-url="<?php echo base_url('/admin/admin_change_permissions_ajax/'.$admin->id); ?>">
            <label class="form-check-label">
              Permissions
            </label>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox1" value="Employee Create"
              <?php echo in_array('Employee Create', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox1">Employee Create</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox2" value="Admin Create"
              <?php echo in_array('Admin Create', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox2">Admin Create</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox3" value="Admin Superuser"
              <?php echo in_array('Admin Superuser', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox3">Admin Superuser</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox4" value="Facility Create"
              <?php echo in_array('Facility Create', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox4">Facility Create</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox5" value="Job Create"
              <?php echo in_array('Job Create', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox5">Job Create</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox6" value="Convert Applicants"
              <?php echo in_array('Convert Applicants', $curr_permission) ? "checked" : "";?>>
              <label class="form-check-label" for="checkbox6">Convert Applicants</label>
            </div>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
        </form>
      </div>
    </div>
  </div>
</div>
