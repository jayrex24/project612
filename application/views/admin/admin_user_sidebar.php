<div class="col col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                Admin (ID: <?php echo $admin->id; ?>)
            </h2>
        </div>
        <div class="panel-collapse">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_info/'.$admin->id); ?>">Personal Information</a>
                </li>
                <?php if ($admin->id != $session['id']): ?>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_change_permissions/'.$admin->id); ?>">Change Permissions</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_change_status/'.$admin->id); ?>">Change Status</a>
                </li>
                <?php elseif ($admin->id == $session['id']): ?>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_view_permissions/'.$admin->id); ?>">View Permissions</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_view_status/'.$admin->id); ?>">View Status</a>
                </li>
                <?php endif; ?>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/admin/admin_change_password/'.$admin->id); ?>">Change Password</a>
                </li>
            </ul>
        </div>
    </div>
</div>
