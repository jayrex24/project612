<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Admin Change Status</h2>
      </div>
      <div class="panel-body">
        <div>
          <div class="form-group">
            <label for="status">*Admin Status</label>
            <select class="form-control" id="status" name="status" disabled>
                <option value="" disabled selected>
                  Please select an admin status</option>
                <?php foreach ($admin_status as $key => $status): ?>
                  <option value="<?php echo htmlspecialchars($key); ?>"
                    <?php echo $key == $admin->status ? "selected" : ""; ?>>
                    <?php echo htmlspecialchars($status); ?>
                  </option>
                <?php endforeach; ?>
            </select>
          </div>
      </div>
    </div>
  </div>
</div>
