<div class="col col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2>Add New Admin</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/admin/admin_add_user_ajax'); ?>">
        <div class="col col-md-8">
          <div class="form-group">
            <label for="employee_id">*Employee name</label>
            <select class="form-control select_state" name="employee_id" id="employee_id">
                <option value="" disabled selected>Please select an employee</option>
                <?php foreach ($employees as $employee):?>
                  <option value="<?php echo $employee->id;?>">
                    <?php echo sprintf(
                      '%s, %s (Employee ID: %d)',
                      htmlspecialchars($employee->last_name),
                      htmlspecialchars($employee->first_name),
                      $employee->id
                    ); ?>
                  </option>
                <?php endforeach;?>
            </select>
          </div>
          <div class="form-group">
            <label for="password">*Password</label>
            <input type="password" class="form-control" name="password" id="password"
                maxlength="50" minlength="8" placeholder="Enter password">
          </div>
          <div class="form-group">
            <label for="confirm_password">*Confirm Password</label>
            <input type="password" class="form-control" name="confirm_password" id="confirm_password"
                maxlength="50" minlength="8" placeholder="Confirm password">
          </div>
          <label class="form-check-label">
            Permissions
          </label>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox1" value="Employee Create">
            <label class="form-check-label" for="checkbox1">Employee Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox2" value="Admin Create">
            <label class="form-check-label" for="checkbox2">Admin Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox3" value="Admin Superuser">
            <label class="form-check-label" for="checkbox3">Admin Superuser</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox4" value="Facility Create">
            <label class="form-check-label" for="checkbox4">Facility Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="permissions[]" id="checkbox5" value="Job Create">
            <label class="form-check-label" for="checkbox5">Job Create</label>
          </div><br/>
          <button type="button" class="btn btn-primary" id="add_new_button">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Submit
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
