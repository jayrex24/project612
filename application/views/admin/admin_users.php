<div class="col col-md-12">
  <?php if (!isset($admin) || empty($admin)): ?>
    <div class="panel panel-default">
      <div class="panel-body">No admin currently exist.
        <?php if (permission_granted(array(A_C, A_S))): ?>
        Add new admin using the 'Add New Admin' button.
        <?php endif; ?>
      </div>
    </div>
  <?php else: ?>
    <div class="panel panel-default">
      <div class="panel-body">Click the link on the table rows to view/edit admin information.</div>
    </div>
    <table id="example" class="display responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Admin ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($admin as $value): ?>
            <tr>
                <td>
                  <a href="<?php echo base_url('/admin/admin_info/'.$value->id); ?>" title="View admin">
                    <?php echo $value->id; ?></a>
                </td>
                <td><?php echo htmlspecialchars($value->first_name); ?></td>
                <td><?php echo htmlspecialchars($value->last_name); ?></td>
                <td><?php echo $value->status == 0 ? "Inactive" : "Active"; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table><br/>
  <?php endif; ?>

  <?php if (permission_granted(array(A_C, A_S))): ?>
    <button class="btn btn-primary" onclick="location.href='<?php echo base_url('/admin/admin_add_user'); ?>';">
      <span class="glyphicon glyphicon-plus"></span>
        Add New Admin
    </button>
  <?php endif; ?>
</div>
