<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Admin Change Status</h2>
      </div>
      <div class="panel-body">
        <div>
          <form data-url="<?php echo base_url('/admin/admin_change_status_ajax/'.$admin->id); ?>">
            <div class="form-group">
              <label for="status">*Admin Status</label>
              <select class="form-control" id="status" name="status">
                  <option value="" disabled selected>
                    Please select an admin status</option>
                  <?php foreach ($admin_status as $key => $status): ?>
                    <option value="<?php echo htmlspecialchars($key); ?>"
                      <?php echo $key == $admin->status ? "selected" : ""; ?>>
                      <?php echo htmlspecialchars($status); ?>
                    </option>
                  <?php endforeach; ?>
              </select>
            </div>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
        </form>
      </div>
    </div>
  </div>
</div>
