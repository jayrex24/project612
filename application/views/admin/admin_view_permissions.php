<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Admin View Permissions</h2>
      </div>
      <div class="panel-body">
        <div>
          <label class="form-check-label">
            Permissions
          </label>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="Employee Create"
            <?php echo in_array('Employee Create', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox2">Employee Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="Admin Create"
            <?php echo in_array('Admin Create', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox5">Admin Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="Admin Superuser"
            <?php echo in_array('Admin Superuser', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox4">Admin Superuser</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Facility Create"
            <?php echo in_array('Facility Create', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox1">Facility Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="Job Create"
            <?php echo in_array('Job Create', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox3">Job Create</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="Convert Applicants"
            <?php echo in_array('Convert Applicants', $curr_permission) ? "checked" : "";?> disabled>
            <label class="form-check-label" for="inlineCheckbox6">Convert Applicants</label>
          </div>
      </div>
    </div>
  </div>
</div>
