<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Admin Personal Information</h2>
      </div>
      <div class="panel-body">
        <div>
          <div class="form-group">
            <label>First Name</label><br/>
            <span class="text-muted"><?php echo !empty($admin->first_name) ? htmlspecialchars($admin->first_name) : '-';?></span>
          </div>
          <div class="form-group">
            <label>Middle Initial</label><br/>
            <span class="text-muted"><?php echo !empty($admin->mid_init) ? htmlspecialchars($admin->mid_init) : '-';?></span>
          </div>
          <div class="form-group">
            <label>Last Name</label><br/>
            <span class="text-muted"><?php echo !empty($admin->last_name) ? htmlspecialchars($admin->last_name) : '-';?></span>
          </div>
          <div class="form-group">
            <label>Email</label><br/>
            <span class="text-muted"><?php echo !empty($admin->email) ? htmlspecialchars($admin->email) : '-';?></span>
          </div>
          <div class="form-group">
            <label>Date of Birth</label><br/>
            <span class="text-muted"><?php echo !empty($admin->date_of_birth) ? htmlspecialchars($admin->date_of_birth) : '-';?></span>
          </div>
          <div class="form-group">
            <label>Social Security Number</label><br/>
            <?php
              $ssn = substr($admin->ssn, -4);
              $ssn = "XXX-XX-" . $ssn;
            ?>
            <span class="text-muted"><?php echo htmlspecialchars($ssn); ?></span>
          </div>
          <div class="form-group">
            <label>Employment Date</label><br/>
            <span class="text-muted"><?php echo !empty($admin->employment_date) ? htmlspecialchars($admin->employment_date) : '-';?></span>
          </div>
          <button type="button" class="btn btn-primary" onclick="location.href='/employee/employee_info/<?php echo $admin->employee_id; ?>'">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Edit Information
          </button>
        </form>
      </div>
    </div>
  </div>
</div>
