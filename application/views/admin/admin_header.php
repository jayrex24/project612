<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="<?php echo base_url('/dashboard/home'); ?>" class="navbar-brand">Bethesda Mission HR</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url('/dashboard/home'); ?>" <?php echo isset($dash_active) == true ? "class='header_active'" : "";?>>Dashboard</a></li>
        <li><a href="<?php echo base_url('/facility/home'); ?>" <?php echo isset($fcl_active) == true ? "class='header_active'" : "";?>>Facilities</a></li>
        <li><a href="<?php echo base_url('/applicant/view'); ?>" <?php echo isset($apl_active) == true ? "class='header_active'" : "";?>>Applications</a></li>
        <li><a href="<?php echo base_url('/jobs/home'); ?>" <?php echo isset($job_active) == true ? "class='header_active'" : "";?>>Jobs</a></li>
        <li><a href="<?php echo base_url('/employee/employees'); ?>" <?php echo isset($emp_active) == true ? "class='header_active'" : "";?>>Employees</a></li>
        <li><a href="<?php echo base_url('/admin/home'); ?>" <?php echo isset($adm_active) == true ? "class='header_active'" : "";?>>Admins</a></li>
        <li class="dropdown ">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <?php echo htmlspecialchars($session['name_for_header']); ?>
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li class="dropdown-header">Actions</li>
            <li class=""><a href="<?php echo base_url('/admin/admin_info/' . $session['id']); ?>">View Your Profile</a></li>
            <li class=""><a href="<?php echo base_url('/admin/admin_change_password/' . $session['id']); ?>">Change Password</a></li>
            <li class="divider"></li>
            <?php if (permission_granted(array(A_C, A_S))): ?>
            <li class=""><a href="<?php echo base_url('/admin/admin_add_user'); ?>">Add New Admin</a></li>
            <?php endif; ?>
            <?php if (permission_granted(array(E_C, A_S))): ?>
            <li class=""><a href="<?php echo base_url('/employee/add_employee'); ?>">Add New Employee</a></li>
            <?php endif; ?>
            <?php if (permission_granted(array(F_C, A_S))): ?>
            <li class=""><a href="<?php echo base_url('/facility/add_new_facility'); ?>">Add New Facility</a></li>
            <?php endif; ?>
            <?php if (permission_granted(array(J_C, A_S))): ?>
            <li class=""><a href="<?php echo base_url('/jobs/add_new_position'); ?>">Add New Job Position</a></li>
            <?php endif; ?>
            <li class="divider"></li>
            <li class=""><a href="<?php echo base_url('/applicant/jobs'); ?>">View Applicant Job Page</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('/home/logout'); ?>">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="col col-md-12">

    <?php if ($success):?>
      <div class="alert alert-success alert-dismissable" style="display:block">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <strong>Success!</strong> <?php echo $success; ?>
      </div>
    <?php endif;?>

    <?php if ($error): ?>
      <div class="alert alert-danger alert-dismissable" style="display:block">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <?php echo $error; ?>
      </div>
    <?php endif; ?>

    <div id="messageDiv">
      <div class="alert alert-danger alert-dismissable" id="errorMsg"></div>
      <div class="alert alert-success alert-dismissable" id="successMsg"></div>
    </div>
  </div>
