<div class="col col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                Widgets
            </h2>
        </div>
        <div class="panel-body" id="dashboard_checkboxes">

            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="employee_by_state" id="employee_by_state"
                data-url="/dashboard/get_employee_count_by_state"
                  <?php echo isset($dashboard_selected) && in_array('employee_by_state', $dashboard_selected) ? "checked" : "" ;?>>
              <label class="form-check-label" for="employee_by_state">
                Employees By States
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="employee_by_status" id="employee_by_status"
                data-url="/dashboard/get_employee_count_by_status"
                  <?php echo isset($dashboard_selected) && in_array('employee_by_status', $dashboard_selected) ? "checked" : "" ;?>>
              <label class="form-check-label" for="employee_by_status">
                Employees By Status
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="applicants_by_month" id="applicants_by_month"
                data-url="/dashboard/get_applicant_count_by_month"
                  <?php echo isset($dashboard_selected) && in_array('applicants_by_month', $dashboard_selected) ? "checked" : "" ;?>>
              <label class="form-check-label" for="applicants_by_month">
                Applicants By Month
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="applications_by_day" id="applications_by_day"
                data-url="/dashboard/get_application_count_by_day"
                  <?php echo isset($dashboard_selected) && in_array('applications_by_day', $dashboard_selected) ? "checked" : "" ;?>>
              <label class="form-check-label" for="applicants_by_day">
                Applications By Day
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="jobs_by_facility" id="jobs_by_facility"
                data-url="/dashboard/get_jobs_count_by_facility"
                  <?php echo isset($dashboard_selected) && in_array('jobs_by_facility', $dashboard_selected) ? "checked" : "" ;?>>
              <label class="form-check-label" for="jobs_by_facility">
                Jobs By Facility
              </label>
            </div>
        </div>
    </div>
</div>
<div class="col col-md-9">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Dashboard Widgets</h2>
      </div>
      <div class="panel-body" id="dashboard_main">
        <p class="text-muted">Select a checkbox to add a dashboard widget.</p>
      </div>
    </div>
</div>
