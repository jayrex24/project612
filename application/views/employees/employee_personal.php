<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Personal Information</h2>
      </div>
      <div class="panel-body">
        <div>
          <form data-url="<?php echo base_url('/employee/employee_info_ajax/'. $employee_id); ?>">
            <div class="form-group">
              <label for="first_name">*First Name</label>
              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter first name"
                value="<?php echo $employee_info->first_name != "" ? htmlspecialchars($employee_info->first_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="mid_init">Middle Initial</label>
              <input type="text" class="form-control" name="mid_init" id="mid_init" placeholder="Enter middle initial" maxlength="1"
                value="<?php echo $employee_info->mid_init != "" ? htmlspecialchars($employee_info->mid_init) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="last_name">*Last Name</label>
              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter last name"
                value="<?php echo $employee_info->last_name != "" ? htmlspecialchars($employee_info->last_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="email">*Email</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"
               value="<?php echo $employee_info->email != "" ? htmlspecialchars($employee_info->email) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="date_of_birth">*Date of Birth <span class="smallfont text-muted">(YYYY-MM-DD)</span></label>
              <input type="text" class="form-control min_age" name="date_of_birth" id="date_of_birth" placeholder='Enter date of birth'
                value="<?php echo $employee_info->date_of_birth != "" ? htmlspecialchars($employee_info->date_of_birth) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="ssn">*Social Security Number  <span class="smallfont text-muted">(XXX-XX-XXXX)</span></label>
              <input type="password" class="form-control ssn" name="ssn" id="ssn" placeholder='Enter Social Security Number'
                value="<?php echo $employee_info->ssn != "" ? htmlspecialchars($employee_info->ssn) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="employment_date">*Employment Date <span class="smallfont text-muted">(YYYY-MM-DD)</span></label>
              <input type="text" class="form-control calendar" id="employment_date" name="employment_date" placeholder='Enter employment date'
                value="<?php echo $employee_info->employment_date != "" ? htmlspecialchars($employee_info->employment_date) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="type">*Employment Status</label>
              <select class="form-control" id="employment_status" name="employment_status">
                  <option value="" disabled selected>
                    Please select an employment status</option>
                  <?php foreach ($employment_status as $status): ?>
                    <option value="<?php echo htmlspecialchars($status); ?>"
                      <?php echo $status == $employee_info->employment_status ? "selected" : ""; ?>>
                      <?php echo htmlspecialchars($status); ?>
                    </option>
                  <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label for="created_date">Employee Created Date</label>
              <input type="text" class="form-control" id="created_date"
                value="<?php echo $employee_info->created_date != "" ? htmlspecialchars($employee_info->created_date) : ""; ?>" disabled>
            </div>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
          </form>
        </div>
      </div>
  </div>
</div>
