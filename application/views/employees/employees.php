<div class="col col-md-12">
  <?php if (!isset($employees) || empty($employees)): ?>
    <div class="panel panel-default">
      <div class="panel-body">No employees currently exist.
        <?php if (permission_granted(array(E_C, A_S))): ?>
          Add new employee using the 'Add New Employee' button.
        <?php endif; ?>
      </div>
    </div>
  <?php else: ?>
    <div class="panel panel-default">
      <div class="panel-body">Click the link on the table rows to view/edit employee information.</div>
    </div>
    <table id="example" class="display responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Date of Birth</th>
                <th>Employment Date</th>
                <th>Employment Status</th>
            </tr>
        </thead>
        <tbody><?php foreach ($employees as $value): ?>
            <tr>
                <td>
                  <a href="<?php echo base_url('/employee/employee_info/' . $value->id); ?>" title="View employee">
                    <?php echo $value->id; ?></a>
                </td>
                <td><?php echo htmlspecialchars($value->first_name); ?></td>
                <td><?php echo htmlspecialchars($value->last_name); ?></td>
                <td><?php echo htmlspecialchars($value->email); ?></td>
                <td><?php echo htmlspecialchars($value->date_of_birth); ?></td>
                <td><?php echo htmlspecialchars($value->employment_date); ?></td>
                <td><?php echo htmlspecialchars($value->employment_status); ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table><br/>
  <?php endif; ?>

  <?php if (permission_granted(array(E_C, A_S))): ?>
  <button class="btn btn-primary" onclick="location.href='<?php echo base_url('/employee/add_employee'); ?>'">
    <span class="glyphicon glyphicon-plus"></span>
      Add New Employee
  </button>
  <?php endif; ?>
</div>
