<div class="col col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h2>Education Information</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/employee/employee_education_ajax/'.$employee_id); ?>">
        <div class="add-to">
        <?php foreach ($employee_education as $key => $education):
          $index = $key + 1; ?>
          <fieldset>
            <legend>Education #<span class="legend_id"><?php echo $index; ?></span>
              <button type="button" class="btn btn-danger remove_div btn-xs">
                Remove
              </button>
            </legend>
            <div class="form-group">
              <label for="institution_<?php echo $index; ?>">*School/Institution</label>
              <input type="text" class="form-control" id="institution_<?php echo $index; ?>"
                name="education[<?php echo $key; ?>][institution]" placeholder="Enter School/Institution name"
                value="<?php echo $education->institution != "" ? htmlspecialchars($education->institution) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="degreeCertification_<?php echo $index; ?>">*Degree/Certification</label>
              <select class="form-control select_type" id="degreeCertification_<?php echo $index; ?>"
                  name="education[<?php echo $key; ?>][degree_certification]">
                  <option value="" disabled <?php echo empty($education->degree_certification) ? "selected" : ""; ?>>
                    Please select an education type</option>
                  <?php foreach ($education_types as $type): ?>
                    <option value="<?php echo htmlspecialchars($type); ?>"
                      <?php echo $type == $education->degree_certification ? "selected" : ""; ?>>
                      <?php echo htmlspecialchars($type); ?>
                    </option>
                  <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label for="fromDate_<?php echo $index; ?>">*From Date</label>
              <input type="text" class="form-control calendar" id="fromDate_<?php echo $index; ?>"
                name="education[<?php echo $key; ?>][from_date]" placeholder="Enter From Date"
                value="<?php echo $education->from_date != "" ? htmlspecialchars($education->from_date) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="toDate_<?php echo $index; ?>">*To Date</label> &nbsp;
              <input type="checkbox" class="presentBox" id="toDatePresentEducation_<?php echo $index; ?>" name="education[<?php echo $key; ?>][present]" value="0"> Present
              <input type="text" class="form-control calendar to-date" id="toDate_<?php echo $index; ?>"
                name="education[<?php echo $key; ?>][to_date]" placeholder="Enter To Date"
                value="<?php echo $education->to_date != "" ? htmlspecialchars($education->to_date) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="comments_<?php echo $index; ?>">Comments</label>
              <textarea class="form-control" rows="5" id="comments_<?php echo $index; ?>"
                name="education[<?php echo $key; ?>][comments]"
                placeholder="Enter comments"><?php echo $education->comments != "" ? htmlspecialchars($education->comments) : "";?></textarea>
            </div>
          </fieldset>
        <?php endforeach;?>
        </div>
        <div class='buttonsArea' data-area='opt-out-education'>
          <div class="checkbox">
            <label><input type="checkbox" class="opt-out" value="">No Education</label>
          </div>
          <button type="button" class="btn btn-default addButton">
            <span class="glyphicon glyphicon-plus"></span>
            Add Education
          </button>
          <button type="button" class="btn btn-primary" id="update_button">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
