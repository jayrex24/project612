<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Address Information</h2>
      </div>
      <div class="panel-body">
        <div>
          <form data-url="<?php echo base_url('/employee/employee_address_ajax/'. $employee_id); ?>">
            <div class="form-group">
              <label for="addr1">*Address 1</label>
              <input type="text" class="form-control" name="addr1" id="addr1" placeholder='Enter Address 1'
                value="<?php echo $employee_address->addr1 != "" ? htmlspecialchars($employee_address->addr1) : "";?>">
            </div>
            <div class="form-group">
              <label for="addr2">Address 2</label>
              <input type="text" class="form-control" name="addr2" id="addr2" placeholder='Enter Address 2'
                value="<?php echo $employee_address->addr2 != "" ? htmlspecialchars($employee_address->addr2) : "";?>">
            </div>
            <div class="form-group">
              <label for="city">*City</label>
              <input type="text" class="form-control" name="city" id="city" placeholder='Enter city'
                value="<?php echo $employee_address->city != "" ? htmlspecialchars($employee_address->city) : "";?>">
            </div>
            <div class="form-group">
              <label for="state">*State</label>
              <select class="form-control select_state" name="state" id="state">
                  <option value="" disabled>Please select a state</option>
                  <?php foreach ($states as $state):?>
                    <option value="<?php echo $state;?>"
                      <?php echo $state == $employee_address->state ? "selected" : "";?>>
                      <?php echo $state;?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <div class="form-group">
              <label for="zip_code">*Zip Code</label>
              <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="Enter zip code" maxlength="5"
                value="<?php echo $employee_address->zip_code != "" ? htmlspecialchars($employee_address->zip_code) : "";?>">
            </div>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
          </form>
        </div>
      </div>
  </div>
</div>
