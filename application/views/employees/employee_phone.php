<div class="col col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h2>Phone Information</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/employee/employee_phone_ajax/'. $employee_id); ?>">
        <div class="add-to">
        <?php foreach ($phones as $key => $phone):
          $index = $key + 1; ?>
          <fieldset>
            <legend>Phone #<span class="legend_id"><?php echo $index; ?></span>
              <button type="button" class="btn btn-danger remove_div btn-xs">
                Remove
              </button>
            </legend>
            <div class="form-group">
              <label for="phoneNumber_1">*Phone <span class="smallfont text-muted">(XXX-XXX-XXXX)</span></label>
              <input type="text" class="form-control phone_number" id="phoneNumber_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][phone_number]"
                value="<?php echo htmlspecialchars($phone->phone_number); ?>" placeholder='Enter phone number'>
            </div>
            <div class="form-group">
              <label for="extension_1">Extension</label>
              <input type="text" class="form-control" id="extension_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][extension]"
                value="<?php echo htmlspecialchars($phone->extension); ?>" placeholder='Enter extension'>
            </div>
            <div class="form-group">
              <label for="type">Phone Type</label>
              <select class="form-control select_type" id="phoneType_<?php echo $index; ?>" name="phone[<?php echo $key; ?>][phone_type]">
                  <option value="" disabled <?php echo empty($phone->phone_type) ? "selected" : ""; ?>>
                    Please select a phone type</option>
                  <?php foreach ($phone_types as $type): ?>
                    <option value="<?php echo htmlspecialchars($type); ?>"
                      <?php echo $type == $phone->phone_type ? "selected" : ""; ?>>
                      <?php echo htmlspecialchars($type); ?>
                    </option>
                  <?php endforeach; ?>
              </select>
            </div>
          </fieldset>
        <?php endforeach; ?>
        </div>
        <div class='buttonsArea' data-area='opt-out-phone'>
          <div class="checkbox">
            <label><input type="checkbox" class="opt-out" value="">No Phone</label>
          </div>
          <button type="button" class="btn btn-default addButton">
            <span class="glyphicon glyphicon-plus"></span>
            Add Phone
          </button>
          <button type="button" class="btn btn-primary" id="update_button">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
