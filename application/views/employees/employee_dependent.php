<div class="col col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h2>Dependent Information</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/employee/employee_dependent_ajax/'.$employee_id); ?>">
        <div class="add-to">
        <?php foreach ($dependents as $key => $dependent):
          $index = $key + 1; ?>
          <fieldset>
            <legend>Dependent #<span class="legend_id"><?php echo $index; ?></span>
              <button type="button" class="btn btn-danger remove_div btn-xs">
                Remove
              </button>
            </legend>
            <div class="form-group">
              <label for="firstName_<?php echo $index; ?>">*First Name</label>
              <input type="text" class="form-control" id="firstName_<?php echo $index; ?>"
                name="dependent[<?php echo $key; ?>][first_name]" placeholder="Enter first name"
                value="<?php echo $dependent->first_name != "" ? htmlspecialchars($dependent->first_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="middleInitial_<?php echo $index; ?>">Middle Initial</label>
              <input type="text" class="form-control" id="middleInitial_<?php echo $index; ?>" maxlength="1"
                name="dependent[<?php echo $key; ?>][mid_init]" placeholder="Enter middle initial"
                value="<?php echo $dependent->mid_init != "" ? htmlspecialchars($dependent->mid_init) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="lastName_<?php echo $index; ?>">*Last Name</label>
              <input type="text" class="form-control" id="lastName_<?php echo $index; ?>"
                name="dependent[<?php echo $key; ?>][last_name]" placeholder="Enter last name"
                value="<?php echo $dependent->last_name != "" ? htmlspecialchars($dependent->last_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="relationship_<?php echo $index; ?>">*Relationship</label>
              <select class="form-control select_type" id="relationship_<?php echo $index; ?>"
                  name="dependent[<?php echo $key; ?>][relationship]">
                  <option value="" disabled <?php echo empty($dependent->relationship) ? "selected" : ""; ?>>
                    Please select a relationship</option>
                  <?php foreach ($relationship_types as $type): ?>
                    <option value="<?php echo htmlspecialchars($type); ?>"
                      <?php echo $type == $dependent->relationship ? "selected" : ""; ?>>
                      <?php echo htmlspecialchars($type); ?>
                    </option>
                  <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label for="ssn_<?php echo $index; ?>">*Social Security Number</label>
              <input type="password" class="form-control ssn" id="ssn_<?php echo $index; ?>"
                name="dependent[<?php echo $key; ?>][ssn]" placeholder="Enter Social Security Number"
                value="<?php echo $dependent->ssn != "" ? htmlspecialchars($dependent->ssn) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="dob_<?php echo $index; ?>">*Date of Birth</label>
              <input type="text" class="form-control calendar" id="dob_<?php echo $index; ?>"
                name="dependent[<?php echo $key; ?>][date_of_birth]" placeholder="Enter Date of Birth"
                value="<?php echo $dependent->date_of_birth != "" ? htmlspecialchars($dependent->date_of_birth) : ""; ?>">
            </div>
          </fieldset>
        <?php endforeach;?>
        </div>
        <div class='buttonsArea' data-area='opt-out-dependent'>
          <div class="checkbox">
            <label><input type="checkbox" class="opt-out" value="">No Dependent</label>
          </div>
          <button type="button" class="btn btn-default addButton">
            <span class="glyphicon glyphicon-plus"></span>
            Add Dependents
          </button>
          <button type="button" class="btn btn-primary" id="update_button">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
