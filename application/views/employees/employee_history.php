<div class="col col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h2>Employment History</h2>
    </div>
    <div class="panel-body">
      <form data-url="<?php echo base_url('/employee/employee_history_ajax/'.$employee_id); ?>">
        <div class="add-to">
        <?php foreach ($employee_history as $key => $history):
          $index = $key + 1; ?>
          <fieldset>
            <legend>Employment #<span class="legend_id"><?php echo $index; ?></span>
              <button type="button" class="btn btn-danger remove_div btn-xs">
                Remove
              </button>
            </legend>
            <div class="form-group">
              <label for="institution_<?php echo $index; ?>">*Company/Institution</label>
              <input type="text" class="form-control" id="institution_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][institution]" placeholder="Enter Company/Institution name"
                value="<?php echo $history->institution != "" ? htmlspecialchars($history->institution) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="position_<?php echo $index; ?>">*Position</label>
              <input type="text" class="form-control" id="position_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][job_title]" placeholder="Enter position name"
                value="<?php echo $history->job_title != "" ? htmlspecialchars($history->job_title) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="supervisor_<?php echo $index; ?>">Supervisor name</label>
              <input type="text" class="form-control" id="supervisor_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][supervisor_name]" placeholder="Enter supervisor name"
                value="<?php echo $history->supervisor_name != "" ? htmlspecialchars($history->supervisor_name) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="fromDate_<?php echo $index; ?>">*From Date</label>
              <input type="text" class="form-control calendar" id="fromDate_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][from_date]" placeholder="Enter From Date"
                value="<?php echo $history->from_date != "" ? htmlspecialchars($history->from_date) : ""; ?>">
            </div>
            <div class="form-group">
              <label for="toDate_<?php echo $index; ?>">*To Date</label> &nbsp;
              <input type="checkbox" class="presentBox" id="toDatePresentEmployment_<?php echo $index; ?>" name="employment[<?php echo $key; ?>][present]" value="0"> Present
              <input type="text" class="form-control calendar to-date" id="toDate_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][to_date]" placeholder="Enter To Date"
                value="<?php echo $history->to_date != "" ? htmlspecialchars($history->to_date) : ""; ?>">
            </div>
            <div class="form-group reason_for_leaving">
              <label for="reasonForLeaving_<?php echo $index; ?>">*Reason for Leaving</label>
              <textarea class="form-control" rows="5" id="reasonForLeaving_<?php echo $index; ?>"
                name="employment[<?php echo $key; ?>][reason_for_leaving]"
                placeholder="Enter comments"><?php echo $history->reason_for_leaving != "" ? htmlspecialchars($history->reason_for_leaving) : "";?></textarea>
            </div>
          </fieldset>
        <?php endforeach;?>
        </div>
        <div class='buttonsArea' data-area='opt-out-employment'>
          <div class="checkbox">
            <label><input type="checkbox" class="opt-out" value="">No Employment History</label>
          </div>
          <button type="button" class="btn btn-default addButton">
            <span class="glyphicon glyphicon-plus"></span>
            Add Employment
          </button>
          <button type="button" class="btn btn-primary" id="update_button">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
