<div class="col col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">
                Employee (ID: <?php echo $employee_id; ?>)
            </h2>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_info/' . $employee_id); ?>">Personal Information</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_address/' . $employee_id); ?>">Address Information</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_history/' . $employee_id); ?>">Employment History</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_education/' . $employee_id); ?>">Education Information</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_phone/' . $employee_id); ?>">Phone Information</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_dependent/' . $employee_id); ?>">Dependent Information</a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo base_url('/employee/employee_jobchange/' . $employee_id); ?>">Job Assignment</a>
                </li>
            </ul>
        </div>
    </div>
</div>
