<div class="col col-md-6">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Job Assignment</h2>
      </div>
      <div class="panel-body">
        <div>
          <form data-url="<?php echo base_url('/employee/employee_jobchange_ajax/'. $employee_id); ?>">
            <div class="form-group">
              <label for="job_id">*Job Assignment</label>
              <select class="form-control" name="job_id" id="job_id">
                  <?php if (isset($current_job->id)) : ?>
                    <option value="x">Please select a position</option>
                    <option value="<?php echo htmlspecialchars($current_job->id); ?>" selected>
                      <?php echo htmlspecialchars(sprintf("%s (ID: %d)", $current_job->job_title, $current_job->id));?>
                    </option>
                  <?php else: ?>
                    <option value="x" selected>Please select a position</option>
                  <?php endif; ?>
                  <?php foreach ($jobs as $job):?>
                    <option value="<?php echo $job->id;?>">
                      <?php echo htmlspecialchars(sprintf("%s (ID: %d)", $job->job_title, $job->id));?>
                    </option>
                  <?php endforeach;?>
              </select>
            </div>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
          </form>
        </div>
      </div>
  </div>
</div>
