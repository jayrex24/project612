<div class="col col-md-12">
  <?php if (!isset($jobs) || empty($jobs)): ?>
    <div class="panel panel-default">
      <div class="panel-body">No job currently exist. Add new job opening using the 'Add New Position' button.</div>
    </div>
  <?php else: ?>
    <div class="panel panel-default">
      <div class="panel-body">Click the link on the table rows to view/edit job information.</div>
    </div>
    <table id="job_table" class="display responsive" style="width:100%">
        <thead>
            <tr>
                <th>Job ID</th>
                <th>Job Title</th>
                <th>Facility</th>
                <th>Assigned To</th>
                <th>Created Date</th>
            </tr>
        </thead>
        <tbody><?php foreach ($jobs as $job): ?>
            <tr>
                <td>
                  <a href="<?php echo base_url('/jobs/job_info/' . $job->id); ?>" title="View job">
                    <?php echo $job->id; ?></a>
                </td>
                <td><?php echo htmlspecialchars($job->job_title); ?></td>
                <td><?php echo isset($job->facility_name) && $job->facility_name != "" ? htmlspecialchars($job->facility_name) : "--"; ?></td>
                <td><?php echo !empty($job->first_name) || !empty($job->last_name) ? htmlspecialchars($job->first_name." ".$job->last_name) : "--"; ?></td>
                <td><?php echo htmlspecialchars($job->created_date); ?></td>

            </tr>
            <?php endforeach ?>
        </tbody>
    </table><br/>
  <?php endif; ?>

  <?php if (permission_granted(array(J_C, A_S))): ?>
  <button class="btn btn-primary" onclick="location.href='<?php echo base_url('/jobs/add_new_position'); ?>'">
    <span class="glyphicon glyphicon-plus"></span>
      Add New Position
  </button>
  <?php endif; ?>
</div>
