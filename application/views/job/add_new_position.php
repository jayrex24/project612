<div class="col col-md-9">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Add New Job Position</h2>
      </div>
      <div class="panel-body">
        <div class="col col-md-8">
          <form data-url="<?php echo base_url('/jobs/add_new_position_ajax'); ?>" method="POST">
            <div class="form-group">
              <label for="job_title">*Job Title</label>
              <input type="text" class="form-control" name="job_title" id="job_title" placeholder="Enter job title">
            </div>

            <div class="form-group">
              <label for="description">*Description</label>
              <textarea class="form-control" name="description" id="description" rows="10"
                  placeholder="Enter job description"></textarea>
            </div>

            <div class="form-group">
              <label for="facility_id">*Facility Name</label>
              <select class="form-control" name="facility_id" id="facility_id">
                <option value="" selected disabled>Please select a facility</option>
                <?php foreach ($facilities as $facility) : ?>
                    <option value="<?php echo $facility->id; ?>">
                      <?php echo htmlspecialchars($facility->facility_name); ?>
                    </option>
                <?php endforeach; ?>
              </select>
            </div>

            <button type="button" class="btn btn-primary" id="add_new_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Submit
            </button>
          </form>
        </div>
      </div>
  </div>
</div>
