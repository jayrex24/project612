<div class="col col-md-9">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h2>Job Information (ID: <?php echo $job_info->id; ?>)</h2>
      </div>
      <div class="panel-body">
        <div class="col col-md-8">
          <form data-url="<?php echo base_url('/jobs/job_info_ajax/'. $job_info->id); ?>">
            <div class="form-group">
              <label for="job_title">*Job Title</label>
              <input type="text" class="form-control" name="job_title" id="job_title" placeholder="Enter job title"
                value="<?php echo $job_info->job_title != "" ? htmlspecialchars($job_info->job_title) : ""; ?>">
            </div>

            <div class="form-group">
              <label for="description">*Description</label>
              <textarea class="form-control" name="description" id="description" rows="10"
              placeholder="Enter job description"><?php echo $job_info->description != "" ? htmlspecialchars($job_info->description) : ""; ?></textarea>
            </div>

            <div class="form-group">
              <label for="facility_id">*Facility Name</label>
              <select class="form-control" name="facility_id" id="facility_id">
                <option value="" disabled>Please select a facility</option>
                <?php foreach ($facilities as $facility) : ?>
                    <option value="<?php echo $facility->id; ?>"
                        <?php echo $facility->id == $job_info->facility_id ? "selected" : "";?>>
                      <?php echo htmlspecialchars($facility->facility_name); ?>
                    </option>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for="employee_assinged">Employee Assigned</label>
                <?php
                  $employee_name = "No Employee Assinged";
                  if (!empty($employee_assinged)) {
                      $employee_name = sprintf(
                          "%s, %s (Employee ID: %d)",
                          htmlspecialchars($employee_assinged->last_name),
                          htmlspecialchars($employee_assinged->first_name),
                          $employee_assinged->id
                      );
                  }
                ?>
                <input type="text" class="form-control" id="employee_assinged"
                  value="<?php echo htmlspecialchars($employee_name); ?>" disabled>
            </div>

            <?php if (permission_granted(array(J_C, A_S))): ?>
            <button type="button" class="btn btn-primary" id="update_button">
              <span class="glyphicon glyphicon-floppy-disk"></span>
              Save
            </button>
            <?php else: ?>
              <script>
                    $("input, select").attr("disabled", true);
              </script>
            <?php endif; ?>
          </form>
        </div>
      </div>
  </div>
</div>
