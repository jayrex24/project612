<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?php echo base_url('/assets/images/logo.png'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/source/Bootstrap3/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/source/DataTables/css/jquery.dataTables.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/source/Responsive/css/responsive.dataTables.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/source/jQuery-ui/jquery-ui.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/source/jQuery-ui/jquery-ui-1-12-1.css'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/style.css'); ?>">

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/source/jQuery/jquery-3.3.1.min.js'); ?>"></script>

	<title><?php echo $title; ?></title>
</head>
<body>
