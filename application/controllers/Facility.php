<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facility extends CI_Controller
{
    public $active = true;

    public function __construct()
    {
        parent::__construct();
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);
    }

    public function home()
    {
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Facilities' . SITE_TITLE,
            'success' => $success,
            'error' => $error,
            'facilities' => $this->facility_model->get_all_facilities(),
            'fcl_active' => $this->active,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('facility/facility');
        $this->load->view('footer');
    }

    public function facility_info($facility_id = null)
    {
        if (!$facility_info = $this->facility_model->get_facility_by_id($facility_id)) {
            $this->session->set_flashdata('error', 'Facility does not exist.');
            redirect(base_url('/facility/home'), 'refresh');
        }

        if (!permission_granted(array(F_C, A_S))) {
            $this->session->set_flashdata('error', "You do not have proper permissions to edit facility information.");
            redirect(base_url('/facility/home'), 'refresh');
        }

        $help = array(
            $this->definitions->required_help(),
            "<span>Update facility information. Click save to process changes.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Facility Manager</h4>
                <span>Select employee that will be managing the facility.</span>
              </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Facility ID: $facility_id" . SITE_TITLE,
            'error' => $this->session->flashdata('error'),
            'success' => $this->session->flashdata('success'),
            'employees' => $this->employee_model->get_all(),
            'facility_info' => $facility_info,
            'states' => $this->selection->get_states(),
            'fcl_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('facility/facility_info');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function facility_info_ajax($facility_id)
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(F_C, A_S))) {
            handleError($data, "You do not have proper permissions to edit facility information.", false);
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['facility_name'])) {
                handleError($data, "Facility name required.", false, 'facility_name');
            }

            if (empty($post_data['location_state'])) {
                handleError($data, "Location state required.", false, 'location_state');
            }

            if (empty($post_data['location_city'])) {
                handleError($data, "Location city required.", false, 'location_city');
            }

            if (empty($post_data['manager_employee_id'])) {
                handleError($data, "Facility manager required.", false, 'manager_employee_id');
            }

            if ($data['success']) {
                if ($this->facility_model->set_facility_info($facility_id, $post_data)) {
                    handleError($data, "Successfully updated facility information.", true);
                } else {
                    handleError($data, "Failed to update facility information", false);
                }
            }
        } else {
            handleError($data, "Unable to update facility information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function add_new_facility()
    {
        $help = array(
            $this->definitions->required_help(),
            "<span>Add new facility. Click submit to process new facility.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Facility Manager</h4>
                <span>Select employee that will be managing the facility.</span>
              </div>"
        );

        if (!permission_granted(array(F_C, A_S))) {
            $this->session->set_flashdata('error', "You do not have proper permissions to add a new facility.");
            redirect(base_url('/facility/home'), 'refresh');
        }

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Add New Facility' . SITE_TITLE,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'states' => $this->selection->get_states(),
            'employees' => $this->employee_model->get_all(),
            'fcl_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('facility/add_new_facility');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function add_new_facility_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(F_C, A_S))) {
            handleError($data, "You do not have proper permissions to add new facility.", false);
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['facility_name'])) {
                handleError($data, "Facility name required.", false, 'facility_name');
            }

            if (empty($post_data['location_state'])) {
                handleError($data, "Location state required.", false, 'location_state');
            }

            if (empty($post_data['location_city'])) {
                handleError($data, "Location city required.", false, 'location_city');
            }

            if (empty($post_data['manager_employee_id'])) {
                handleError($data, "Facility manager required.", false, 'manager_employee_id');
            }

            if ($data['success']) {
                if ($facility_id = $this->facility_model->add_new_facility($post_data)) {
                    $data['redirect_url'] = base_url("/facility/home");
                    $data['path'] = base_url("/facility/facility_info/$facility_id");
                    handleError($data, "New facility added.", true);
                } else {
                    handleError($data, "Failed to add new facility.", false);
                }
            }
        } else {
            handleError($data, "Unable to add new facility. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }
}
