<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reset extends CI_Controller
{
    public function forgot_password()
    {
        $page_data = array(
            'title' => 'Reset Password' . SITE_TITLE,
            'page' => "reset"
        );

        $this->load->view('header', $page_data);
        $this->load->view('login/forgot_password');
        $this->load->view('footer');
    }

    public function password_retrieve_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['forgot_email'])) {
                handleError($data, "Email required.", false, 'forgot_email');
                echo json_encode($data);
                exit();
            } elseif (!filter_var($post_data['forgot_email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'forgot_email');
                echo json_encode($data);
                exit();
            }

            if ($data['success'] && $this->employee_model->employee_email_exists($post_data['forgot_email'])) {
                // Create tokens
                $selector = bin2hex(random_bytes(8));
                $token = random_bytes(32);

                $url = sprintf('%sreset/reset_password?%s', base_url(), http_build_query(['selector' => $selector, 'validator' => bin2hex($token)]));

                // Token expiration
                $expires = new DateTime('NOW');
                $expires->add(new DateInterval('PT01H')); // 1 hour

                if ($admin = $this->admin_model->get_admin_by_email($post_data['forgot_email'])) {
                    // Delete any existing tokens for this user
                    $this->admin_model->delete_token($admin->id);

                    // Update reset token in database
                    if ($this->admin_model->update_token($admin->id, $selector, hash('sha256', $token), $expires->format('U'))) {
                        ob_start();
                        send_reset_email($url, $post_data['forgot_email']);
                        ob_end_clean();

                        handleError($data, "Your reset link has been sent.", true);
                    }
                } else {
                    handleError($data, "Your reset link has been sent.", true); //just kidding
                }
            } else {
                handleError($data, "Your reset link has been sent.", true); //just kidding
            }
        } else {
            handleError($data, "Unable to process request. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function reset_password()
    {
        $selector = filter_input(INPUT_GET, 'selector');
        $validator = filter_input(INPUT_GET, 'validator');

        $allow_reset = false;
        if (false !== ctype_xdigit($selector) && false !== ctype_xdigit($validator)) {
            $allow_reset = true;
        }

        if (!$allow_reset) {
            redirect(base_url());
        }

        $page_data = array(
            'title' => 'Reset Password' . SITE_TITLE,
            'page' => "reset",
            'selector' => filter_input(INPUT_GET, 'selector'),
            'validator' => filter_input(INPUT_GET, 'validator')
        );

        $this->load->view('header', $page_data);
        $this->load->view('login/password_reset');
        $this->load->view('footer');
    }

    public function password_reset_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'password');
            } elseif (strlen($post_data['password']) < 8) {
                handleError($data, "Password must be atleast 8 characters.", false, 'password');
            }

            if (empty($post_data['selector']) || empty($post_data['validator'])) {
                handleError($data, "Unable to complete request. Error code 001", false);
            }

            if ($data['success']) {
                if ($result = $this->admin_model->get_token($post_data['selector'], time())) {
                    $calc = hash('sha256', hex2bin($post_data['validator']));

                    // Validate tokens
                    if (hash_equals($calc, $result->token)) {

                        // Update password
                        if ($this->admin_model->update_password($result->id, $post_data['password'])) {
                            $this->admin_model->delete_token($result->id);
                            $data['redirect_url'] = base_url();
                            session_destroy();
                        }
                    } else {
                        handleError($data, "There was an error processing your request. Error code 002", false);
                    }
                } else {
                    handleError($data, "There was an error processing your request. Error code 003", false);
                }
            }
        }

        echo json_encode($data);
        exit();
    }
}
