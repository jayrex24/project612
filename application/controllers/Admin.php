<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public $active = true;

    public function __construct()
    {
        parent::__construct();
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);
    }

    public function home()
    {
        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Admin User Home' . SITE_TITLE,
            'admin' => $this->admin_model->get_all(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_users');
        $this->load->view('footer');
    }

    public function admin_info($admin_id = null)
    {
        if (!$admin_id || !$this->admin_model->check_id($admin_id)) {
            $this->session->set_flashdata('error', "Admin does not exist.");
            redirect(base_url('admin/home'), 'refresh');
        }

        if (!permission_granted(array(A_S)) && $this->session->id != $admin_id) {
            $this->session->set_flashdata('error', "You do not have permission to access other admin profiles.");
            redirect(base_url('admin/home'), 'refresh');
        }

        $who = $this->session->id == $admin_id ? "your" : "admin";

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $this->admin_model->get_admin_info($admin_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => array(
              "<span>View $who information. Click 'Edit Information' button to make changes to employee data.</span>"),
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_info');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_view_permissions($admin_id = null)
    {
        if ($this->session->id != $admin_id) {
            redirect(base_url('admin/admin_change_permissions/'.$admin_id), 'refresh');
        }

        $help = array(
            "<span>Viewing your permissions. You cannot modify your own permissions.
            Please contact your administrator if you wish to update your permissions.</span><br/><br/>",
            $this->definitions->permission_help()
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $this->admin_model->get_admin_info($admin_id),
            'curr_permission' => $this->admin_model->get_permissions($admin_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_view_permissions');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_change_permissions($admin_id = null)
    {
        //check if admin exist
        if (!$admin_id || !$this->admin_model->check_id($admin_id)) {
            $this->session->set_flashdata('error', "Admin does not exist.");
            redirect(base_url('admin/home'), 'refresh');
        }

        //admin should not be able to edit their own permission
        if ($this->session->id == $admin_id) {
            redirect(base_url('admin/admin_view_permissions/'.$admin_id), 'refresh');
        }

        if (!permission_granted(array(A_S))) {
            $this->admin_permission_check(array(A_S), "access other admin profiles.");
        }

        $who = $this->session->id == $admin_id ? "your" : "admin";

        $help = array(
            "<span>Update $who permissions.</span><br/><br/>",
            $this->definitions->permission_help()
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $this->admin_model->get_admin_info($admin_id),
            'curr_permission' => $this->admin_model->get_permissions($admin_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_change_permissions');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_change_permissions_ajax($admin_id = null)
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(A_S)) || ($this->session->id == $admin_id)) {
            handleError($data, "You do not have permission to change admin permissions.", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($admin_id)) {
                handleError($data, "Admin ID not found.", false);
            }

            $permissions = array();
            if (!empty($post_data['permissions'])) {
                $permissions = $post_data['permissions'];
            }

            if ($data['success']) {
                if ($this->admin_model->update_permission($admin_id, $permissions)) {
                    handleError($data, "Permissions updated.", true);
                } else {
                    handleError($data, "Failed to update permissions.", false);
                }
            }
        } else {
            handleError($data, "Unable to change permissions. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function admin_view_status($admin_id = null)
    {
        if ($this->session->id != $admin_id) {
            redirect(base_url('admin/admin_change_status/'.$admin_id), 'refresh');
        }

        $help = array(
            "<span>Viewing your status. You cannot modify your own status.</span><br/><br/>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $this->admin_model->get_admin_info($admin_id),
            'admin_status' => $this->selection->get_admin_status(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_view_status');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_change_status($admin_id = null)
    {
        //check if admin exist
        if (!$admin_id || !$this->admin_model->check_id($admin_id)) {
            $this->session->set_flashdata('error', "Admin does not exist.");
            redirect(base_url('admin/home'), 'refresh');
        }

        //admin should not be able to edit their own permission
        if ($this->session->id == $admin_id) {
            redirect(base_url('admin/admin_view_status/'.$admin_id), 'refresh');
        }

        if (!permission_granted(array(A_S))) {
            $this->admin_permission_check(array(A_S), "access other admin profiles.");
        }

        $help = array(
            $this->definitions->required_help(),
            "<span>Update admin status.</span><br/><br/>",
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $this->admin_model->get_admin_info($admin_id),
            'admin_status' => $this->selection->get_admin_status(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_change_status');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_change_status_ajax($admin_id = null)
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(A_S)) || ($this->session->id == $admin_id)) {
            handleError($data, "You do not have permission to change admin status.", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($admin_id)) {
                handleError($data, "Admin ID not found.", false);
            }

            if (!isset($post_data['status'])) {
                handleError($data, "Admin status required.", false, 'status');
            }

            if ($data['success']) {
                if ($this->admin_model->update_status($admin_id, $post_data['status'])) {
                    handleError($data, "Status updated.", true);
                } else {
                    handleError($data, "Failed to update status.", false);
                }
            }
        } else {
            handleError($data, "Unable to change status. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function admin_change_password($admin_id = null)
    {
        //admin does not exist
        if (!$admin_id || !$this->admin_model->check_id($admin_id)) {
            $this->session->set_flashdata('error', "Admin does not exist.");
            redirect(base_url('admin/home'), 'refresh');
        }

        //admin has to be superuser to change others password
        //otherwise, only the admin in the session can make updates
        if (!permission_granted(array(A_S)) && ($this->session->id != $admin_id)) {
            $this->admin_permission_check(array(A_S), "access other admin profiles.");
        }

        $admin_info = $this->admin_model->get_admin_info($admin_id);

        if ($admin_info->test_account) {
            $this->session->set_flashdata('error', "Your account has been flagged as a test account. You may not change your password at this time.");
            redirect(base_url('admin/home'), 'refresh');
        }

        $who = $this->session->id == $admin_id ? "your" : "admin";

        $help = array(
            $this->definitions->required_help(),
            "<span>Update $who password. Click 'save' to process password change request.</span><br/><br/>",
            $this->definitions->password_help(),
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Admin ID: $admin_id" . SITE_TITLE,
            'admin' => $admin_info,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_user_sidebar');
        $this->load->view('admin/admin_change_password');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_change_password_ajax($admin_id = null)
    {
        if (!permission_granted(array(A_S)) && ($this->session->id != $admin_id)) {
            handleError($data, "You do not have permission to edit other admin profile.", false);
            echo json_encode($data);
            exit();
        }

        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['old_password'])) {
                handleError($data, "Old password required.", false, 'old_password');
            }

            if (empty($post_data['new_password'])) {
                handleError($data, "New password required.", false, 'new_password');
            } elseif (strlen($post_data['new_password']) < 8) {
                handleError($data, "New password must be atleast 8 characters.", false, 'new_password');
            }

            if (empty($post_data['confirm_password'])) {
                handleError($data, "Confirm password required.", false, 'confirm_password');
            } elseif (strlen($post_data['confirm_password']) < 8) {
                handleError($data, "Confirm password must be atleast 8 characters.", false, 'confirm_password');
            }

            if ($post_data['new_password'] != $post_data['confirm_password']) {
                handleError($data, "Confirm password does not match.", false, 'confirm_password');
            }

            if ($data['success']) {
                $admin_info = $this->admin_model->get_admin_info($admin_id);

                if ($this->password->verify_password_hash($post_data['old_password'], $admin_info->password)) {
                    if ($this->admin_model->update_password($admin_id, $post_data['new_password'])) {
                        handleError($data, "Password updated.", true);
                    //TODO clear fields
                    } else {
                        handleError($data, "Failed to update password.", false);
                    }
                } else {
                    handleError($data, "Old password does not match existing password.", false, 'old_password');
                }
            }
        } else {
            handleError($data, "Unable to change password. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function admin_add_user()
    {
        $this->admin_permission_check(array(A_C, A_S), 'add new employee');

        $help = array(
            $this->definitions->required_help(),
            "<span>Add new administrators. Click submit to process new administrator.</span><br/><br/>",
            $this->definitions->password_help(),
            $this->definitions->permission_help(),
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Add New Admin" . SITE_TITLE,
            'employees' => $this->admin_model->get_all_non_admin(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'adm_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/admin_add_user');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function admin_add_user_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(A_C, A_S))) {
            handleError($data, "You do not have permission to do add new admin.", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (!isset($post_data['employee_id'])) {
                handleError($data, "No employee selected.", false, 'employee_id');
            } elseif (isset($post_data['employee_id']) &&
                  $this->admin_model->check_admin_exist($post_data['employee_id'])) {
                handleError($data, "Admin already exists", false, 'employee_id');
            }

            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'password');
            } elseif (strlen($post_data['password']) < 8) {
                handleError($data, "Password must be atleast 8 characters long.", false, 'password');
            }

            if (empty($post_data['confirm_password'])) {
                handleError($data, "Confirm Password required.", false, 'confirm_password');
            }

            if ($post_data['password'] != $post_data['confirm_password']) {
                handleError($data, "Confirm Password does not match.", false, 'confirm_password');
            }

            unset($post_data['confirm_password']); //no longer needed

            if ($data['success']) {
                //hash password
                $post_data['password'] = $this->password->hash_password($post_data['password']);

                $message = $this->admin_model->add_new_admin($post_data);

                if (is_numeric($message)) {
                    $data['redirect_url'] = base_url("/admin/home");
                    $data['path'] = base_url("/admin/admin_info/$message");
                    handleError($data, "New admin added.", true);
                } else {
                    handleError($data, $message, false);
                }
            }
        } else {
            handleError($data, "Unable to add admin. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function admin_permission_check($permissions, $message)
    {
        if (!permission_granted($permissions)) {
            $this->session->set_flashdata('error', "You do not have proper permissions to $message.");
            redirect(base_url('admin/home'), 'refresh');
        }
    }
}
