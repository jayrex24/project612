<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Applicant extends CI_Controller
{
    public $active = true;

    public function jobs()
    {
        $page_data = array(
            'title' => 'Jobs' . SITE_TITLE,
            'page' => "jobs",
            'jobs' => $this->job_model->get_open_jobs(),
            'session' => $this->session,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error')
        );

        $this->load->view('header', $page_data);
        $this->load->view('applicant/applicant_header');
        $this->load->view('applicant/applicant_jobs');
        $this->load->view('applicant/applicant_footer');
        $this->load->view('footer');
    }

    public function apply($job_id = null)
    {
        //no id given or job assigned
        if (!isset($job_id)
              || !$this->job_model->get_job_by_id($job_id)) {
            redirect(base_url('/applicant/jobs'), 'refresh');
        }

        $show_form = false;
        if ($this->session->applicant) {
            $form = "<div class='row'>
                      <div class='col col-md-6'>
                        <button type='button' class='btn btn-primary' data-jobid='$job_id' id='logged_in_apply_button'>
                          <span class='glyphicon glyphicon-floppy-disk'></span>
                          Apply
                        </button>
                      </div>
                    </div>";

            if ($this->applicant_model->already_applied($this->session->applicant['id'], $job_id)) {
                $form = "<p class='text-muted'>You have already applied for this position.</p>";
            }
        } else {
            $show_form = true;
            $form = "";
        }

        if ($this->applicant_model->job_assigned($job_id)) {
            $show_form = false;
            $form = "<p class='text-muted'>Position no longer available.</p>";
        }

        $page_data = array(
            'title' => 'Apply' . SITE_TITLE,
            'page' => "apply",
            'job' => $this->job_model->get_job_by_id($job_id),
            'states' => $this->selection->get_states(),
            'education_types' => $this->selection->get_education(),
            'phone_types' => $this->selection->get_phones(),
            'session' => $this->session,
            'form' => $form,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
        );

        $this->load->view('header', $page_data);
        $this->load->view('applicant/applicant_header');
        $this->load->view('applicant/applicant_apply');

        if ($show_form) {
            $this->load->view('applicant/apply_form');
        }

        $this->load->view('applicant/applicant_footer');
        $this->load->view('footer');
    }

    public function hire_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(C_A, A_S))) {
            handleError($data, "You do not have permission to convert applicant", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['applicant_id'])) {
                handleError($data, "Application not found.", false);
            }

            if (empty($post_data['job_id'])) {
                handleError($data, "Job not found.", false);
            }

            $applicant = new Applicants($post_data['applicant_id']);
            $applicant_info = $applicant->loadApplicant();
            $personal = $applicant_info['personal'];
            $address = $applicant_info['address'];
            $phone = json_decode(json_encode($applicant_info['phone']), true);
            $education = json_decode(json_encode($applicant_info['education']), true);
            $employment = json_decode(json_encode($applicant_info['history']), true);

            $personal = array(
                'first_name' => $personal->first_name,
                'mid_init' => $personal->mid_init,
                'last_name' => $personal->last_name,
                'email' => $personal->email,
                'date_of_birth' => $personal->date_of_birth,
                'employment_date' => date('Y-m-d'),
                'status_date' => date('Y-m-d'),
                'employment_status' => 'Active',
                'ssn' => $personal->ssn
            );

            $address = array(
                'addr1' => $address->addr1,
                'addr2' => $address->addr2,
                'city' => $address->city,
                'state' => $address->state,
                'zip_code' => $address->zip_code
            );

            if ($data['success']) {
                if ($employee = $this->employee_model->get_employee_by_email($personal['email'])) {

                    //update employee Information
                    if ($this->employee_model->update_employee_complete($employee->id, $personal, $address, $phone, $education, $employment, $post_data['job_id'])) {
                        $this->applicant_model->set_application_status($post_data['applicant_id'], $post_data['job_id'], 2);
                        handleError($data, "", true);
                    } else {
                        handleError($data, "Failed to updated existing employee with applicant information.", false);
                    }
                } else {
                    //insert new employee
                    if ($this->employee_model->create_employee($personal, $address, $phone, $education, $employment, $post_data['job_id'])) {
                        $this->applicant_model->set_application_status($post_data['applicant_id'], $post_data['job_id'], 2);
                        handleError($data, "", true);
                    } else {
                        handleError($data, "Failed to add applicant as an employee.", false);
                    }
                }
            }
        } else {
            handleError($data, "Unable to convert applicant. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function check_if_employee()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['applicant_id'])) {
                handleError($data, "Application not found.", false);
            }

            $applicant = new Applicants($post_data['applicant_id']);
            $applicant_info = $applicant->loadApplicant();
            $personal = $applicant_info['personal'];

            if ($this->employee_model->employee_email_exists($personal->email)) {
                $employee = $this->employee_model->get_employee_by_email($personal->email);
                $data['message']['employee_exist'] = true;
                $data['message']['employee_exist_name'] = htmlentities($employee->first_name . " " .$employee->last_name);
                handleError($data, "Employee exists.", false);
            }
        }

        echo json_encode($data);
        exit();
    }

    public function reject_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(C_A, A_S))) {
            handleError($data, "You do not have permission to convert applicant", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['applicant_id'])) {
                handleError($data, "Application not found.", false);
            }

            if (empty($post_data['job_id'])) {
                handleError($data, "Job not found.", false);
            }

            if ($data['success']) {
                if ($this->applicant_model->set_application_status($post_data['applicant_id'], $post_data['job_id'], 1)) {
                    handleError($data, "Application successfully rejected.", true);
                } else {
                    handleError($data, "Failed to reject employee application. Try again later.", false);
                }
            }
        } else {
            handleError($data, "Unable to reject application. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function easy_apply_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['job_id'])) {
                handleError($data, "Position not found.", false);
            } elseif ($this->applicant_model->job_assigned($post_data['job_id'])) {
                handleError($data, "Position not available.", false);
            } elseif ($this->applicant_model->already_applied($this->session->applicant['id'], $post_data['job_id'])) {
                handleError($data, "You've already applied for this position.", false);
            }

            if ($data['success']) {
                if ($this->applicant_model->apply_to_job($this->session->applicant['id'], $post_data['job_id'])) {
                    $data['redirect_url'] = base_url('/applicant/manage');
                    handleError($data, "Your application has been submitted.", true);
                } else {
                    handleError($data, "Failed to apply to position. Try again later.", false);
                }
            }
        } else {
            handleError($data, "Unable to add application. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function add_applicant_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['first_name'])) {
                handleError($data, "First name required.", false, 'first_name');
            }

            if (empty($post_data['last_name'])) {
                handleError($data, "Last name required.", false, 'last_name');
            }

            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, 'email');
            } elseif (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'email');
            } elseif ($this->applicant_model->applicant_email_exists($post_data['email'])) {
                handleError($data, "Email already exists.", false, 'email');
            }

            if (empty($post_data['date_of_birth'])) {
                handleError($data, "Date of Birth required.", false, 'date_of_birth');
            } elseif (!validateDate($post_data['date_of_birth'], 'Y-m-d')) {
                handleError($data, "Date of Birth - Invalid date format.", false, 'date_of_birth');
            }

            if (empty($post_data['ssn'])) {
                handleError($data, "Social Security Number required.", false, 'ssn');
            } elseif (!validateSSN($post_data['ssn'])) {
                handleError($data, "Invalid Social Security Number.", false, 'ssn');
            }

            if (empty($post_data['addr1'])) {
                handleError($data, "Address 1 required.", false, 'addr1');
            }

            if (empty($post_data['city'])) {
                handleError($data, "City required.", false, 'city');
            }

            $state = null;
            if (!isset($post_data['state']) || empty($post_data['state'])) {
                handleError($data, "State required.", false, 'state');
            } else {
                $state = $post_data['state'];
            }

            if (empty($post_data['zip_code'])) {
                handleError($data, "Zip Code required.", false, 'zip_code');
            }

            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'password');
            } elseif (strlen($post_data['password']) < 8) {
                handleError($data, "Password must be atleast 8 characters.", false, 'password');
            }

            if (empty($post_data['confirm_password'])) {
                handleError($data, "Confirm password required.", false, 'confirm_password');
            } elseif (strlen($post_data['confirm_password']) < 8) {
                handleError($data, "Confirm password must be atleast 8 characters.", false, 'confirm_password');
            }

            if ($post_data['password'] != $post_data['confirm_password']) {
                handleError($data, "Confirm password does not match.", false, 'confirm_password');
            }

            if (empty($post_data['job_position_id'])) {
                handleError($data, "Job position required.", false, 'job_position_id');
            }

            if (!isset($post_data['opt-out-phone'])) {
                foreach ($post_data['phone'] as $key => $phone) {
                    $index = $key + 1;
                    if (empty($phone['phone_number'])) {
                        handleError($data, "Phone #$index - phone number required.", false, "phoneNumber_$index");
                    }
                }
            } else {
                $post_data['phone'] = null;
            }

            if (!isset($post_data['opt-out-education'])) {
                foreach ($post_data['education'] as $key => &$education) {
                    $index = $key + 1;
                    if (empty($education['institution'])) {
                        handleError($data, "Education #$index - School/Institution required.", false, "institution_$index");
                    }

                    if (empty($education['degree_certification'])) {
                        handleError($data, "Education #$index - Type required.", false, "degreeCertification_$index");
                    }

                    if (empty($education['from_date'])) {
                        handleError($data, "Education #$index - From Date required.", false, "fromDate_$index");
                    }

                    if (!isset($education['present'])) {
                        if (empty($education['to_date'])) {
                            handleError($data, "Education #$index - To Date required.", false, "toDate_$index");
                        }

                        if (strtotime($education['from_date']) > strtotime($education['to_date'])) {
                            handleError($data, "Education #$index - To Date  - Must be later than from date.", false, "toDate_$index");
                        }
                    } else {
                        $education['to_date'] = "0000-00-00";
                        unset($education['present']);
                    }
                }
            } else {
                $post_data['education'] = null;
            }

            if (!isset($post_data['opt-out-employment'])) {
                foreach ($post_data['employment'] as $key => &$employment) {
                    $index = $key + 1;
                    if (empty($employment['institution'])) {
                        handleError($data, "Employment #$index - Company/Institution required.", false, "institutionEmployment_$index");
                    }

                    if (empty($employment['job_title'])) {
                        handleError($data, "Employment #$index - Position required.", false, "positionEmployment_$index");
                    }

                    if (empty($employment['from_date'])) {
                        handleError($data, "Employment #$index - From Date required.", false, "fromDateEmployment_$index");
                    } elseif (!validateDate($employment['from_date'], 'Y-m-d')) {
                        handleError($data, "Employment #$index - From Date - Invalid date format.", false, "fromDateEmployment_$index");
                    }

                    if (!isset($employment['present'])) {
                        if (empty($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date required.", false, "toDateEmployment_$index");
                        } elseif (!validateDate($employment['to_date'], 'Y-m-d')) {
                            handleError($data, "Employment #$index - To Date - Invalid date format.", false, "toDateEmployment_$index");
                        }

                        if (strtotime($employment['from_date']) > strtotime($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date  - Must be later than from date.", false, "toDateEmployment_$index");
                        }

                        if (strtotime($employment['from_date']) > strtotime($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date  - Must be later than from date.", false, "toDateEmployment_$index");
                        }

                        if (empty($employment['reason_for_leaving'])) {
                            handleError($data, "Employment #$index - Reason for leaving required.", false, "reasonForLeavingEmployment_$index");
                        }
                    } else {
                        $employment['to_date'] = "0000-00-00";
                        unset($employment['present']);
                    }
                }
            } else {
                $post_data['employment'] = null;
            }

            if ($data['success']) {
                $personal_data = array(
                  'first_name' => $post_data['first_name'],
                  'mid_init' => $post_data['mid_init'],
                  'last_name' => $post_data['last_name'],
                  'email' => $post_data['email'],
                  'date_of_birth' => $post_data['date_of_birth'],
                  'ssn' => $post_data['ssn']
                );

                $address_data = array(
                  'addr1' => $post_data['addr1'],
                  'addr2' => $post_data['addr2'],
                  'city' => $post_data['city'],
                  'state' => $state,
                  'zip_code' => $post_data['zip_code'],
                );

                $message = $this->applicant_model->create_applicant(
                  $personal_data,
                  $address_data,
                  $post_data['phone'],
                  $post_data['education'],
                  $post_data['employment'],
                  $post_data['job_position_id'],
                  $post_data['password']
                );

                if (is_numeric($message)) {
                    $data['redirect_url'] = base_url('applicant/jobs');
                    handleError($data, "Your application has been submitted. View your application status by logging in.", true);
                } else {
                    handleError($data, $message, false);
                }
            }
        } else {
            handleError($data, "Unable to add application. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function view()
    {
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Applicants' . SITE_TITLE,
            'applicants' => $this->applicant_model->get_all_applications(),
            'apl_active' => $this->active,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error')
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('applicant/applicant_view');
        $this->load->view('footer');
    }

    public function applicant_details($applicant_id = null, $job_id = null)
    {
        $help = array();

        if (!isset($applicant_id) || !isset($job_id)) {
            $this->session->set_flashdata('error', "Missing applicantion details.");
            redirect(base_url('applicant/view'), 'refresh');
        }

        if (!$applicant = $this->applicant_model->get_applicant_info($applicant_id)) {
            $this->session->set_flashdata('error', "Applicant does not exist.");
            redirect(base_url('applicant/view'), 'refresh');
        }

        if (!$job = $this->job_model->get_job_by_id($job_id)) {
            $this->session->set_flashdata('error', "Job position does not exist.");
            redirect(base_url('applicant/view'), 'refresh');
        }

        if (!$this->applicant_model->already_applied($applicant_id, $job_id)) {
            $this->session->set_flashdata('error', "No record of applicant applying for provided job id.");
            redirect(base_url('applicant/view'), 'refresh');
        }

        $help = array(
            "<span>Hire/Reject applications.</span><br/><br/>"
        );

        $page_data = array(
            'title' => 'Application Status' . SITE_TITLE,
            'page' => "manage",
            'employee_assinged' => $this->job_model->get_employee_by_job_id($job_id),
            'application_status' => $this->applicant_model->get_application_status($applicant_id, $job_id)->status,
            'applicant_info' => $applicant,
            'applicant_address' => $this->applicant_model->get_applicant_address($applicant_id),
            'applicant_education' => $this->applicant_model->get_applicant_education($applicant_id),
            'applicant_history' => $this->applicant_model->get_applicant_history($applicant_id),
            'applicant_phone' => $this->applicant_model->get_applicant_phone($applicant_id),
            'job_info' => $job,
            'session' => $this->session->userdata,
            'states' => $this->selection->get_states(),
            'education_types' => $this->selection->get_education(),
            'phone_types' => $this->selection->get_phones(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'apl_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('applicant/applicant_details');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function status()
    {
        if (!$this->session->applicant) {
            redirect(base_url('applicant/jobs'), 'refresh');
        }

        $page_data = array(
            'title' => 'Application Status' . SITE_TITLE,
            'page' => "manage",
            'session' => $this->session,
            'applications' => $this->applicant_model->get_applicant_applied($this->session->applicant['id']),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
        );

        $this->load->view('header', $page_data);
        $this->load->view('applicant/applicant_header');
        $this->load->view('applicant/applicant_application_status');
        $this->load->view('applicant/applicant_footer');
        $this->load->view('footer');
    }

    public function manage()
    {
        if (!$this->session->applicant) {
            redirect(base_url('applicant/jobs'), 'refresh');
        }

        $page_data = array(
            'title' => 'Application Status' . SITE_TITLE,
            'page' => "manage",
            'applicant_info' => $this->applicant_model->get_applicant_info($this->session->applicant['id']),
            'applicant_address' => $this->applicant_model->get_applicant_address($this->session->applicant['id']),
            'applicant_education' => $this->applicant_model->get_applicant_education($this->session->applicant['id']),
            'applicant_history' => $this->applicant_model->get_applicant_history($this->session->applicant['id']),
            'applicant_phone' => $this->applicant_model->get_applicant_phone($this->session->applicant['id']),
            'session' => $this->session,
            'states' => $this->selection->get_states(),
            'education_types' => $this->selection->get_education(),
            'phone_types' => $this->selection->get_phones(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
        );

        $this->load->view('header', $page_data);
        $this->load->view('applicant/applicant_header');
        $this->load->view('applicant/applicant_manage_profile');
        $this->load->view('applicant/applicant_footer');
        $this->load->view('footer');
    }

    public function manage_ajax()
    {
        $applicant_id = $this->session->applicant['id'];
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['first_name'])) {
                handleError($data, "First name required.", false, 'first_name');
            }

            if (empty($post_data['last_name'])) {
                handleError($data, "Last name required.", false, 'last_name');
            }

            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, 'email');
            } elseif (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'email');
            } elseif ($this->applicant_model->applicant_email_exists($post_data['email'], $applicant_id)) {
                handleError($data, "Email already exists.", false, 'email');
            }

            if (empty($post_data['date_of_birth'])) {
                handleError($data, "Date of Birth required.", false, 'date_of_birth');
            } elseif (!validateDate($post_data['date_of_birth'], 'Y-m-d')) {
                handleError($data, "Date of Birth - Invalid date format.", false, 'date_of_birth');
            }

            if (empty($post_data['ssn'])) {
                handleError($data, "Social Security Number required.", false, 'ssn');
            } elseif (!validateSSN($post_data['ssn'])) {
                handleError($data, "Invalid Social Security Number.", false, 'ssn');
            }

            if (empty($post_data['addr1'])) {
                handleError($data, "Address 1 required.", false, 'addr1');
            }

            if (empty($post_data['city'])) {
                handleError($data, "City required.", false, 'city');
            }

            $state = null;
            if (!isset($post_data['state']) || empty($post_data['state'])) {
                handleError($data, "State required.", false, 'state');
            } else {
                $state = $post_data['state'];
            }

            if (empty($post_data['zip_code'])) {
                handleError($data, "Zip Code required.", false, 'zip_code');
            }

            if (!isset($post_data['opt-out-phone'])) {
                foreach ($post_data['phone'] as $key => $phone) {
                    $index = $key + 1;
                    if (empty($phone['phone_number'])) {
                        handleError($data, "Phone #$index - phone number required.", false, "phoneNumber_$index");
                    }
                }
            } else {
                $post_data['phone'] = null;
            }

            if (!isset($post_data['opt-out-education'])) {
                foreach ($post_data['education'] as $key => &$education) {
                    $index = $key + 1;
                    if (empty($education['institution'])) {
                        handleError($data, "Education #$index - School/Institution required.", false, "institution_$index");
                    }

                    if (empty($education['degree_certification'])) {
                        handleError($data, "Education #$index - Type required.", false, "degreeCertification_$index");
                    }

                    if (empty($education['from_date'])) {
                        handleError($data, "Education #$index - From Date required.", false, "fromDate_$index");
                    }

                    if (!isset($education['present'])) {
                        if (empty($education['to_date'])) {
                            handleError($data, "Education #$index - To Date required.", false, "toDate_$index");
                        }

                        if (strtotime($education['from_date']) > strtotime($education['to_date'])) {
                            handleError($data, "Education #$index - To Date  - Must be later than from date.", false, "toDate_$index");
                        }
                    } else {
                        $education['to_date'] = "0000-00-00";
                        unset($education['present']);
                    }
                }
            } else {
                $post_data['education'] = null;
            }

            if (!isset($post_data['opt-out-employment'])) {
                foreach ($post_data['employment'] as $key => &$employment) {
                    $index = $key + 1;
                    if (empty($employment['institution'])) {
                        handleError($data, "Employment #$index - Company/Institution required.", false, "institutionEmployment_$index");
                    }

                    if (empty($employment['job_title'])) {
                        handleError($data, "Employment #$index - Position required.", false, "positionEmployment_$index");
                    }

                    if (empty($employment['from_date'])) {
                        handleError($data, "Employment #$index - From Date required.", false, "fromDateEmployment_$index");
                    } elseif (!validateDate($employment['from_date'], 'Y-m-d')) {
                        handleError($data, "Employment #$index - From Date - Invalid date format.", false, "fromDateEmployment_$index");
                    }

                    if (!isset($employment['present'])) {
                        if (empty($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date required.", false, "toDateEmployment_$index");
                        } elseif (!validateDate($employment['to_date'], 'Y-m-d')) {
                            handleError($data, "Employment #$index - To Date - Invalid date format.", false, "toDateEmployment_$index");
                        }

                        if (strtotime($employment['from_date']) > strtotime($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date  - Must be later than from date.", false, "toDateEmployment_$index");
                        }

                        if (empty($employment['reason_for_leaving'])) {
                            handleError($data, "Employment #$index - Reason for leaving required.", false, "reasonForLeavingEmployment_$index");
                        }
                    } else {
                        $employment['to_date'] = "0000-00-00";
                        unset($employment['present']);
                    }
                }
            } else {
                $post_data['employment'] = null;
            }

            if ($data['success']) {
                $personal_data = array(
                  'first_name' => $post_data['first_name'],
                  'mid_init' => $post_data['mid_init'],
                  'last_name' => $post_data['last_name'],
                  'email' => $post_data['email'],
                  'date_of_birth' => $post_data['date_of_birth'],
                  'ssn' => $post_data['ssn']
                );

                $address_data = array(
                  'addr1' => $post_data['addr1'],
                  'addr2' => $post_data['addr2'],
                  'city' => $post_data['city'],
                  'state' => $state,
                  'zip_code' => $post_data['zip_code'],
                );

                $message = $this->applicant_model->update_applicant_complete(
                  $applicant_id,
                  $personal_data,
                  $address_data,
                  $post_data['phone'],
                  $post_data['education'],
                  $post_data['employment']
                );

                if (is_numeric($message)) {
                    $data['redirect_url'] = base_url('applicant/jobs');
                    handleError($data, "Your application has been updated.", true);
                } else {
                    handleError($data, $message, false);
                }
            }
        } else {
            handleError($data, "Unable to updated application. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function login_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, 'applicant_email');
            } elseif (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'applicant_email');
            }

            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'applicant_password');
            }

            if ($data['success']) {
                list($verify, $applicant_id) = $this->auth_model->applicant_login_verify($post_data['email'], $post_data['password']);

                if ($verify) {
                    if ($applicant_info = $this->applicant_model->get_applicant_info($applicant_id)) {
                        unset(
                          $applicant_info->password,
                          $applicant_info->selector,
                          $applicant_info->token
                        );
                        $applicant_info->name_for_header = sprintf("%s, %s.", $applicant_info->last_name, substr($applicant_info->first_name, 0, 1));
                        $this->session->set_userdata('applicant', (array)$applicant_info);

                        $data['redirect_url'] = base_url('/applicant/status');
                        handleError($data, "Login success.", true);
                    } else {
                        handleError($data, "Failed to log in, unable to retrieve profile.", false);
                    }
                } else {
                    handleError($data, "Failed to log in, please provide correct credentials.", false);
                }
            }
        } else {
            handleError($data, "Unable to login. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function password_retrieve_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['forgot_email'])) {
                handleError($data, "Email required.", false, 'forgot_email');
                echo json_encode($data);
                exit();
            } elseif (!filter_var($post_data['forgot_email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'forgot_email');
                echo json_encode($data);
                exit();
            }

            if ($data['success'] && $this->applicant_model->applicant_email_exists($post_data['forgot_email'])) {
                // Create tokens
                $selector = bin2hex(random_bytes(8));
                $token = random_bytes(32);

                $url = sprintf('%sapplicant/reset?%s', base_url(), http_build_query(['selector' => $selector, 'validator' => bin2hex($token)]));

                // Token expiration
                $expires = new DateTime('NOW');
                $expires->add(new DateInterval('PT01H')); // 1 hour

                // Delete any existing tokens for this user
                $this->applicant_model->delete_token($post_data['forgot_email']);

                // Update reset token in database
                if ($this->applicant_model->update_token($post_data['forgot_email'], $selector, hash('sha256', $token), $expires->format('U'))) {
                    ob_start();
                    send_reset_email($url, $post_data['forgot_email']);
                    ob_end_clean();
                    handleError($data, "Success", true);
                }
            }
        } else {
            handleError($data, "Unable to process request. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function reset()
    {
        $selector = filter_input(INPUT_GET, 'selector');
        $validator = filter_input(INPUT_GET, 'validator');

        $allow_reset = false;
        if (false !== ctype_xdigit($selector) && false !== ctype_xdigit($validator)) {
            $allow_reset = true;
        }

        if (!$allow_reset) {
            redirect('applicant/jobs');
        }

        $page_data = array(
            'title' => 'Reset Password' . SITE_TITLE,
            'page' => "reset",
            'session' => $this->session,
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'selector' => filter_input(INPUT_GET, 'selector'),
            'validator' => filter_input(INPUT_GET, 'validator')
        );

        $this->load->view('header', $page_data);
        $this->load->view('applicant/applicant_header');
        $this->load->view('applicant/applicant_password_reset');
        $this->load->view('applicant/applicant_footer');
        $this->load->view('footer');
    }

    public function password_reset_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'password');
            } elseif (strlen($post_data['password']) < 8) {
                handleError($data, "Password must be atleast 8 characters.", false, 'password');
            }

            if (empty($post_data['selector']) || empty($post_data['validator'])) {
                handleError($data, "Unable to complete request. Error code 001", false);
            }

            if ($data['success']) {
                if ($result = $this->applicant_model->get_token($post_data['selector'], time())) {
                    $calc = hash('sha256', hex2bin($post_data['validator']));

                    // Validate tokens
                    if (hash_equals($calc, $result->token)) {

                        // Update password
                        if ($this->applicant_model->update_password($result->id, $post_data['password'])) {
                            $this->applicant_model->delete_token($result->email);
                            $data['redirect_url'] = base_url("/applicant/jobs");
                            session_destroy();
                        }
                    } else {
                        handleError($data, "There was an error processing your request. Error code 002", false);
                    }
                } else {
                    handleError($data, "There was an error processing your request. Error code 003", false);
                }
            }
        }

        echo json_encode($data);
        exit();
    }

    public function logout()
    {
        $this->session->unset_userdata('applicant');
        redirect(base_url('applicant/jobs'), 'refresh');
    }
}
