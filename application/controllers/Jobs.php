<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jobs extends CI_Controller
{
    public $active = true;

    public function __construct()
    {
        parent::__construct();
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);
    }

    public function home()
    {
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Jobs' . SITE_TITLE,
            'success' => $success,
            'error' => $error,
            'jobs' => $this->job_model->get_all_jobs(),
            'job_active' => $this->active,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('job/jobs');
        $this->load->view('footer');
    }

    public function job_info($job_id = null)
    {
        if (!$job_info = $this->job_model->get_job_by_id($job_id)) {
            $this->session->set_flashdata('error', 'Job position does not exist.');
            redirect(base_url('/jobs/home'), 'refresh');
        }

        if (!permission_granted(array(J_C, A_S))) {
            $this->session->set_flashdata('error', "You do not have proper permissions to edit job information.");
            redirect(base_url('/jobs/home'), 'refresh');
        }

        $help = array(
            $this->definitions->required_help(),
            "<span>Update job position. Click save to process changes.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Description</h4>
                <span>Provide the requirements for the position.</span>
              </div>",
            "<div class='help-div'>
                <h4>Facility Name</h4>
                <span>Indicate which facility the new position will be added to.</span>
              </div>",
            "<div class='help-div'>
                <h4>Employee Assigned</h4>
                <span>Indicates which employee currently holds the position.</span>
              </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Job ID: $job_id" . SITE_TITLE,
            'error' => $this->session->flashdata('error'),
            'success' => $this->session->flashdata('success'),
            'job_info' => $job_info,
            'facilities' => $this->job_model->get_all_facility(),
            'employee_assinged' => $this->job_model->get_employee_by_job_id($job_id),
            'job_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('job/job_info');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function job_info_ajax($job_id)
    {
        if (!permission_granted(array(J_C, A_S))) {
            $this->session->set_flashdata('error', "You do not have proper permissions to edit job information.");
            redirect(base_url('/jobs/home'), 'refresh');
        }

        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['job_title'])) {
                handleError($data, "Job Title required.", false, 'job_title');
            }

            if (empty($post_data['description'])) {
                handleError($data, "Description required.", false, 'description');
            }

            if (empty($post_data['facility_id'])) {
                handleError($data, "Facility required.", false, 'facility_id');
            }

            if ($data['success']) {
                if ($this->job_model->set_job_info($job_id, $post_data)) {
                    handleError($data, "Successfully updated job information.", true);
                } else {
                    handleError($data, "Failed to update job information", false);
                }
            }
        } else {
            handleError($data, "Unable to update job information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function add_new_position()
    {
        $help = array(
            $this->definitions->required_help(),
            "<span>Add new job position. Click submit to process new position.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Description</h4>
                <span>Provide the requirements for the position.</span>
              </div>",
            "<div class='help-div'>
                <h4>Facility Name</h4>
                <span>Indicate which facility the new position will be added to.</span>
             </div>"
        );

        $job_update = true;
        if (!permission_granted(array(J_C, A_S))) {
            $job_update = false;
        }

        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Jobs' . SITE_TITLE,
            'success' => $success,
            'error' => $error,
            'facilities' => $this->job_model->get_all_facility(),
            'job_active' => $this->active,
            'help' => $help,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('job/add_new_position');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function add_new_position_ajax()
    {
        if (!permission_granted(array(J_C, A_S))) {
            $this->employee_redirect('You do not have permission to create a new job position.', 'error', 'jobs/home');
        }

        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            log_message('error', print_r($post_data, true));

            if (empty($post_data['job_title'])) {
                handleError($data, "Job title required.", false, "job_title");
            }

            if (empty($post_data['description'])) {
                handleError($data, "Description required.", false, "description");
            }

            if (empty($post_data['facility_id'])) {
                handleError($data, "Facility required.", false, 'facility_id');
            }

            if ($data['success']) {
                if ($position_id = $this->job_model->add_new_position($post_data)) {
                    $data['redirect_url'] = base_url("/jobs/home");
                    $data['path'] = base_url("/jobs/job_info/$position_id");
                    handleError($data, "New position added.", true);
                } else {
                    handleError($data, "Failed to add new position.", false);
                }
            }
        } else {
            handleError($data, "Unable to add new position. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }
}
