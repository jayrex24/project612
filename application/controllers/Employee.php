<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends CI_Controller
{
    public $active = true;

    public function __construct()
    {
        parent::__construct();
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);
    }

    /**
     * Contains datatable of all employees
     */
    public function employees()
    {
        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Employees' . SITE_TITLE,
            'employees' => $this->employee_model->get_all(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employees');
        $this->load->view('footer');
    }

    public function employee_info($employee_id = null)
    {
        $this->employee_check($employee_id);

        $help = array(
            $this->definitions->required_help(),
            "<span>Update employee personal information. Click save to process changes.</span><br/><br/>",
            $this->definitions->dob_help(),
            $this->definitions->employment_status_help()
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Personal - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'employee_info' => $this->employee_model->get_employee_info($employee_id),
            'employment_status' => $this->selection->get_status(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_personal');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_info_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        $employee_info = $this->employee_model->get_employee_info($employee_id);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['first_name'])) {
                handleError($data, "First name required.", false, 'first_name');
            }

            if (empty($post_data['last_name'])) {
                handleError($data, "Last name required.", false, 'last_name');
            }

            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, 'email');
            } elseif (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, 'email');
            } elseif ($this->employee_model->employee_email_exists($post_data['email'], $employee_id)) {
                handleError($data, "Email already exists.", false, 'email');
            }

            if (empty($post_data['date_of_birth'])) {
                handleError($data, "Date of Birth required.", false, 'date_of_birth');
            } elseif (!validateDate($post_data['date_of_birth'], 'Y-m-d')) {
                handleError($data, "Date of Birth - Invalid date format.", false, 'date_of_birth');
            }

            if (empty($post_data['ssn'])) {
                handleError($data, "Social Security Number required.", false, 'ssn');
            } elseif (!validateSSN($post_data['ssn'])) {
                handleError($data, "Invalid Social Security Number.", false, 'ssn');
            }

            if (empty($post_data['employment_date'])) {
                handleError($data, "Employment Date required.", false, 'employment_date');
            } elseif (!validateDate($post_data['employment_date'], 'Y-m-d')) {
                handleError($data, "Employment Date - Invalid date format.", false, 'employment_date');
            }

            $status_date = $employee_info->status_date;
            if (empty($post_data['employment_status'])) {
                handleError($data, "Employment status required.", false, 'employment_status');
            } elseif ($employee_info->employment_status != $post_data['employment_status']) {
                $status_date = date("Y-m-d");
            }

            if ($data['success']) {
                $personal_data = array(
                    'first_name' => $post_data['first_name'],
                    'mid_init' => $post_data['mid_init'],
                    'last_name' => $post_data['last_name'],
                    'email' => $post_data['email'],
                    'date_of_birth' => $post_data['date_of_birth'],
                    'ssn' => $post_data['ssn'],
                    'employment_date' => $post_data['employment_date'],
                    'employment_status' => $post_data['employment_status'],
                    'status_date' => $status_date,
                );

                if ($this->employee_model->set_personal_info($employee_id, $personal_data)) {
                    handleError($data, "Successfully updated personal information.", true);
                } else {
                    handleError($data, "Failed to update personal information", false);
                }
            }
        } else {
            handleError($data, "Unable to update personal information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function employee_address($employee_id = null)
    {
        $this->employee_check($employee_id);

        $help = array(
            $this->definitions->required_help(),
            "<span>Update employee address information. Click save to process changes.</span><br/><br/>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Address - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'employee_address' => $this->employee_model->get_employee_address($employee_id),
            'states' => $this->selection->get_states(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_address');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_address_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        $this->employee_check_ajax($data, $employee_id);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['addr1'])) {
                handleError($data, "Address 1 required.", false, 'addr1');
            }

            if (empty($post_data['city'])) {
                handleError($data, "City required.", false, 'city');
            }

            $state = null;
            if (!isset($post_data['state']) || empty($post_data['state'])) {
                handleError($data, "State required.", false, 'state');
            } else {
                $state = $post_data['state'];
            }

            if (empty($post_data['zip_code'])) {
                handleError($data, "Zip Code required.", false, 'zip_code');
            }

            if ($data['success']) {
                if ($this->employee_model->set_address_info($employee_id, $post_data)) {
                    handleError($data, "Successfully updated address information.", true);
                } else {
                    handleError($data, "Failed to update address information", false);
                }
            }
        } else {
            handleError($data, "Unable to update address information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function employee_education($employee_id = null)
    {
        $this->employee_check($employee_id);

        $help = array(
          $this->definitions->required_help(),
          "<span>Update employee education information. Click save to process changes.</span><br/><br/>",
          "<div class='help-div'>
              <h4>Actions</h4>
              <ul>
                <li>Click [+ Add Education] button if you require multiple entries.</li>
                <li>A remove icon will be provided if more than one entry.</li>
                <li>Check 'No education' checkbox to remove all entries.</li>
              </ul>
            </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Education - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'employee_education' => $this->employee_model->get_employee_education($employee_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'education_types' => $this->selection->get_education(),
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_education');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_education_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (!isset($post_data['opt-out-education'])) {
                pl($post_data['education']);
                foreach ($post_data['education'] as $key => &$education) {
                    $index = $key + 1;
                    if (empty($education['institution'])) {
                        handleError($data, "Education #$index - School/Institution required.", false, "institution_$index");
                    }

                    if (empty($education['degree_certification'])) {
                        handleError($data, "Education #$index - Degree/Certification required.", false, "degreeCertification_$index");
                    }

                    if (empty($education['from_date'])) {
                        handleError($data, "Education #$index - From Date required.", false, "fromDate_$index");
                    } elseif (!validateDate($education['from_date'], 'Y-m-d')) {
                        handleError($data, "Education #$index - From Date - Invalid date format.", false, "fromDate_$index");
                    }

                    if (!isset($education['present'])) {
                        if (empty($education['to_date'])) {
                            handleError($data, "Education #$index - To Date required.", false, "toDate_$index");
                        } elseif (!validateDate($education['to_date'], 'Y-m-d')) {
                            handleError($data, "Education #$index - To Date  - Invalid date format.", false, "toDate_$index");
                        }

                        if (strtotime($education['from_date']) > strtotime($education['to_date'])) {
                            handleError($data, "Education #$index - To Date  - Must be later than from date.", false, "toDate_$index");
                        }
                    } else {
                        $education['to_date'] = "0000-00-00";
                        unset($education['present']);
                    }
                }

                if ($data['success']) {
                    if ($this->employee_model->set_education_info($employee_id, $post_data['education'])) {
                        handleError($data, "Successfully updated education information.", true);
                    } else {
                        handleError($data, "Failed to update education information", false);
                    }
                }
            } else {
                if ($this->employee_model->delete_education_info($employee_id)) {
                    handleError($data, "Successfully updated education information.", true);
                } else {
                    handleError($data, "Failed to update education information", false);
                }
            }
        } else {
            handleError($data, "Unable to update education information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function employee_history($employee_id = null)
    {
        $this->employee_check($employee_id);

        $help = array(
          $this->definitions->required_help(),
          "<span>Update employee employment history information. Click save to process changes.</span><br/><br/>",
          "<div class='help-div'>
              <h4>Actions</h4>
              <ul>
                <li>Click [+ Add Employment] button if you require multiple entries.</li>
                <li>A remove icon will be provided if more than one entry.</li>
                <li>Check 'No Employment History' checkbox to remove all entries.</li>
              </ul>
            </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Employment History - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'employee_history' => $this->employee_model->get_employee_history($employee_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'education_types' => $this->selection->get_education(),
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_history');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_history_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (!isset($post_data['opt-out-employment'])) {
                foreach ($post_data['employment'] as $key => &$employment) {
                    $index = $key + 1;
                    if (empty($employment['institution'])) {
                        handleError($data, "Employment #$index - Company/Institution required.", false, "institution_$index");
                    }

                    if (empty($employment['job_title'])) {
                        handleError($data, "Employment #$index - Position required.", false, "position_$index");
                    }

                    if (empty($employment['from_date'])) {
                        handleError($data, "Employment #$index - From Date required.", false, "fromDate_$index");
                    } elseif (!validateDate($employment['from_date'], 'Y-m-d')) {
                        handleError($data, "Employment #$index - From Date - Invalid date format.", false, "fromDate_$index");
                    }

                    if (!isset($employment['present'])) {
                        if (empty($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date required.", false, "toDate_$index");
                        } elseif (!validateDate($employment['to_date'], 'Y-m-d')) {
                            handleError($data, "Employment #$index - To Date - Invalid date format.", false, "toDate_$index");
                        }

                        if (strtotime($employment['from_date']) > strtotime($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date  - Must be later than from date.", false, "toDate_$index");
                        }

                        if (empty($employment['reason_for_leaving'])) {
                            handleError($data, "Employment #$index - Reason for leaving required.", false, "reasonForLeaving_$index");
                        }
                    } else {
                        $employment['to_date'] = "0000-00-00";
                        unset($employment['present']);
                    }
                }

                if ($data['success']) {
                    if ($this->employee_model->set_employment_info($employee_id, $post_data['employment'])) {
                        handleError($data, "Successfully updated employment information.", true);
                    } else {
                        handleError($data, "Failed to update employment information", false);
                    }
                }
            } else {
                if ($this->employee_model->delete_employment_history($employee_id)) {
                    handleError($data, "Successfully updated employment information.", true);
                } else {
                    handleError($data, "Failed to update employment information", false);
                }
            }
        } else {
            handleError($data, "Unable to update employment information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function employee_phone($employee_id)
    {
        $this->employee_check($employee_id);

        $help = array(
            $this->definitions->required_help(),
            "<span>Update phone numbers.
            At least one number must be kept in the system. Click save to process changes.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Actions</h4>
                <ul>
                  <li>Click [+ Add Phone] button if you require multiple entries.</li>
                  <li>A remove icon will be provided if more than one entry.</li>
                  <li>Check 'No phone' checkbox to remove all entries.</li>
                </ul>
              </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Phone Info - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'phones' => $this->employee_model->get_employee_phone($employee_id),
            'phone_types' => $this->selection->get_phones(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_phone');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_phone_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (!isset($post_data['opt-out-phone'])) {
                foreach ($post_data['phone'] as $key => $phone) {
                    $index = $key + 1;
                    if (empty($phone['phone_number'])) {
                        handleError($data, "Phone #$index - Phone number required", false, "phoneNumber_$index");
                    }
                }

                if ($data['success']) {
                    if ($this->employee_model->set_phone_info($employee_id, $post_data['phone'])) {
                        handleError($data, "Successfully updated phone information.", true);
                    } else {
                        handleError($data, "Failed to update phone information", false);
                    }
                }
            } else {
                if ($this->employee_model->delete_phone_number($employee_id)) {
                    handleError($data, "Successfully updated phone information.", true);
                } else {
                    handleError($data, "Failed to update phone information", false);
                }
            }
        } else {
            handleError($data, "Unable to update information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function employee_dependent($employee_id)
    {
        $this->employee_check($employee_id);

        $help = array(
            $this->definitions->required_help(),
            "<span>Update employee dependent information. Click save to process changes.</span><br/><br/>",
            "<div class='help-div'>
                <h4>Actions</h4>
                <ul>
                  <li>Click [+ Add Dependents] button if you require multiple entries.</li>
                  <li>A remove icon will be provided if more than one entry.</li>
                  <li>Check 'No dependent' checkbox to remove all entries.</li>
                </ul>
              </div>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Employee Dependent - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'dependents' => $this->employee_model->get_employee_dependents($employee_id),
            'relationship_types' => $this->selection->get_relationships(),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_dependent');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_dependent_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (!isset($post_data['opt-out-dependent'])) {
                foreach ($post_data['dependent'] as $key => $dependent) {
                    $index = $key + 1;
                    if (empty($dependent['first_name'])) {
                        handleError($data, "Dependent #$index - First Name required.", false, "firstName_$index");
                    }

                    if (empty($dependent['last_name'])) {
                        handleError($data, "Dependent #$index - Last Name required.", false, "lastName_$index");
                    }

                    if (empty($dependent['relationship'])) {
                        handleError($data, "Dependent #$index - Relationship required.", false, "relationship_$index");
                    }

                    if (empty($dependent['date_of_birth'])) {
                        handleError($data, "Dependent #$index - Date of Birth required.", false, "dob_$index");
                    } elseif (!validateDate($dependent['date_of_birth'], 'Y-m-d')) {
                        handleError($data, "Dependent #$index - Date of Birth - Invalid date format.", false, "dob_$index");
                    }

                    if (empty($dependent['ssn'])) {
                        handleError($data, "Dependent #$index - Social Security Number required.", false, "ssn_$index");
                    } elseif (!validateSSN($dependent['ssn'])) {
                        handleError($data, "Dependent #$index - Invalid Social Security Number.", false, "ssn_$index");
                    }
                }

                if ($data['success']) {
                    if ($this->employee_model->set_dependent_info($employee_id, $post_data['dependent'])) {
                        handleError($data, "Successfully updated dependent information.", true);
                    } else {
                        handleError($data, "Failed to update dependent information", false);
                    }
                }
            } else {
                if ($this->employee_model->delete_dependents($employee_id)) {
                    handleError($data, "Successfully updated dependent information.", true);
                } else {
                    handleError($data, "Failed to update dependent information", false);
                }
            }
        } else {
            handleError($data, "Unable to update dependent information. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }


    public function employee_jobchange($employee_id = null)
    {
        $this->employee_check($employee_id);

        $help = array(
            $this->definitions->required_help(),
            "<span>Update employee job assignment information. Click save to process changes.</span><br/><br/>"
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Job Change - Employee ID: $employee_id" . SITE_TITLE,
            'employee_id' => $employee_id,
            'jobs' => $this->job_model->get_open_jobs(),
            'current_job' => $this->job_model->get_job_by_employee($employee_id),
            'success' => $this->session->flashdata('success'),
            'error' => $this->session->flashdata('error'),
            'emp_active' => $this->active,
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/employee_sidebar');
        $this->load->view('employees/employee_jobchange');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function employee_jobchange_ajax($employee_id)
    {
        $data = array('message' => array(), 'success' => true);

        if ($message = $this->employee_check_ajax($data, $employee_id)) {
            handleError($data, $message, false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['job_id']) || $post_data['job_id'] == 0) {
                handleError($data, "Job Assignment required.", false);
            }

            if ($data['success']) {
                if ($this->employee_model->assign_job($employee_id, $post_data['job_id'])) {
                    handleError($data, "Successfully updated job assignment.", true);
                } else {
                    handleError($data, "Failed to update job assignment", false);
                }
            }
        } else {
            handleError($data, "Unable to update job assignment. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    public function add_employee()
    {
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        if (!permission_granted(array(E_C, A_S))) {
            $this->employee_redirect('You do not have permission to add new employee.', 'error', 'employee/employees');
        }

        $help = array(
            $this->definitions->required_help(),
            "<span>Add new employee. Click submit to process new employee.</span><br/><br/>",
            $this->definitions->dob_help(),
        );

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => "Add New Employee" . SITE_TITLE,
            'success' => $success,
            'error' => $error,
            'emp_active' => $this->active,
            'states' => $this->selection->get_states(),
            'education_types' => $this->selection->get_education(),
            'employment_status' => $this->selection->get_status(),
            'open_jobs' => $this->job_model->get_open_jobs(),
            'phone_types' => $this->selection->get_phones(),
            'help' => $help
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('employees/add_employee');
        $this->load->view('instructions');
        $this->load->view('footer');
    }

    public function add_employee_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if (!permission_granted(array(E_C, A_S))) {
            handleError($data, "You do not have permission to add new employee.", false);
            echo json_encode($data);
            exit();
        }

        if ($post_data = $this->input->post()) {
            if (empty($post_data['first_name'])) {
                handleError($data, "First name required.", false, "first_name");
            }

            if (empty($post_data['last_name'])) {
                handleError($data, "Last name required.", false, "last_name");
            }

            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, "email");
            } elseif (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                handleError($data, "Invalid email format.", false, "email");
            } elseif ($this->employee_model->employee_email_exists($post_data['email'])) {
                handleError($data, "Email already exists.", false, "email");
            }

            if (empty($post_data['date_of_birth'])) {
                handleError($data, "Date of Birth required.", false, "date_of_birth");
            } elseif (!validateDate($post_data['date_of_birth'], 'Y-m-d')) {
                handleError($data, "Date of Birth - Invalid date format.", false, "date_of_birth");
            }

            if (empty($post_data['ssn'])) {
                handleError($data, "Social Security Number required.", false, "ssn");
            } elseif (!validateSSN($post_data['ssn'])) {
                handleError($data, "Invalid Social Security Number.", false, "ssn");
            }

            if (empty($post_data['addr1'])) {
                handleError($data, "Address 1 required.", false, "addr1");
            }

            if (empty($post_data['city'])) {
                handleError($data, "City required.", false, "city");
            }

            $state = null;
            if (!isset($post_data['state']) || empty($post_data['state'])) {
                handleError($data, "State required.", false, "state");
            } else {
                $state = $post_data['state'];
            }

            if (empty($post_data['zip_code'])) {
                handleError($data, "Zip Code required.", false, "zip_code");
            }

            if (!isset($post_data['opt-out-phone'])) {
                foreach ($post_data['phone'] as $key => $phone) {
                    $index = $key + 1;
                    if (empty($phone['phone_number'])) {
                        handleError($data, "Phone #$index - Phone number $index required.", false, "phoneNumber_$index");
                    }
                }
            } else {
                $post_data['phone'] = null;
            }

            if (!isset($post_data['opt-out-education'])) {
                foreach ($post_data['education'] as $key => &$education) {
                    $index = $key + 1;
                    if (empty($education['institution'])) {
                        handleError($data, "Education #$index - School/Institution required.", false, "institution_$index");
                    }

                    if (empty($education['degree_certification'])) {
                        handleError($data, "Education #$index - Degree/Certification required.", false, "degreeCertification_$index");
                    }

                    if (empty($education['from_date'])) {
                        handleError($data, "Education #$index - From Date required.", false, "fromDate_$index");
                    } elseif (!validateDate($education['from_date'], 'Y-m-d')) {
                        handleError($data, "Education #$index - From Date - Invalid date format.", false, "fromDate_$index");
                    }

                    if (!isset($education['present'])) {
                        if (empty($education['to_date'])) {
                            handleError($data, "Education #$index - To Date required.", false, "toDate_$index");
                        } elseif (!validateDate($education['to_date'], 'Y-m-d')) {
                            handleError($data, "Education #$index - To Date - Invalid date format.", false, "toDate_$index");
                        }

                        if (strtotime($education['from_date']) > strtotime($education['to_date'])) {
                            handleError($data, "Education #$index - To Date  - Must be later than from date.", false, "toDate_$index");
                        }
                    } else {
                        $education['to_date'] = "0000-00-00";
                        unset($education['present']);
                    }
                }
            } else {
                $post_data['education'] = null;
            }

            if (!isset($post_data['opt-out-employment'])) {
                foreach ($post_data['employment'] as $key => &$employment) {
                    $index = $key + 1;
                    if (empty($employment['institution'])) {
                        handleError($data, "Employment #$index - Company/Institution required.", false, "institutionEmployment_$index");
                    }

                    if (empty($employment['job_title'])) {
                        handleError($data, "Employment #$index - Position required.", false, "positionEmployment_$index");
                    }

                    if (empty($employment['from_date'])) {
                        handleError($data, "Employment #$index - From Date required.", false, "fromDateEmployment_$index");
                    } elseif (!validateDate($employment['from_date'], 'Y-m-d')) {
                        handleError($data, "Employment #$index - From Date - Invalid date format.", false, "fromDateEmployment_$index");
                    }

                    if (!isset($employment['present'])) {
                        if (empty($employment['to_date'])) {
                            handleError($data, "Employment #$index - To Date required.", false, "toDateEmployment_$index");
                        } elseif (!validateDate($employment['to_date'], 'Y-m-d')) {
                            handleError($data, "Employment #$index - To Date - Invalid date format.", false, "toDateEmployment_$index");
                        }

                        if (strtotime($employment['from_date']) > strtotime($employment['to_date'])) {
                            handleError($data, "Education #$index - To Date  - Must be later than from date.", false, "toDateEmployment_$index");
                        }

                        if (empty($employment['reason_for_leaving'])) {
                            handleError($data, "Education #$index - Reason for leaving required.", false, "reasonForLeavingEmployment_$index");
                        }
                    } else {
                        $employment['to_date'] = "0000-00-00";
                        unset($employment['present']);
                    }
                }
            } else {
                $post_data['employment'] = null;
            }

            if (empty($post_data['employment_date'])) {
                handleError($data, "Employment Date required.", false, "employment_date");
            } elseif (!validateDate($post_data['employment_date'], 'Y-m-d')) {
                handleError($data, "Employment Date - Invalid date format.", false, "employment_date");
            }

            if (empty($post_data['job_position_id'])) {
                handleError($data, "Job position required.", false, "job_position_id");
            } elseif ($this->job_model->job_assigned($post_data['job_position_id'])) {
                handleError($data, "Position has already been assigned.", false, "job_position_id");
            }

            if (empty($post_data['employment_status'])) {
                handleError($data, "Employment status required.", false, 'employment_status');
            }

            if ($data['success']) {
                $personal_data = array(
                    'first_name' => $post_data['first_name'],
                    'mid_init' => $post_data['mid_init'],
                    'last_name' => $post_data['last_name'],
                    'email' => $post_data['email'],
                    'date_of_birth' => $post_data['date_of_birth'],
                    'ssn' => $post_data['ssn'],
                    'employment_date' => $post_data['employment_date'],
                    'employment_status' => $post_data['employment_status']
                );

                $address_data = array(
                    'addr1' => $post_data['addr1'],
                    'addr2' => $post_data['addr2'],
                    'city' => $post_data['city'],
                    'state' => $state,
                    'zip_code' => $post_data['zip_code'],
                );

                $message = $this->employee_model->create_employee(
                    $personal_data,
                    $address_data,
                    $post_data['phone'],
                    $post_data['education'],
                    $post_data['employment'],
                    $post_data['job_position_id']
                );

                if (is_numeric($message)) {
                    $data['redirect_url'] = base_url('employee/employees');
                    $data['path'] = base_url("/employee/employee_info/$message");
                    handleError($data, "New employee added.", true);
                } else {
                    handleError($data, $message, false);
                }
            }
        } else {
            handleError($data, "Unable to add employee. Try again later.", false);
        }

        echo json_encode($data);
        exit();
    }

    private function employee_check($employee_id)
    {
        if (!$employee_id || !$this->employee_model->check_id($employee_id)) {
            $this->employee_redirect('Employee does not exist.', 'error', 'employee/employees');
        }

        if ($employee_id != $this->session->employee_id) {
            if ($this->admin_model->check_admin_exist($employee_id) && !permission_granted(array(A_S))) {
                $this->employee_redirect('You do not have permission to edit other admin employees.', 'error', 'employee/employees');
            }
            if (!permission_granted(array(E_C, A_S))) {
                $this->employee_redirect('You do not have permission to edit employee profiles.', 'error', 'employee/employees');
            }
        }
    }

    private function employee_check_ajax($data, $employee_id)
    {
        if ($employee_id != $this->session->employee_id) {
            if ($this->admin_model->check_admin_exist($employee_id) && !permission_granted(array(A_S))) {
                return "You do not have permission to edit admin employees.";
            }
            if (!permission_granted(array(E_C, A_S))) {
                return "You do not have permission to edit employee profiles.";
            }
        }
        return false;
    }

    private function employee_redirect($message, $name, $path)
    {
        $this->session->set_flashdata($name, $message);
        redirect(base_url($path), 'refresh');
    }
}
