<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public $active = true;

    public function __construct()
    {
        parent::__construct();
        $this->auth_model->is_admin();
        $this->session->permission = $this->admin_model->get_permissions($this->session->id);
    }

    public function home()
    {
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        $page_data = array(
            'session' => $this->session->userdata,
            'title' => 'Dashboard' . SITE_TITLE,
            'success' => $success,
            'error' => $error,
            'dash_active' => $this->active,
            'dashboard_selected' => array_values($this->dashboard_model->get_dashboard_selected()),
        );

        $this->load->view('header', $page_data);
        $this->load->view('admin/admin_header');
        $this->load->view('dashboard/dashboard_home');
        $this->load->view('footer');
    }

    public function get_employee_count_by_state()
    {
        $data = $this->employee_model->get_employee_count_by_state();

        $x = array();
        $y = array();
        foreach ($data as $value) {
            if ($value->state == "") {
                $x[] = "'Unknown'";
            } else {
                $x[] = "'$value->state'";
            }
            $y[] = $value->count;
        }
        $x = implode(",", $x);
        $y = implode(",", $y);

        echo json_encode($this->dashboard_model->create_chart(
          'employee_by_state',
          'Employees By States',
          'Number of Employees',
          'States',
          '#0066CC',
          'Employees',
          $x,
          $y,
          'bar'
        ));
        exit;
    }

    public function get_employee_count_by_status()
    {
        $data = $this->employee_model->get_employee_count_by_status();

        $x = array();
        $y = array();
        foreach ($data as $value) {
            $x[] = "'$value->employment_status'";
            $y[] = $value->count;
        }
        $x = implode(",", $x);
        $y = implode(",", $y);

        echo json_encode($this->dashboard_model->create_chart(
          'employee_by_status',
          'Employees By Status',
          'Number of Employees',
          'Status',
          '#FFA500',
          'Employees',
          $x,
          $y,
          'column'
        ));
        exit;
    }

    public function get_applicant_count_by_month()
    {
        $data = $this->applicant_model->get_applicant_count_by_month();

        $x = array();
        $y = array();
        foreach ($data as $value) {
            $date = date("M Y", strtotime($value->monthyear));
            $x[] = "'$date'";
            $y[] = $value->count;
        }
        $x = implode(",", $x);
        $y = implode(",", $y);

        echo json_encode($this->dashboard_model->create_chart(
          'applicants_by_month',
          'Applicants By Month',
          'Number of Applicants',
          'Month',
          '#00FF00',
          'Applicant',
          $x,
          $y,
          'line'
        ));
        exit;
    }

    public function get_application_count_by_day()
    {
        $data = $this->applicant_model->get_application_count_by_day();

        $x = array();
        $y = array();
        foreach ($data as $value) {
            $x[] = "'$value->day'";
            $y[] = $value->count;
        }
        $x = implode(",", $x);
        $y = implode(",", $y);

        echo json_encode($this->dashboard_model->create_chart(
          'applications_by_day',
          'Applications By Day',
          'Number of Applications',
          'Day',
          '#FF00FF',
          'Applications',
          $x,
          $y,
          'line'
        ));
        exit;
    }

    public function get_jobs_count_by_facility()
    {
        $data = $this->facility_model->get_jobs_count_by_facility();

        $x = array();
        $y = array();
        foreach ($data as $value) {
            $x[] = "'$value->facility_name'";
            $y[] = $value->count;
        }
        $x = implode(",", $x);
        $y = implode(",", $y);

        echo json_encode($this->dashboard_model->create_chart(
          'jobs_by_facility',
          'Jobs By Facility',
          'Number of Jobs',
          'Facility',
          '#00FFFF',
          'Jobs',
          $x,
          $y,
          'bar'
        ));
        exit;
    }

    public function add_dashboard_type()
    {
        if (!$this->session->id) {
            return false;
        }

        $params = array(
          'admin_id' => $this->session->id,
          'dashboard_type' => $this->input->post('dashboard_type'),
        );

        if ($this->dashboard_model->insert_admin_dashboard($params)) {
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }

        echo json_encode($data);
    }

    public function delete_dashboard_type()
    {
        if (!$this->session->id) {
            return false;
        }

        $params = array(
          'admin_id' => $this->session->id,
          'dashboard_type' => $this->input->post('dashboard_type'),
        );

        if ($this->dashboard_model->delete_admin_dashboard($params)) {
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }

        echo json_encode($data);
    }
}
