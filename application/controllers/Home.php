<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    /**
     * Landing page (Login)
     */
    public function index()
    {
        if (isset($_SESSION['is_admin']) && ($_SESSION['is_admin'] == true)) {
            redirect(base_url('/dashboard/home'), 'refresh');
        }

        $page_data = array(
            'title' => 'Admin Login' . SITE_TITLE,
        );

        $this->load->view('header', $page_data);
        $this->load->view('login/login');
        $this->load->view('footer');
    }

    public function login_ajax()
    {
        $data = array('message' => array(), 'success' => true);

        if ($post_data = $this->input->post()) {
            if (empty($post_data['email'])) {
                handleError($data, "Email required.", false, 'email');
            }

            if (empty($post_data['password'])) {
                handleError($data, "Password required.", false, 'password');
            }

            list($verify, $admin_id) = $this->auth_model->user_login_verify($post_data['email'], $post_data['password']);

            if ($verify) {
                if ($admin_info = $this->admin_model->get_admin_info($admin_id)) {
                    if ($admin_info->status == 0) {
                        handleError($data, "Your account has been set to inactive. Please contact your administrator if you believe it to be a mistake.", false);
                        echo json_encode($data);
                        exit();
                    }

                    //remove password so that it isn't added to session variables
                    unset($admin_info->{"password"});
                    $admin_info->is_admin = true;
                    $admin_info->name_for_header = sprintf("%s, %s.", $admin_info->last_name, substr($admin_info->first_name, 0, 1));
                    $this->session->set_userdata((array)$admin_info);

                    $data['redirect_url'] = base_url('/dashboard/home');
                    handleError($data, "Login success.", true);
                } else {
                    handleError($data, "Failed to log in, unable to retrieve profile.", false);
                }
            } else {
                handleError($data, "Failed to log in, please provide correct credentials.", false);
            }
        }

        echo json_encode($data);
        exit();
    }



    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
}
