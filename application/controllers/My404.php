<?php
defined('BASEPATH') or exit('No direct script access allowed');

class My404 extends CI_Controller
{
    public function index()
    {
        $page_data = array(
          'title' => 'Page Not Found' . SITE_TITLE
        );

        $this->load->view('header', $page_data);
        $this->load->view('404');
        $this->load->view('footer');
    }
}
