<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Definitions
{
    public function username_help()
    {
        return "<div class='help-div'>
                  <h4>Username</h4>
                  <ul>
                    <li>Provide a unique username</li>
                    <li>Max length: <strong>20</strong> characters</li>
                    <li>Case-insensitive</li>
                  </ul>
                </div>";
    }

    public function permission_help()
    {
        return "<div class='help-div'>
                  <h4>Permission</h4>
                  <ul>
                    <li><strong>Employee Create</strong> - Able to add and edit employee profiles</li>
                    <li><strong>Admin Create</strong> - Able to add and edit other admin profiles</li>
                    <li><strong>Facility Create</strong> -  Able to add and edit facilities</li>
                    <li><strong>Job Create</strong> -  Able to add and edit job positions</li>
                    <li><strong>Convert Applicants</strong> -  Able to convert applicant to employees</li>
                    <li><strong>Admin Superuser</strong> -  Able to do all the functions above</li>
                  </ul>
                </div>";
    }

    public function password_help()
    {
        return "<div class='help-div'>
                  <h4>Password</h4>
                  <ul>
                    <li>Atleast <strong>8</strong> characters long</li>
                    <li>New password and Confirm password must match</li>
                  </ul>
                </div>";
    }

    public function required_help()
    {
        return "<div class='help-div'>* Indicates required field.<br/><br/></div>";
    }

    public function employment_status_help()
    {
        return "<div class='help-div'>
                  <h4>Employment Status</h4>
                  <ul>
                    <li><strong>Active</strong> - Working</li>
                    <li><strong>Inactive</strong> - Suspended or Resigned</li>
                    <li><strong>Terminated</strong> - Quit or Fired</li>
                  </ul>
                </div>";
    }

    public function dob_help()
    {
        return "<div class='help-div'>
                  <h4>Date of Birth</h4>
                  <ul>
                    <li>Minimum age of employement: <strong>14</strong></li>
                    <li>Format: (YYYY-MM-DD)</li>
                  </ul>
                </div>";
    }

    public function education_help()
    {
        return "<div class='help-div'>
                  <h4>Education</h4>
                  <span>Please enter most recent education background. (Must have at least one)</span>
                </div>";
    }

    public function phone_help()
    {
        return "<div class='help-div'>
                  <h4>Phone Information</h4>
                  <span>Please enter phone numbers you can be reached by. (Must have at least one)</span>
                </div>";
    }
}
