<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Applicants
{
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct($id = null)
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();

        if ($id) {
            $this->setId($id);
        }
    }

    private $id;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function loadApplicant()
    {
        return array(
          'personal' => $this->personal = $this->getPersonal(),
          'address' => $this->address = $this->getAddress(),
          'phone' => $this->phone = $this->getPhone(),
          'education' => $this->education = $this->getEducation(),
          'history' => $this->history = $this->getHistory(),
        );
    }

    /******************************************************
    Personal
    ******************************************************/

    private $personal;

    public function setPersonal($personal)
    {
        $this->personal = $personal;
    }

    public function getPersonal()
    {
        if (!$this->personal) {
            $this->personal = $this->getPersonalDB();
        }
        return $this->personal;
    }

    private function getPersonalDB()
    {
        $this->CI->db->where('id', $this->id);
        if ($result = $this->CI->db->get('applicant')->row()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Address
    ******************************************************/

    private $address;

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        if (!$this->address) {
            $this->address = $this->getAddressDB();
        }
        return $this->address;
    }

    private function getAddressDB()
    {
        $this->CI->db->where('applicant_id', $this->id);
        if ($result = $this->CI->db->get('address')->row()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Phone
    ******************************************************/

    private $phone;

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        if (!$this->phone) {
            $this->phone = $this->getPhoneDB();
        }
        return $this->phone;
    }

    private function getPhoneDB()
    {
        $this->CI->db->select('phone_number, extension, phone_type');
        $this->CI->db->where('applicant_id', $this->id);
        if ($result = $this->CI->db->get('phone_information')->result()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Education
    ******************************************************/

    private $education;

    public function setEducation($education)
    {
        $this->education = $education;
    }

    public function getEducation()
    {
        if (!$this->education) {
            $this->education = $this->getEducationDB();
        }
        return $this->education;
    }

    private function getEducationDB()
    {
        $this->CI->db->select('from_date, to_date, institution, degree_certification, comments');
        $this->CI->db->where('applicant_id', $this->id);
        if ($result = $this->CI->db->get('education_background')->result()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    History
    ******************************************************/

    private $history;

    public function setHistory($history)
    {
        $this->history = $history;
    }

    public function getHistory()
    {
        if (!$this->history) {
            $this->history = $this->getHistoryDB();
        }
        return $this->history;
    }

    private function getHistoryDB()
    {
        $this->CI->db->select('from_date, to_date, institution, job_title, reason_for_leaving, supervisor_name');
        $this->CI->db->where('applicant_id', $this->id);
        if ($result = $this->CI->db->get('employment_background')->result()) {
            return $result;
        }
        return false;
    }
}
