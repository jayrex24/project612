<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employees
{
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct($id = null)
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();

        if ($id) {
            $this->setId($id);
        }
    }

    private $id;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function loadEmployee()
    {
        return array(
          'personal' => $this->personal = $this->getPersonal(),
          'address' => $this->address = $this->getAddress(),
          'phone' => $this->phone = $this->getPhone(),
          'education' => $this->selection = $this->getEducation(),
          'history' => $this->history = $this->getHistory(),
          'dependents' => $this->dependents = $this->getDependents(),
        );
    }

    /******************************************************
    Personal
    ******************************************************/

    private $personal;

    public function setPersonal($personal)
    {
        $this->personal = $personal;
    }

    public function getPersonal()
    {
        if (!$this->personal) {
            $this->personal = $this->getPersonalDB();
        }
        return $this->personal;
    }

    private function getPersonalDB()
    {
        $this->CI->db->where('id', $this->id);
        if ($result = $this->CI->db->get('employee')->row()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Address
    ******************************************************/

    private $address;

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        if (!$this->address) {
            $this->address = $this->getAddressDB();
        }
        return $this->address;
    }

    private function getAddressDB()
    {
        $this->CI->db->where('employee_id', $this->id);
        if ($result = $this->CI->db->get('address')->row()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Phone
    ******************************************************/

    private $phone;

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        if (!$this->phone) {
            $this->phone = $this->getPhoneDB();
        }
        return $this->phone;
    }

    private function getPhoneDB()
    {
        $this->CI->db->select('phone_number, extension, phone_type');
        $this->CI->db->where('employee_id', $this->id);
        if ($result = $this->CI->db->get('phone_information')->result()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Education
    ******************************************************/

    private $education;

    public function setEducation($education)
    {
        $this->selection = $education;
    }

    public function getEducation()
    {
        if (!$this->selection) {
            $this->selection = $this->getEducationDB();
        }
        return $this->selection;
    }

    private function getEducationDB()
    {
        $this->CI->db->select('from_date, to_date, institution, degree_certification, comments');
        $this->CI->db->where('employee_id', $this->id);
        if ($result = $this->CI->db->get('education_background')->result()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    History
    ******************************************************/

    private $history;

    public function setHistory($history)
    {
        $this->history = $history;
    }

    public function getHistory()
    {
        if (!$this->history) {
            $this->history = $this->getHistoryDB();
        }
        return $this->history;
    }

    private function getHistoryDB()
    {
        $this->CI->db->select('from_date, to_date, institution, job_title, reason_for_leaving, supervisor_name');
        $this->CI->db->where('employee_id', $this->id);
        if ($result = $this->CI->db->get('employment_background')->result()) {
            return $result;
        }
        return false;
    }

    /******************************************************
    Dependents
    ******************************************************/

    private $dependents;

    public function setDependents($dependents)
    {
        $this->dependents = $dependents;
    }

    public function getDependents()
    {
        if (!$this->dependents) {
            $this->dependents = $this->getDependentsDB();
        }
        return $this->dependents;
    }

    private function getDependentsDB()
    {
        $sql = "SELECT d.*
                  FROM employee e
             LEFT JOIN dependents d
                    ON e.id = d.employee_id
                 WHERE e.id = ?
              ORDER BY d.id";

        return $this->CI->db->query($sql, array($this->id))->result();
    }
}
