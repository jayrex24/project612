<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Password
{
    public function hash_password($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function verify_password_hash($password, $hash)
    {
        return password_verify($password, $hash);
    }
}
