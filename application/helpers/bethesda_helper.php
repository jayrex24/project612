<?php defined('BASEPATH') or exit('No direct script access allowed');

if (! function_exists('permission_granted')) {
    /**
     * Checks if current permission matches any of the required
     */
    function permission_granted($permission_required = array())
    {
        if (!empty($_SESSION['permission'])
            && array_intersect($_SESSION['permission'], $permission_required)) {
            return true;
        }
        return false;
    }
}

if (! function_exists('validateDate')) {
    /**
     * Checks if value given is in the correct format
     */
    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if (! function_exists('validateSSN')) {
    /**
     * Checks if value given is in the correct format
     */
    function validateSSN($ssn)
    {
        if (preg_match("/^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/", $ssn)) {
            return true;
        }
        return false;
    }
}

if (! function_exists('send_reset_email')) {
    function send_reset_email($url, $to)
    {
        $CI =& get_instance();
        $CI->load->library('email');

        // Subject
        $subject = 'Bethesda Mission - Your password reset link';

        // Message
        $message = '<p>We recieved a password reset request. The link to reset your password is below. ';
        $message .= 'If you did not make this request, you can ignore this email.</p>';
        $message .= '<p>Here is your password reset link: </br>';
        $message .= sprintf('<a href="%s">%s</a></p>', $url, $url);
        $message .= '<p>Thanks!</p>';
        $message .= '<p><strong>(Disclaimer)</strong> This message is created for student project purposes.</p>';

        $CI->email
          ->from("Bethesda Mission<bethesda.md.hr@gmail.com>")
          ->to($to)
          ->subject($subject)
          ->message($message)
          ->send();
    }
}

if (! function_exists('handleError')) {
    /**
     * Easy handle ajax return message
     */
    function handleError(&$array, $message, $success, $id = null)
    {
        $array['message'][] = array('msg' => $message, 'id' => $id);
        $array['success'] = $success;
    }
}

if (! function_exists('pl')) {
    /**
     * Print to Log file
     */
    function pl($data)
    {
        log_message('error', print_r($data, true));
    }
}
